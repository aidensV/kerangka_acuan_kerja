-- phpMyAdmin SQL Dump
-- version 4.7.0
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Mar 01, 2019 at 07:31 AM
-- Server version: 10.1.26-MariaDB
-- PHP Version: 7.0.22

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `kerangka_acuan_kerja`
--

-- --------------------------------------------------------

--
-- Table structure for table `agenda`
--

CREATE TABLE `agenda` (
  `id` int(11) NOT NULL,
  `idsatker` varchar(50) NOT NULL,
  `idkak` varchar(50) NOT NULL,
  `idpengawas` varchar(20) NOT NULL,
  `title` varchar(250) NOT NULL,
  `start` datetime NOT NULL,
  `end` datetime NOT NULL,
  `statuspengerjaan` varchar(5) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `agenda`
--

INSERT INTO `agenda` (`id`, `idsatker`, `idkak`, `idpengawas`, `title`, `start`, `end`, `statuspengerjaan`) VALUES
(1, 'S0002', 'K0001', 'P0001', 'minta tanda tangan ke lurah', '2018-12-13 00:00:00', '2018-12-13 00:00:00', '1'),
(2, 'S0002', 'K0001', 'P0001', 'minta persetujuan camat', '2018-12-05 00:00:00', '2018-12-05 00:00:00', '2'),
(3, 'S0002', 'K0001', 'P0001', 'monitoring', '2018-12-05 00:00:00', '2018-12-05 19:00:00', '3'),
(4, 'S0001', 'K0002', 'P0002', 'jaranan', '2018-12-05 00:00:00', '2018-12-05 19:00:00', '1'),
(5, 'S0002', 'K0003', 'P0002', 'tahap pertama K0003', '2019-01-05 00:00:00', '2019-01-06 00:00:00', '3'),
(6, 'S0001', 'K0004', 'P0001', 'tahap pertama K0004', '2019-01-05 00:00:00', '2019-01-06 00:00:00', '2');

-- --------------------------------------------------------

--
-- Table structure for table `kak`
--

CREATE TABLE `kak` (
  `idkak` varchar(10) NOT NULL,
  `idsatker` varchar(10) NOT NULL,
  `judul` varchar(200) NOT NULL,
  `judulpdf` longtext NOT NULL,
  `latarbelakang` longtext NOT NULL,
  `maksud` longtext NOT NULL,
  `tujuan` longtext NOT NULL,
  `metodepelaksanaan` longtext NOT NULL,
  `tempatpelaksanaan` longtext NOT NULL,
  `penanggungjawab` longtext NOT NULL,
  `status` int(3) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `kak`
--

INSERT INTO `kak` (`idkak`, `idsatker`, `judul`, `judulpdf`, `latarbelakang`, `maksud`, `tujuan`, `metodepelaksanaan`, `tempatpelaksanaan`, `penanggungjawab`, `status`) VALUES
('K0001', 'S0002', 'KEMENTERIAN KEUANGAN KERANGKA ACUAN KERJA/TERM OF REFERENCE KELUARAN GEDUNG OLAHRAGA TA 2013', '<p style=\"text-align:center\">KEMENTERIAN KEUANGAN</p><p style=\"text-align:center\">KERANGKA ACUAN KERJA/TERM OF REFERENCE</p><p style=\"text-align:center\">KELUARAN GEDUNG OLAHRAGA TA 2013<p>', '<p>&nbsp;&nbsp;&nbsp;&nbsp;jalanya rusak di tembus banjir a a a a a a a a a a a a a a a a a a a a a b b b b b b b b b b b c c c c c c c </p><p>&nbsp;&nbsp;&nbsp;&nbsp;a a a a a a a a aaaaaaaaa</p>', 'membetulkan jalan maksud', 'membetulkan jalan tujuan', '1. analisa\r\n2. penulis', 'kota mawar', 'P0001', 1),
('K0004', 'S0002', 'memperingati kemerdekaan republik indonesia', '0', 'latar belakang', 'maksud', 'tujuan', 'metode pelaksanaan', 'tempat', 'P0001', 1),
('K0005', 'S0001', 'coba 1', 'coba 1', 'coba 1', 'coba 1', 'coba 1', 'coba 1', 'coba 1', 'coba 1', 1);

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1);

-- --------------------------------------------------------

--
-- Table structure for table `pengawas`
--

CREATE TABLE `pengawas` (
  `idpengawas` varchar(20) NOT NULL,
  `namapengawas` varchar(50) NOT NULL,
  `alamatpengawas` varchar(50) NOT NULL,
  `nohppengawas` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `pengawas`
--

INSERT INTO `pengawas` (`idpengawas`, `namapengawas`, `alamatpengawas`, `nohppengawas`) VALUES
('P0001', 'pengawas1', 'alamatpengawas1', 'nohppengawas1'),
('P0002', 'pengawas2', 'alamatpengawas2', 'nohppengawas2');

-- --------------------------------------------------------

--
-- Table structure for table `satker`
--

CREATE TABLE `satker` (
  `idsatker` varchar(50) NOT NULL,
  `namasatker` varchar(50) NOT NULL,
  `alamatsatker` varchar(500) NOT NULL,
  `nohpsatker` int(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `satker`
--

INSERT INTO `satker` (`idsatker`, `namasatker`, `alamatsatker`, `nohpsatker`) VALUES
('S0001', 'Dinas Pendidikan', 'Jalan Mayor Bismo No.10-12, Semampir, Kecamatan Kota Kediri, Mojoroto, Kota Kediri, Jawa Timur 64129', 354689923),
('S0002', 'Dinas Pemuda dan Olahraga', 'Jl. KDP Slamet No.33, Mojoroto, Kota Kediri, Jawa Timur 64114', 354773157);

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE `user` (
  `iduser` varchar(50) NOT NULL,
  `email` varchar(100) NOT NULL,
  `nama` varchar(100) NOT NULL,
  `password` varchar(50) NOT NULL,
  `level` int(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`iduser`, `email`, `nama`, `password`, `level`) VALUES
('U0001', 'artha@gmail.com', 'artha', '123456', 1);

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(10) UNSIGNED NOT NULL,
  `iduser` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `level` int(11) NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `iduser`, `name`, `email`, `password`, `level`, `remember_token`, `created_at`, `updated_at`) VALUES
(2, 'S0001', 'arthasatker2', 'arthailma12345@gmail.com', '$2y$10$6NIloNFKX1gQqkS1SepS7uVyMaOIR5G136zVjsnVOe15NpNJO84XW', 1, 'JSJT5o2hH4d2RqlXRiW3Z1i7qYO4bRoqMKZOS3Xp3Qw1LcqHEthYc3LgjH5q', '2018-11-29 21:13:28', '2018-11-29 21:13:28'),
(3, 'V0001', 'uun', 'uunvalidator@gmail.com', '$2y$10$6NIloNFKX1gQqkS1SepS7uVyMaOIR5G136zVjsnVOe15NpNJO84XW', 3, 'g67lKHALEoIHR3umLB28dwTST0ZiLwbofdFVH9YQv0qZcnUYARK52qY4bk6V', NULL, NULL),
(4, 'I0001', 'arthainspektorat', 'arthailma123456@gmail.com', '$2y$10$6NIloNFKX1gQqkS1SepS7uVyMaOIR5G136zVjsnVOe15NpNJO84XW', 2, '2IG0X94d2JrrmhK5fGDWHNn29ISBxluGEvut6zDbclgKEZKa6laLHy21N12b', NULL, NULL),
(6, 'S0002', 'arthasatker1', 'satker1@gmail.com', '$2y$10$6NIloNFKX1gQqkS1SepS7uVyMaOIR5G136zVjsnVOe15NpNJO84XW', 1, 'NmxIikCEoaVKcdfamvIEEPJTmSSLWNJAhekZaPJm2NXZkD0e6wWxWAIrkRG0', NULL, NULL),
(7, 'S0003', 'satker3', 'satker3@gmail.com', '$2y$10$6NIloNFKX1gQqkS1SepS7uVyMaOIR5G136zVjsnVOe15NpNJO84XW', 1, 'vPbkJQq4jWejzTysk0zLgUng4NCht0NxRisCCJxVhdLwNNC8RfG1Z8PwXIbI', NULL, NULL),
(8, 'S0004', 'satker4', 'satker4@gmail.com', '$2y$10$6NIloNFKX1gQqkS1SepS7uVyMaOIR5G136zVjsnVOe15NpNJO84XW', 1, NULL, NULL, NULL),
(9, 'O0001', 'operator1', 'operator1@gmail.com', '$2y$10$6NIloNFKX1gQqkS1SepS7uVyMaOIR5G136zVjsnVOe15NpNJO84XW', 4, NULL, NULL, NULL);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `agenda`
--
ALTER TABLE `agenda`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `kak`
--
ALTER TABLE `kak`
  ADD PRIMARY KEY (`idkak`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `pengawas`
--
ALTER TABLE `pengawas`
  ADD PRIMARY KEY (`idpengawas`);

--
-- Indexes for table `satker`
--
ALTER TABLE `satker`
  ADD PRIMARY KEY (`idsatker`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`iduser`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `agenda`
--
ALTER TABLE `agenda`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
