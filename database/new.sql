/*
SQLyog Ultimate v12.4.1 (64 bit)
MySQL - 10.1.31-MariaDB : Database - kerangka_acuan_kerja
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
CREATE DATABASE /*!32312 IF NOT EXISTS*/`kerangka_acuan_kerja` /*!40100 DEFAULT CHARACTER SET latin1 */;

USE `kerangka_acuan_kerja`;

/*Table structure for table `agenda` */

DROP TABLE IF EXISTS `agenda`;

CREATE TABLE `agenda` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `idsatker` varchar(50) NOT NULL,
  `idkak` varchar(50) NOT NULL,
  `idpengawas` varchar(20) NOT NULL,
  `title` varchar(250) NOT NULL,
  `start` datetime NOT NULL,
  `end` datetime NOT NULL,
  `statuspengerjaan` varchar(5) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;

/*Data for the table `agenda` */

insert  into `agenda`(`id`,`idsatker`,`idkak`,`idpengawas`,`title`,`start`,`end`,`statuspengerjaan`) values 
(1,'S0002','K0001','P0001','minta tanda tangan ke lurah','2018-12-13 00:00:00','2018-12-13 00:00:00','1'),
(2,'S0002','K0001','P0001','minta persetujuan camat','2018-12-05 00:00:00','2018-12-05 00:00:00','2'),
(3,'S0002','K0001','P0001','monitoring','2018-12-05 00:00:00','2018-12-05 19:00:00','3'),
(4,'S0001','K0002','P0002','jaranan','2018-12-05 00:00:00','2018-12-05 19:00:00','1'),
(5,'S0002','K0003','P0002','tahap pertama K0003','2019-01-05 00:00:00','2019-01-06 00:00:00','3'),
(6,'S0001','K0004','P0001','tahap pertama K0004','2019-01-05 00:00:00','2019-01-06 00:00:00','2');

/*Table structure for table `kak` */

DROP TABLE IF EXISTS `kak`;

CREATE TABLE `kak` (
  `idkak` varchar(10) NOT NULL,
  `idsatker` varchar(10) NOT NULL,
  `judul` varchar(200) NOT NULL,
  `judulpdf` longtext,
  `latarbelakang` longtext NOT NULL,
  `maksud` longtext NOT NULL,
  `tujuan` longtext NOT NULL,
  `metodepelaksanaan` longtext NOT NULL,
  `tempatpelaksanaan` longtext NOT NULL,
  `penanggungjawab` longtext NOT NULL,
  `status` int(3) NOT NULL,
  `kak_kegiatan` text,
  `kak_keluaran` text,
  `kak_anggaran` text,
  `kak_penutup` text,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`idkak`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `kak` */

insert  into `kak`(`idkak`,`idsatker`,`judul`,`judulpdf`,`latarbelakang`,`maksud`,`tujuan`,`metodepelaksanaan`,`tempatpelaksanaan`,`penanggungjawab`,`status`,`kak_kegiatan`,`kak_keluaran`,`kak_anggaran`,`kak_penutup`,`created_at`,`updated_at`) values 
('K0001','S0002','KEMENTERIAN KEUANGAN KERANGKA ACUAN KERJA/TERM OF REFERENCE KELUARAN GEDUNG OLAHRAGA TA 2013','<p style=\"text-align:center\">KEMENTERIAN KEUANGAN</p><p style=\"text-align:center\">KERANGKA ACUAN KERJA/TERM OF REFERENCE</p><p style=\"text-align:center\">KELUARAN GEDUNG OLAHRAGA TA 2013<p>','<p>&nbsp;&nbsp;&nbsp;&nbsp;jalanya rusak di tembus banjir a a a a a a a a a a a a a a a a a a a a a b b b b b b b b b b b c c c c c c c </p><p>&nbsp;&nbsp;&nbsp;&nbsp;a a a a a a a a aaaaaaaaa</p>','membetulkan jalan maksud','membetulkan jalan tujuan','1. analisa\r\n2. penulis','kota mawar','P0001',1,NULL,NULL,NULL,NULL,NULL,NULL),
('K0004','S0002','memperingati kemerdekaan republik indonesia','0','latar belakang','maksud','tujuan','metode pelaksanaan','tempat','P0001',1,NULL,NULL,NULL,NULL,NULL,NULL),
('K0005','S0001','coba 1','coba 1','coba 1','coba 1','coba 1','coba 1','coba 1','coba 1',1,NULL,NULL,NULL,NULL,NULL,NULL);

/*Table structure for table `migrations` */

DROP TABLE IF EXISTS `migrations`;

CREATE TABLE `migrations` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*Data for the table `migrations` */

insert  into `migrations`(`id`,`migration`,`batch`) values 
(1,'2014_10_12_000000_create_users_table',1);

/*Table structure for table `pengawas` */

DROP TABLE IF EXISTS `pengawas`;

CREATE TABLE `pengawas` (
  `idpengawas` varchar(20) NOT NULL,
  `namapengawas` varchar(50) NOT NULL,
  `alamatpengawas` varchar(50) NOT NULL,
  `nohppengawas` varchar(50) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`idpengawas`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `pengawas` */

insert  into `pengawas`(`idpengawas`,`namapengawas`,`alamatpengawas`,`nohppengawas`,`created_at`,`updated_at`) values 
('P0001','pengawas1','alamatpengawas1','nohppengawas1',NULL,NULL),
('P0002','pengawas2','alamatpengawas2','nohppengawas2',NULL,NULL),
('P0004','sdfsdf','324242','dsfsdfdsf','2019-03-02 19:37:41','2019-03-02 19:37:41');

/*Table structure for table `satker` */

DROP TABLE IF EXISTS `satker`;

CREATE TABLE `satker` (
  `idsatker` varchar(50) NOT NULL,
  `namasatker` varchar(50) NOT NULL,
  `alamatsatker` varchar(500) NOT NULL,
  `nohpsatker` varchar(50) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`idsatker`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `satker` */

insert  into `satker`(`idsatker`,`namasatker`,`alamatsatker`,`nohpsatker`,`created_at`,`updated_at`) values 
('S0001','Dinas Pendidikan','Jalan Mayor Bismo No.10-12, Semampir, Kecamatan Kota Kediri, Mojoroto, Kota Kediri, Jawa Timur 64129','354689923',NULL,NULL),
('S0002','Dinas Pemuda dan Olahraga','Jl. KDP Slamet No.33, Mojoroto, Kota Kediri, Jawa Timur 64114','354773157',NULL,NULL),
('S0003','dinas pertanian','jalan kebon jeruk nomor 56 lebak bulus - jaktim','08927682887','2019-03-03 11:53:27','2019-03-03 11:53:27');

/*Table structure for table `user` */

DROP TABLE IF EXISTS `user`;

CREATE TABLE `user` (
  `iduser` varchar(50) NOT NULL,
  `email` varchar(100) NOT NULL,
  `nama` varchar(100) NOT NULL,
  `password` varchar(50) NOT NULL,
  `level` int(50) NOT NULL,
  PRIMARY KEY (`iduser`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `user` */

insert  into `user`(`iduser`,`email`,`nama`,`password`,`level`) values 
('U0001','artha@gmail.com','artha','123456',1);

/*Table structure for table `users` */

DROP TABLE IF EXISTS `users`;

CREATE TABLE `users` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `iduser` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `level` int(11) NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*Data for the table `users` */

insert  into `users`(`id`,`iduser`,`name`,`email`,`password`,`level`,`remember_token`,`created_at`,`updated_at`) values 
(2,'S0001','arthasatker2','arthailma12345@gmail.com','$2y$10$6NIloNFKX1gQqkS1SepS7uVyMaOIR5G136zVjsnVOe15NpNJO84XW',1,'mTgEfoCll6LDz7gEfdtTGk9rdxlTF2mccrVKGSHC3fKGYwYI48qNElqmXyyz','2018-11-30 04:13:28','2018-11-30 04:13:28'),
(3,'V0001','uun','uunvalidator@gmail.com','$2y$10$6NIloNFKX1gQqkS1SepS7uVyMaOIR5G136zVjsnVOe15NpNJO84XW',3,'g67lKHALEoIHR3umLB28dwTST0ZiLwbofdFVH9YQv0qZcnUYARK52qY4bk6V',NULL,NULL),
(4,'I0001','arthainspektorat','arthailma123456@gmail.com','$2y$10$6NIloNFKX1gQqkS1SepS7uVyMaOIR5G136zVjsnVOe15NpNJO84XW',2,'2IG0X94d2JrrmhK5fGDWHNn29ISBxluGEvut6zDbclgKEZKa6laLHy21N12b',NULL,NULL),
(6,'S0002','Dinas Pemuda dan Olahraga','satker1@gmail.com','$2y$10$.5MoL7dVRDu38rosiIxUKOAua4cJPX3MX3ghR7p0dwsL.KlOX8QX.',1,'NmxIikCEoaVKcdfamvIEEPJTmSSLWNJAhekZaPJm2NXZkD0e6wWxWAIrkRG0',NULL,'2019-03-02 19:38:27'),
(9,'O0001','operator1','operator1@gmail.com','$2y$10$6NIloNFKX1gQqkS1SepS7uVyMaOIR5G136zVjsnVOe15NpNJO84XW',4,'uVhymGBQNaYGg2nOQpayExQGV0atlVc6MqYdr2rhXZAYPTCdK6zl0VVfVQQx',NULL,NULL),
(11,'P0004','sdfsdf','sdfsdf@sdfs.sffsd','$2y$10$L/FsxoqzgdLas3S8rIRLROZ0sgx25AlVrU0QuJWkrBug6CFH/AEIW',2,NULL,'2019-03-02 19:37:41','2019-03-02 19:39:38'),
(12,'S0003','dinas pertanian','satkerpertanian@gmail.com','$2y$10$W3TkaRDHPXVdpNMipJRMBO.OG1EF1SG8CpsqfD9GYYl1D7h1dBD8G',1,'vUPGA1or99hTmo0fgFyUjEImhrtzJp1DTRTiRz8SOywTvrsDhts5VOxq02b7','2019-03-03 11:53:27','2019-03-03 11:53:27');

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
