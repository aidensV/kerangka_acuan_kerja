/*
SQLyog Ultimate v12.4.1 (64 bit)
MySQL - 10.1.16-MariaDB : Database - carolina
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
CREATE DATABASE /*!32312 IF NOT EXISTS*/`carolina` /*!40100 DEFAULT CHARACTER SET latin1 */;

USE `carolina`;

/*Table structure for table `admin_produk` */

DROP TABLE IF EXISTS `admin_produk`;

CREATE TABLE `admin_produk` (
  `admin_produk_id` int(11) NOT NULL AUTO_INCREMENT,
  `users_id` int(11) DEFAULT NULL,
  `admin_produk_nama` varchar(100) DEFAULT NULL,
  `admin_produk_nohp` varchar(20) DEFAULT NULL,
  `admin_produk_alamat` varchar(200) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`admin_produk_id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

/*Data for the table `admin_produk` */

insert  into `admin_produk`(`admin_produk_id`,`users_id`,`admin_produk_nama`,`admin_produk_nohp`,`admin_produk_alamat`,`created_at`,`updated_at`) values 
(1,2,'Ahmad mudlofar','085777666333','Jl. Pare',NULL,NULL);

/*Table structure for table `kategori` */

DROP TABLE IF EXISTS `kategori`;

CREATE TABLE `kategori` (
  `kategori_id` int(11) NOT NULL AUTO_INCREMENT,
  `kategori_nama` varchar(100) DEFAULT NULL,
  `kategori_jenis` enum('1','2','3') DEFAULT NULL,
  `created_id` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`kategori_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `kategori` */

/*Table structure for table `marketing` */

DROP TABLE IF EXISTS `marketing`;

CREATE TABLE `marketing` (
  `marketing_id` int(11) NOT NULL AUTO_INCREMENT,
  `users_id` int(11) DEFAULT NULL,
  `marketing_nama` varchar(100) DEFAULT NULL,
  `marketing_nohp` varchar(20) DEFAULT NULL,
  `marketing_alamat` varchar(200) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`marketing_id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

/*Data for the table `marketing` */

insert  into `marketing`(`marketing_id`,`users_id`,`marketing_nama`,`marketing_nohp`,`marketing_alamat`,`created_at`,`updated_at`) values 
(1,3,'undur','085666333999','Jl. Sidoarjo',NULL,NULL);

/*Table structure for table `migrations` */

DROP TABLE IF EXISTS `migrations`;

CREATE TABLE `migrations` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `migration` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*Data for the table `migrations` */

insert  into `migrations`(`id`,`migration`,`batch`) values 
(1,'2014_10_12_000000_create_users_table',1),
(2,'2014_10_12_100000_create_password_resets_table',1);

/*Table structure for table `owner` */

DROP TABLE IF EXISTS `owner`;

CREATE TABLE `owner` (
  `owner_id` int(11) NOT NULL AUTO_INCREMENT,
  `users_id` int(11) DEFAULT NULL,
  `owner_nama` varchar(100) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`owner_id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

/*Data for the table `owner` */

insert  into `owner`(`owner_id`,`users_id`,`owner_nama`,`created_at`,`updated_at`) values 
(1,1,'harif setyonon',NULL,NULL);

/*Table structure for table `password_resets` */

DROP TABLE IF EXISTS `password_resets`;

CREATE TABLE `password_resets` (
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  KEY `password_resets_email_index` (`email`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*Data for the table `password_resets` */

/*Table structure for table `pelanggan` */

DROP TABLE IF EXISTS `pelanggan`;

CREATE TABLE `pelanggan` (
  `pelanggan_id` int(11) NOT NULL AUTO_INCREMENT,
  `pelanggan_email` varchar(100) DEFAULT NULL,
  `pelanggan_password` text,
  `pelanggan_nama` varchar(100) DEFAULT NULL,
  `pelanggan_nohp` varchar(20) DEFAULT NULL,
  `pelanggan_alamat` varchar(200) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`pelanggan_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `pelanggan` */

/*Table structure for table `pemesanan` */

DROP TABLE IF EXISTS `pemesanan`;

CREATE TABLE `pemesanan` (
  `pemesanan_id` int(11) NOT NULL AUTO_INCREMENT,
  `pelanggan_id` int(11) DEFAULT NULL,
  `marketing_id` int(11) DEFAULT NULL,
  `produk_id` int(11) DEFAULT NULL,
  `pemesanan_tgl` date DEFAULT NULL,
  `pembayaran_tgl` date DEFAULT NULL,
  `pemesanan_qty` int(11) DEFAULT NULL,
  `pemesanan_harga` int(11) DEFAULT NULL,
  `pemesanan_total` int(11) DEFAULT NULL,
  `pemesanan_status_pembayaran` enum('1','2','3') DEFAULT NULL,
  `pemesanan_status_pengerjaan` enum('1','2','3') DEFAULT NULL,
  `pemesanan_bukti_pengerjaan` text,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`pemesanan_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `pemesanan` */

/*Table structure for table `pemesanan_detail` */

DROP TABLE IF EXISTS `pemesanan_detail`;

CREATE TABLE `pemesanan_detail` (
  `pemesanan_detail_id` int(11) NOT NULL AUTO_INCREMENT,
  `pemesanan_id` int(11) DEFAULT NULL,
  `pemesanan_detail_lebar_bahu` int(11) DEFAULT NULL,
  `pemesanan_detail_ukuran_dada` int(11) DEFAULT NULL,
  `pemesanan_detail_ukuran_pinggang` int(11) DEFAULT NULL,
  `pemesanan_detail_ukuran_pinggul` int(11) DEFAULT NULL,
  `pemesanan_detail_lebar_lengan` int(11) DEFAULT NULL,
  `pemesanan_detail_panjang_lengan` int(11) DEFAULT NULL,
  `pemesanan_detail_panjang_rok` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`pemesanan_detail_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `pemesanan_detail` */

/*Table structure for table `produk` */

DROP TABLE IF EXISTS `produk`;

CREATE TABLE `produk` (
  `produk_id` int(11) NOT NULL AUTO_INCREMENT,
  `kategori_id` int(11) DEFAULT NULL,
  `produk_nama` varchar(200) DEFAULT NULL,
  `produk_harga` int(11) DEFAULT NULL,
  `produk_deskripsi` text,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`produk_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `produk` */

/*Table structure for table `produk_gambar` */

DROP TABLE IF EXISTS `produk_gambar`;

CREATE TABLE `produk_gambar` (
  `produk_gambar_id` int(11) NOT NULL AUTO_INCREMENT,
  `produk_id` int(11) DEFAULT NULL,
  `produk_gambar_gambar` text,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`produk_gambar_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `produk_gambar` */

/*Table structure for table `users` */

DROP TABLE IF EXISTS `users`;

CREATE TABLE `users` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `level` enum('1','2','3') COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `users_email_unique` (`email`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*Data for the table `users` */

insert  into `users`(`id`,`name`,`email`,`password`,`level`,`remember_token`,`created_at`,`updated_at`) values 
(1,'harif','harif@gmail.com','$2y$10$SZBoz2bFSL.Ap//KylzgMurOaFrCbVx5/NBSnvCuocZYXvdiX.W0u','1','kKgZWYgszQoNlKPFlH2NDdsG6ELQMVvKs0m3riRXKIYJD3i8XgCYkr97IeAA',NULL,NULL),
(2,'mudlofar','mudlofar@gmail.com','$2y$10$SZBoz2bFSL.Ap//KylzgMurOaFrCbVx5/NBSnvCuocZYXvdiX.W0u','2','yYVHxnlH0mL3sKfX8GgNaVpioqJkR5MzJmZo6log78rN0ZY8Unkfomdm9Z1o',NULL,NULL),
(3,'uun','uun@gmail.com','$2y$10$SZBoz2bFSL.Ap//KylzgMurOaFrCbVx5/NBSnvCuocZYXvdiX.W0u','3','dujf1Ig78i5daCDsRza4YD4xFeRP5sKJrfUHm13R2KLfba3ukdVoeLReJyRi',NULL,NULL);

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
