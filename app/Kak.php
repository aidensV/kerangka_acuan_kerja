<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Kak extends Model
{
  protected $table = 'kak';
  protected $primaryKey = 'idkak';
  public $incrementing = false;
}
