<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use Hash;
use DB;

class login extends Controller
{
function masuk(Request $kiriman){

$password = Hash::make($kiriman->password);

$users = DB::table('users')
         ->select(DB::raw('*'))
         ->where('email',$kiriman->username)
         ->where('password',$password)
         ->get();

// dd($password);

foreach ($users as $key) {
  $level = $key->level;
}

if(count($users)>0){
  if($level == '31'){
    Auth::guard('validatorinspektorat')->LoginUsingId($users[0]['id']);
    return redirect('/inspektorat');
  }else if($level == '32'){
    Auth::guard('validatorbapeda')->LoginUsingId($users[0]['id']);
    return redirect('/bapeda');
  }else if($level == '33'){
    Auth::guard('validatorbkaad')->LoginUsingId($users[0]['id']);
    return redirect('/bkaad');
  }

}else{
  //gagal login
  return "login gagal";
}

    }



}
