<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;

class ValidatorBapedaAgendaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
     public function index()
     {
         return view('validatorbapeda.agenda.index');
     }

     /**
      * Show the form for creating a new resource.
      *
      * @return \Illuminate\Http\Response
      */
     public function create()
     {
         //
     }

     /**
      * Store a newly created resource in storage.
      *
      * @param  \Illuminate\Http\Request  $request
      * @return \Illuminate\Http\Response
      */
     public function store(Request $request)
     {
         //
     }

     /**
      * Display the specified resource.
      *
      * @param  int  $id
      * @return \Illuminate\Http\Response
      */
     public function show($id)
     {
         //
     }

     /**
      * Show the form for editing the specified resource.
      *
      * @param  int  $id
      * @return \Illuminate\Http\Response
      */
     public function edit($id)
     {
         //
     }

     /**
      * Update the specified resource in storage.
      *
      * @param  \Illuminate\Http\Request  $request
      * @param  int  $id
      * @return \Illuminate\Http\Response
      */
     public function update(Request $request, $id)
     {
         //
     }

     /**
      * Remove the specified resource from storage.
      *
      * @param  int  $id
      * @return \Illuminate\Http\Response
      */
     public function destroy($id)
     {
         //
     }

     public function ambil_agenda_sudah_terlaksanan()
     {
       $ambil_agenda = DB::table('agenda')
       ->select(DB::raw('agenda.*,satker.*'))
       ->join('satker','satker.idsatker','=','agenda.idsatker')
                  ->where('statuspengerjaan','=','3')
                  ->orderBy('agenda.id')
                  ->get();

       // $dt = array();
       foreach ($ambil_agenda as $key ) {
       $id = $key->id;
       $title = $key->title;
       $start = $key->start;
       $end = $key->end;
       }

       return($ambil_agenda);
       // return view('layouts.inspektorat.master',['ambil_agenda'=>$ambil_agenda]);
       // array_push($dt,$data);

     }
     public function ambil_agenda_belum_terlaksanan()
     {
       $ambil_agenda = DB::table('agenda')
       ->select(DB::raw('agenda.*,satker.*'))
       ->join('satker','satker.idsatker','=','agenda.idsatker')
                  ->where('statuspengerjaan','=','1')
                  ->orderBy('agenda.id')
                  ->get();

       // $dt = array();
       foreach ($ambil_agenda as $key ) {
       $id = $key->id;
       $title = $key->title;
       $start = $key->start;
       $end = $key->end;
       }

       return($ambil_agenda);
       // return view('layouts.inspektorat.master',['ambil_agenda'=>$ambil_agenda]);
       // array_push($dt,$data);

     }
     public function ambil_agenda_proses_pelaksanaan()
     {
       $ambil_agenda = DB::table('agenda')
       ->select(DB::raw('agenda.*,satker.*'))
       ->join('satker','satker.idsatker','=','agenda.idsatker')
                  ->where('statuspengerjaan','=','2')
                  ->orderBy('agenda.id')
                  ->get();

       // $dt = array();
       foreach ($ambil_agenda as $key ) {
       $id = $key->id;
       $title = $key->title;
       $start = $key->start;
       $end = $key->end;
       }

       return($ambil_agenda);
       // return view('layouts.inspektorat.master',['ambil_agenda'=>$ambil_agenda]);
       // array_push($dt,$data);

     }
     public function ambil_data_agenda()
     {
       $ambil_agenda = DB::table('agenda')
                  ->select(DB::raw('*'))
                  ->orderBy('agenda.id')
                  ->get();

       // $dt = array();
       foreach ($ambil_agenda as $key ) {
       $id = $key->id;
       $title = $key->title;
       $start = $key->start;
       $end = $key->end;
       }

       // return($ambil_agenda);
       return response()->json($ambil_agenda);
       // array_push($dt,$data);

     }
     public function tampil_per_kegiatan($id)
     {
       // $tampil_kegiatan_per_hari = $request['id'];
       // dd($tampil_kegiatan_per_hari);
       $detailagendaperkegiatan = DB::table('agenda')
                ->select(DB::raw('*'))
                ->where('id','=',$id)
                ->orderBy('agenda.id')
                ->get();

       foreach ($detailagendaperkegiatan as $key) {
                  $idkak = $key->idkak;
                  $namakegiatan = $key->title;
                }

       $detailagenda = DB::table('agenda')
                         ->select(DB::raw('*'))
                         ->where('idkak','=',$idkak)
                         ->orderBy('agenda.id')
                         ->get();

       return view('inspektorat.detailagenda.index',['detailagenda'=>$detailagenda,'namakegiatan'=>$namakegiatan]);
       // array_push($dt,$data);

     }

     public function tampil_per_hari($id)
     {
       // $tampil_kegiatan_per_hari = $request['id'];
       // dd($tampil_kegiatan_per_hari);
       $ambil_agenda_per_hari = DB::table('agenda')
                  ->select(DB::raw('*'))
                  ->where('agenda.id','=', $id)
                  ->orderBy('agenda.id')
                  ->get();
       return($ambil_agenda_per_hari);
       // array_push($dt,$data);

     }
}
