<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;

class AdminProdukRegulerController extends Controller
{

  protected $pesan = array(
      'kode_produk.required' => 'Isikan Kode Produk',
      'kategori_produk.required' => 'Pilih Kategori Produk',
      'nama_produk.required' => 'Isikan Nama Produk',
      'isi_produk.required' => 'Isikan Isi Produk',
      'berat_produk.required' => 'Isikan Berat Produk',
      'dimensi_produk.required' => 'Isikan Dimensi Produk',
      'harga_produk.required' => 'Isikan Harga Produk',
      'gambar.required' => 'Isikan Gambar Produk',
      'keterangan_produk.required' => 'Isikan Keterangan Produk'


      );

  protected $aturan = array(
      'kode_produk' => 'required',
      'kategori_produk' => 'required',
      'nama_produk' => 'required',
      'isi_produk' => 'required',
      'berat_produk' => 'required',
      'dimensi_produk' => 'required',
      'harga_produk' => 'required',
      'gambar' => 'required',
      'keterangan_produk' => 'required'
      );


      protected $pesan_update = array(
          'kode_produk.required' => 'Isikan Kode Produk',
          'kategori_produk.required' => 'Pilih Kategori Produk',
          'nama_produk.required' => 'Isikan Nama Produk',
          'isi_produk.required' => 'Isikan Isi Produk',
          'berat_produk.required' => 'Isikan Berat Produk',
          'dimensi_produk.required' => 'Isikan Dimensi Produk',
          'harga_produk.required' => 'Isikan Harga Produk',
          'keterangan_produk.required' => 'Isikan Keterangan Produk'


          );

      protected $aturan_update = array(
          'kode_produk' => 'required',
          'kategori_produk' => 'required',
          'nama_produk' => 'required',
          'isi_produk' => 'required',
          'berat_produk' => 'required',
          'dimensi_produk' => 'required',
          'harga_produk' => 'required',
          'keterangan_produk' => 'required'
          );



    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
      $produk = DB::table('produk')->join('kategori_produk', 'kategori_produk.id_kategori', '=', 'produk.id_kategori')
      ->where('produk.jenis_kategori',"reguler")->orderBy('produk.id_produk', 'desc')->get();
      return view ('admin.produk_reguler.index',['produk'=>$produk]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
      $kategori= DB::table('kategori_produk')->where('jenis_kategori',"reguler")->get();
      return view('admin.produk_reguler.create',['kategori'=>$kategori]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, $this->aturan, $this->pesan);
        $kode_produk = $request->kode_produk;
        $kategori_produk = $request->kategori_produk;
        $nama_produk = $request->nama_produk;
        $isi_produk = $request->isi_produk;
        $berat_produk = $request->berat_produk;
        $dimensi_produk = $request->dimensi_produk;
        $harga_produk = $request->harga_produk;
        $keterangan_produk = $request->keterangan_produk;
        $gambar = $request->gambar;

        $this->validate($request, [
           'gambar' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048'
       ]);

       $input['gambar'] = time().'.'.$request->gambar->getClientOriginalExtension();
       $request->gambar->move(public_path('images'), $input['gambar']);



        DB::table('produk')->insert(['kode_produk' => $kode_produk, 'id_kategori' => $kategori_produk, 'nama_produk' => $nama_produk,
         'isi_produk' => $isi_produk, 'berat_produk' => $berat_produk, 'dimensi_produk' => $dimensi_produk, 'harga_produk' => $harga_produk,
         'keterangan_produk' => $keterangan_produk,'jenis_kategori' => "reguler" , 'gambar_produk' =>  $input['gambar']]);
        return redirect()->route('admin_produk_reguler.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
      $produk = DB::table('produk')->join('kategori_produk', 'kategori_produk.id_kategori', '=', 'produk.id_kategori')->orderBy('produk.id_kategori', 'asc')->where('id_produk',$id)->get();
      return view ('admin.produk_reguler.lihat',['produk'=>$produk]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
      $kategori= DB::table('kategori_produk')->get();
      $produk = DB::table('produk')->where('id_produk',$id)->get();
      return view ('admin.produk_reguler.edit',['produk'=>$produk, 'kategori' => $kategori]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
      $this->validate($request, $this->aturan_update, $this->pesan_update);

      $kode_produk = $request->kode_produk;
      $kategori_produk = $request->kategori_produk;
      $nama_produk = $request->nama_produk;
      $isi_produk = $request->isi_produk;
      $berat_produk = $request->berat_produk;
      $dimensi_produk = $request->dimensi_produk;
      $harga_produk = $request->harga_produk;
      $keterangan_produk = $request->keterangan_produk;
      // $gambar = $request->gambar;

       if ($request->hasFile('gambar')) {

          $image = \DB::table('produk')->where('id_produk', $id)->first();
          $file= $image->gambar_produk;
          $filename = public_path().'/images/'.$file;
          \File::delete($filename);

           $this->validate($request, [
               'gambar' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048'
           ]);
           $input['gambar'] = time().'.'.$request->gambar->getClientOriginalExtension();
           $request->gambar->move(public_path('images'), $input['gambar']);

           $produk = DB::table('produk')->where('id_produk',$id)->update(['kode_produk' => $kode_produk,
           'id_kategori' => $kategori_produk, 'nama_produk' => $nama_produk,'isi_produk' => $isi_produk,
           'berat_produk' => $berat_produk,'dimensi_produk' => $dimensi_produk,'harga_produk' => $harga_produk,
           'keterangan_produk' => $keterangan_produk, 'gambar_produk' =>  $input['gambar']]);

        }else{
          $produk = DB::table('produk')->where('id_produk',$id)->update(['kode_produk' => $kode_produk,
          'id_kategori' => $kategori_produk, 'nama_produk' => $nama_produk,'isi_produk' => $isi_produk,
          'berat_produk' => $berat_produk,'dimensi_produk' => $dimensi_produk,'harga_produk' => $harga_produk,
          'keterangan_produk' => $keterangan_produk]);

        }


       return redirect()->route('admin_produk_reguler.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
      $image = \DB::table('produk')->where('id_produk', $id)->first();
      $file= $image->gambar_produk;
      $filename = public_path().'/images/'.$file;
      \File::delete($filename);

      $blog = DB::table('produk')->where('id_produk',$id)->delete();
      return redirect()->route('admin_produk_reguler.index');
    }
}
