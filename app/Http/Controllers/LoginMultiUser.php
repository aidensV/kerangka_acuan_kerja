<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use Auth;
use App\Owner;
use App\Admin_produk;
use App\Marketing;
use Kreait\Firebase;
use Kreait\Firebase\Factory;
use Kreait\Firebase\ServiceAccount;
use View;

class LoginMultiUser extends Controller
{
  public function index()
  {

    $level = Auth::user()->level;
    $id = Auth::user()->id;

    // $owner_id_query = Owner::join('users', 'owner.users_id', '=', 'users.id')
    //                   ->where('users.id',$id)
    //                   ->get();
    // foreach ($owner_id_query as $key) {
    //   $owner_id = $key->owner_id;
    // }
    //
    // $admin_produk_id_query = Admin_produk::join('users', 'admin_produk.users_id', '=', 'users.id')
    //                   ->where('users.id',$id)
    //                   ->get();
    // foreach ($admin_produk_id_query as $key2) {
    //   $admin_produk_id = $key2->admin_produk_id;
    // }
    //
    // $marketing_id_query = Marketing::join('users', 'marketing.users_id', '=', 'users.id')
    //                   ->where('users.id',$id)
    //                   ->get();
    // foreach ($marketing_id_query as $key3) {
    //   $marketing_id = $key3->marketing_id;
    // }

    $tgl_sekarang = date('d-m-Y');

    $serviceAccount = ServiceAccount::fromJsonFile(__DIR__.'/notifikasi-17e59-firebase-adminsdk-u9iha-67d45ca394.json');
    $firebase = (new Factory)
    ->withServiceAccount($serviceAccount)
    ->withDatabaseUri('https://notifikasi-17e59.firebaseio.com/')
    ->create();

    $database = $firebase->getDatabase();

    // $newPost = $database
    // ->getReference('notifikasi_status_kak')
    // ->push([
    // 'notifikasi' => 'Notikasi 2' ,
    // 'tanggal' => $tgl_sekarang,
    // 'idkak' => '3'
    // ]);

    $newPost = $database
    ->getReference('notifikasi_status_kak')
    ->getvalue();

    foreach ($newPost as $key) {
      $all_subject[] = $key;
    }



    if ($level == "1") { //satker
      // $view = View::make('satker.dashboard.index');
      // $view->nest('layouts.satker.master',['value'=>$all_subject]);
      //
      // return redirect('satkerindex');
      // return $view;
      return view('satker.dashboard.index');
    }elseif ($level == "2") { //inspektorat
      return view('inspektorat.dashboard.index');
    }elseif ($level == "31") { //validator
      // return view('validatorinspektorat.dashboard.index');
      return redirect('/validatorinspektorat');
    }elseif ($level == "32") { //validator
      // return view('validatorbapeda.dashboard.index');
      return redirect('/validatorbapeda');
    }elseif ($level == "33") { //validator
      // return view('validatorbkaad.dashboard.index');
      return redirect('/validatorbkaad');
    }elseif ($level == "4") { //operator
      return view('operator.dashboard.index');
    }

// return view('admin.index');

















    // $reguler = DB::table('produk')->where('jenis_kategori',"reguler")->count();
    // $edisi = DB::table('produk')->where('jenis_kategori',"edisi")->count();
    // $testimoni = DB::table('testimoni')->count();
    // return view('admin.index', compact('reguler','edisi','testimoni'));
  }
}
