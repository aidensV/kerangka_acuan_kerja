<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Kreait\Firebase;
use Kreait\Firebase\Factory;
use Kreait\Firebase\ServiceAccount;
use DB;

class SatkerDashboardController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
      $tgl_sekarang = date('d-m-Y');

      $serviceAccount = ServiceAccount::fromJsonFile(__DIR__.'/notifikasi-17e59-firebase-adminsdk-u9iha-67d45ca394.json');
      $firebase = (new Factory)
      ->withServiceAccount($serviceAccount)
      ->withDatabaseUri('https://notifikasi-17e59.firebaseio.com/')
      ->create();

      $database = $firebase->getDatabase();

      // $newPost = $database
      // ->getReference('notifikasi_status_kak')
      // ->push([
      // 'notifikasi' => 'Notikasi 2' ,
      // 'tanggal' => $tgl_sekarang,
      // 'idkak' => '3'
      // ]);

      $newPost = $database
      ->getReference('notifikasi_status_kak')
      ->getvalue();

      foreach ($newPost as $key) {
        $all_subject[] = $key;
      }
        return view('satker.dashboard.index',['value'=>$all_subject]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
