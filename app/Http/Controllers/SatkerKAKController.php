<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Auth;
use PDF;
use DB;

use App\Kak;
use App\revisi;
use App\agenda;

class SatkerKAKController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
      $iduser = Auth::user()->iduser;
      $kak = DB::table('kak')
                 ->select(DB::raw('*'))
                 ->where('idsatker','=',$iduser)
                 ->orderBy('kak.idkak')
                 ->get();
        return view('satker.kak.index',['kak'=>$kak]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('satker.kak.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
      $iduser = Auth::user()->iduser;
      $selectMax = DB::table('kak')
                      ->select('idkak')
                      ->max('idkak')
                      ;
                      $getID = (int) substr($selectMax, 2, 3);
                      $char = "K";
                      $setID = $char . sprintf("%04s", $getID);
                      $setID++;

      $kak = new kak;
      $kak->idkak = $setID;
      $kak->idsatker = $iduser;
      $kak->judul = $request['kak_program'];
      $kak->kak_kegiatan = $request['kak_kegiatan'];
      $kak->latarbelakang = $request['kak_latarbelakang'];
      $kak->maksud = $request['kak_maksud'];
      $kak->tujuan = $request['kak_tujuan'];
      $kak->metodepelaksanaan = $request['kak_ruang_lingkup'];
      $kak->penanggungjawab = $request['kak_sasaran'];
      $kak->tempatpelaksanaan = $request['kak_lokasi'];
      $kak->kak_keluaran = $request['kak_keluaran'];
      $kak->kak_anggaran = $request['kak_anggaran'];
      $kak->kak_penutup = $request['kak_penutup'];
      $kak->status = '1';
      $kak->kak_status_bkaad = '1';
      $kak->kak_status_bapeda = '1';
      $kak->kak_status_inspektorat = '1';
      $kak->save();
      $selectMaxNew =kak::max('idkak');

      return redirect()->route('satkeragenda.show',[$selectMaxNew]);

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return('a');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
      $kak = Kak::find($id);

      $revisi_program = DB::table('revisi')
             ->select(DB::raw('*'))
             ->where('revisi.idkak','=',$id)
             ->where('revisi.babkakrevisi','=','1')
             ->get();

      $revisi_latarbelakang = DB::table('revisi')
              ->select(DB::raw('*'))
              ->where('revisi.idkak','=',$id)
              ->where('revisi.babkakrevisi','=','2')
              ->get();

      $revisi_maksutdantujuan = DB::table('revisi')
              ->select(DB::raw('*'))
              ->where('revisi.idkak','=',$id)
              ->where('revisi.babkakrevisi','=','3')
              ->get();

      $revisi_ruanglingkup = DB::table('revisi')
              ->select(DB::raw('*'))
              ->where('revisi.idkak','=',$id)
              ->where('revisi.babkakrevisi','=','4')
              ->get();

      $revisi_sasaran = DB::table('revisi')
              ->select(DB::raw('*'))
              ->where('revisi.idkak','=',$id)
              ->where('revisi.babkakrevisi','=','5')
              ->get();

      $revisi_lokasikegiatan = DB::table('revisi')
              ->select(DB::raw('*'))
              ->where('revisi.idkak','=',$id)
              ->where('revisi.babkakrevisi','=','6')
              ->get();

      $revisi_keluaran = DB::table('revisi')
              ->select(DB::raw('*'))
              ->where('revisi.idkak','=',$id)
              ->where('revisi.babkakrevisi','=','7')
              ->get();

      $revisi_anggaran = DB::table('revisi')
              ->select(DB::raw('*'))
              ->where('revisi.idkak','=',$id)
              ->where('revisi.babkakrevisi','=','8')
              ->get();

      $revisi_penutup = DB::table('revisi')
              ->select(DB::raw('*'))
              ->where('revisi.idkak','=',$id)
              ->where('revisi.babkakrevisi','=','9')
              ->get();

      // return view('satker.kak.edit',compact('kak')
      return view('satker.kak.edit',
      ['revisi_program'=>$revisi_program,
      'revisi_latarbelakang'=>$revisi_latarbelakang,
      'revisi_maksutdantujuan'=>$revisi_maksutdantujuan,
      'revisi_ruanglingkup'=>$revisi_ruanglingkup,
      'revisi_sasaran'=>$revisi_sasaran,
      'revisi_lokasikegiatan'=>$revisi_lokasikegiatan,
      'revisi_keluaran'=>$revisi_keluaran,
      'revisi_anggaran'=>$revisi_anggaran,
      'revisi_penutup'=>$revisi_penutup,
      'kak'=>$kak]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
      $kak = Kak::find($id);
      $kak->judul = $request['kak_program'];
      $kak->kak_kegiatan = $request['kak_kegiatan'];
      $kak->latarbelakang = $request['kak_latarbelakang'];
      $kak->maksud = $request['kak_maksud'];
      $kak->tujuan = $request['kak_tujuan'];
      $kak->metodepelaksanaan = $request['kak_ruang_lingkup'];
      $kak->penanggungjawab = $request['kak_sasaran'];
      $kak->tempatpelaksanaan = $request['kak_lokasi'];
      $kak->kak_keluaran = $request['kak_keluaran'];
      $kak->kak_anggaran = $request['kak_anggaran'];
      $kak->kak_penutup = $request['kak_penutup'];
      $kak->update();

      return redirect('satkerkak');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
      $kak = Kak::find($id);
      $kak->delete();

      return redirect('satkerkak');
    }

    public function ubahStatus($id)
    {
      $kak = Kak::find($id);
      $kak->status = '2';
      $kak->update();

      $kak = DB::table('kak')
             ->select(DB::raw('*'))
             ->where('idkak','=',$id)
             ->get();
      foreach ($kak as $key) {
               $kakstatusbkaad = $key->kak_status_bkaad;
               $kakstatusbapeda = $key->kak_status_bapeda;
               $kakstatusinspektorat = $key->kak_status_inspektorat;
      }

      if($kakstatusbkaad == '2'){
        $kak = Kak::find($id);
        $kak->kak_status_bkaad = '1';
        $kak->update();
      }

      if($kakstatusbapeda == '2'){
        $kak = Kak::find($id);
        $kak->kak_status_bapeda = '1';
        $kak->update();
      }

      if($kakstatusinspektorat == '2'){
        $kak = Kak::find($id);
        $kak->kak_status_inspektorat = '1';
        $kak->update();
      }

      return redirect('satkerkak');
    }

    public function detailagenda($id)
    {
        $namakegiatan = "";
        $detailagenda = DB::table('agenda')
                 ->select(DB::raw('*'))
                 ->where('idkak','=',$id)
                 ->orderBy('agenda.id')
                 ->get();
        return view('satker.detailagenda.index',['detailagenda'=>$detailagenda,'namakegiatan'=>$namakegiatan]);
    }
    public function satkerkakpdf($id)
    {
    $kak = DB::table('kak')
                 ->select(DB::raw('*'))
                 ->where('kak.idkak', '=', $id)
                 ->get();
    $agenda = DB::table('agenda')
                ->select(DB::raw('*'))
                ->where('agenda.idkak', '=', $id)
                ->get();

    $pdf = PDF::loadView('satker.kakpdf.index',['kak'=>$kak,'id'=>$id,'agenda'=>$agenda]);
    $pdf->setPaper('a4', 'portrait');
    return $pdf->stream();
    }
}
