<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use PDF;

class InspektoratKAKController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
      $satker = DB::table('satker')
                 ->select(DB::raw('*'))
                 ->orderBy('satker.idsatker')
                 ->get();
        return view('inspektorat.kak.index',['satker'=>$satker]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
    public function detailkakpersatker($id)
    {
        $kak = DB::table('kak')
                 ->select(DB::raw('*'))
                 ->where('idsatker','=',$id)
                 ->orderBy('kak.idkak')
                 ->get();
        return view('inspektorat.detailkak.index',['kak'=>$kak]);
    }
    public function detailagenda($id)
    {
        $namakegiatan = "";
        $detailagenda = DB::table('agenda')
               ->select(DB::raw('*'))
               ->where('idkak','=',$id)
               ->orderBy('agenda.id')
               ->get();
               // dd($detailagenda);
        return view('inspektorat.detailagenda.index',['detailagenda'=>$detailagenda,'namakegiatan'=>$namakegiatan]);


    }
    public function inspektoratkakpdf($id)
    {
    $kak = DB::table('kak')
                 ->select(DB::raw('*'))
                 ->where('kak.idkak', '=', $id)
                 ->get();
    $agenda = DB::table('agenda')
                ->select(DB::raw('*'))
                ->where('agenda.idkak', '=', $id)
                ->get();

    $pdf = PDF::loadView('inspektorat.kakpdf.index',['kak'=>$kak,'id'=>$id,'agenda'=>$agenda]);
    $pdf->setPaper('a4', 'portrait');
    return $pdf->stream();
    }
}
