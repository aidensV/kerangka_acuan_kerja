<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class MenuController extends Controller
{
    public function beranda()
    {
        $kategori_edisi = DB::table('kategori_produk')->where('jenis_kategori','edisi')->get();
        $kategori_reguler = DB::table('kategori_produk')->where('jenis_kategori','reguler')->get();
        
        $produk = DB::table('produk')->skip(0)->take(8)->orderBy('id_produk','desc')->get();
        $slideshow1 = DB::table('slideshow')->where('id_slideshow',1)->get();
        $slideshow2 = DB::table('slideshow')->where('id_slideshow',2)->get();
        $slideshow3 = DB::table('slideshow')->where('id_slideshow',3)->get();
        return view('user/index',['data'=>$produk,'slide1'=>$slideshow1,'slide2'=>$slideshow2,'slide3'=>$slideshow3,'kategori_reguler'=>$kategori_reguler,'kategori_edisi'=>$kategori_edisi]);
    }
    public function carabelanja()
    {
        $kategori_edisi = DB::table('kategori_produk')->where('jenis_kategori','edisi')->get();
        $kategori_reguler = DB::table('kategori_produk')->where('jenis_kategori','reguler')->get();

        return view('user/carabelanja',['kategori_reguler'=>$kategori_reguler,'kategori_edisi'=>$kategori_edisi]);
    }
    public function testimoni()
    {
        $kategori_edisi = DB::table('kategori_produk')->where('jenis_kategori','edisi')->get();
        $kategori_reguler = DB::table('kategori_produk')->where('jenis_kategori','reguler')->get();

        $batas=4;
        $testimoni = DB::table('testimoni')->orderBy('id_testimoni','desc')->paginate($batas);
        $no=$batas*($testimoni->currentPage()-1);
        return view('user/testimoni',['data'=>$testimoni,'no'=>$no,'kategori_reguler'=>$kategori_reguler,'kategori_edisi'=>$kategori_edisi]);
    }
    public function tos()
    {
        $kategori_edisi = DB::table('kategori_produk')->where('jenis_kategori','edisi')->get();
        $kategori_reguler = DB::table('kategori_produk')->where('jenis_kategori','reguler')->get();
        
        return view('user/tos',['kategori_reguler'=>$kategori_reguler,'kategori_edisi'=>$kategori_edisi]);
    }
    public function about()
    {
        $kategori_edisi = DB::table('kategori_produk')->where('jenis_kategori','edisi')->get();
        $kategori_reguler = DB::table('kategori_produk')->where('jenis_kategori','reguler')->get();
        
        return view('user/about',['kategori_reguler'=>$kategori_reguler,'kategori_edisi'=>$kategori_edisi]);
    }
}
