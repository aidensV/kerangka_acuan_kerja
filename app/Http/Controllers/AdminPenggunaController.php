<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Pengguna;
use Redirect;
use DB;

class AdminPenggunaController extends Controller
{

  protected $pesan = array(
      'nama.required' => 'Isikan Nama Pengguna',
      'email.required' => 'Isikan Email Pengguna',
      'password.required' => 'Isikan Password'
      );

  protected $aturan = array(
      'nama' => 'required',
      'email' => 'required',
      'password' => 'required'
      );

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
      $pengguna = Pengguna::orderBy('id', 'desc')->get();
      return view('admin.pengguna.index', compact('pengguna'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.pengguna.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
      $this->validate($request, $this->aturan, $this->pesan);

       $agenda = new Pengguna;
       $agenda->name = $request['nama'];
       $agenda->email = $request['email'];
       $agenda->password = bcrypt($request['password']);
       $agenda->save();

      return Redirect::route('admin_users.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
      $pengguna = Pengguna::find($id);
      return view('admin.pengguna.edit', compact('pengguna'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, $this->aturan, $this->pesan);

        $pengguna =Pengguna::find($id);
        $pengguna->name = $request['nama'];
        $pengguna->email = $request['email'];
        $pengguna->password = bcrypt($request['password']);


        $pengguna->update();

        return Redirect::route('admin_users.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
      $pengguna = Pengguna::find($id);
      $pengguna->delete();
      return Redirect::route('admin_users.index');
    }
}
