<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use App\Kak;
use Auth;
use App\revisi;

use Kreait\Firebase;
use Kreait\Firebase\Factory;
use Kreait\Firebase\ServiceAccount;

class ValidatorInspektoratKAKController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
     public function index()
     {
       $satker = DB::table('satker')
                  ->select(DB::raw('*'))
                  ->orderBy('satker.idsatker')
                  ->get();
         return view('validatorinspektorat.kak.index',['satker'=>$satker]);
     }

     /**
      * Show the form for creating a new resource.
      *
      * @return \Illuminate\Http\Response
      */
     public function create()
     {
         //
     }

     /**
      * Store a newly created resource in storage.
      *
      * @param  \Illuminate\Http\Request  $request
      * @return \Illuminate\Http\Response
      */
     public function store(Request $request)
     {
         //
     }

     /**
      * Display the specified resource.
      *
      * @param  int  $id
      * @return \Illuminate\Http\Response
      */
     public function show($id)
     {
         //
     }

     /**
      * Show the form for editing the specified resource.
      *
      * @param  int  $id
      * @return \Illuminate\Http\Response
      */
     public function edit($id)
     {
         //
     }

     /**
      * Update the specified resource in storage.
      *
      * @param  \Illuminate\Http\Request  $request
      * @param  int  $id
      * @return \Illuminate\Http\Response
      */
     public function update(Request $request, $id)
     {
         //
     }

     /**
      * Remove the specified resource from storage.
      *
      * @param  int  $id
      * @return \Illuminate\Http\Response
      */
     public function destroy($id)
     {
         //
     }
     public function detailkakpersatker($id)
     {
         $kak = DB::table('kak')
                  ->select(DB::raw('*'))
                  ->where('idsatker','=',$id)
                  ->orderBy('kak.idkak')
                  ->get();
         return view('validatorinspektorat.detailkak.index',['kak'=>$kak]);
     }
     public function detailagenda($id)
     {
         $namakegiatan = "";
         $detailagenda = DB::table('agenda')
                ->select(DB::raw('*'))
                ->where('idkak','=',$id)
                ->orderBy('agenda.id')
                ->get();
         return view('validatorinspektorat.detailagenda.index',['detailagenda'=>$detailagenda,'namakegiatan'=>$namakegiatan]);
     }
     public function updatetersetujuipervalidator(Request $request, $id)
     {
       $level = Auth::user()->level;


       $tgl_sekarang = date('d-m-Y');

       $serviceAccount = ServiceAccount::fromJsonFile(__DIR__.'/notifikasi-17e59-firebase-adminsdk-u9iha-67d45ca394.json');
       $firebase = (new Factory)
       ->withServiceAccount($serviceAccount)
       ->withDatabaseUri('https://notifikasi-17e59.firebaseio.com/')
       ->create();

       $database = $firebase->getDatabase();





       if($level == '33'){
         $kak = Kak::find($id);
         $kak->kak_status_bkaad = '3';
         $kak->update();

         $kak = DB::table('kak')
                ->select(DB::raw('*'))
                ->where('idkak','=',$id)
                ->get();

         foreach ($kak as $key) {
           $judul_kak = $key->judul;
           $id_kak = $key->idkak;
           $id_satker = $key->idsatker;
         }

         $newPost = $database
         ->getReference('notifikasi_status_kak')
         ->push([
         'notifikasi' => 'bkaad telah mensetujui KAK ('.$judul_kak.')' ,
         'tanggal' => $tgl_sekarang,
         'idkak' => $id_kak,
         'idsatker' => $id_satker,
         'status' => 1
         ]);
       }

       if($level == '32'){
         $kak = Kak::find($id);
         $kak->kak_status_bapeda = '3';
         $kak->update();

         $kak = DB::table('kak')
                ->select(DB::raw('*'))
                ->where('idkak','=',$id)
                ->get();

         foreach ($kak as $key) {
           $judul_kak = $key->judul;
           $id_kak = $key->idkak;
           $id_satker = $key->idsatker;
         }

         $newPost = $database
         ->getReference('notifikasi_status_kak')
         ->push([
         'notifikasi' => 'bapeda telah mensetujui KAK ('.$judul_kak.')',
         'tanggal' => $tgl_sekarang,
         'idkak' => $id_kak,
         'idsatker' => $id_satker,
         'status' => 1
         ]);
       }

       if($level == '31'){
         $kak = Kak::find($id);
         $kak->kak_status_inspektorat = '3';
         $kak->update();

         $kak = DB::table('kak')
                ->select(DB::raw('*'))
                ->where('idkak','=',$id)
                ->get();

         foreach ($kak as $key) {
           $judul_kak = $key->judul;
           $id_kak = $key->idkak;
           $id_satker = $key->idsatker;
         }

         $newPost = $database
         ->getReference('notifikasi_status_kak')
         ->push([
         'notifikasi' => 'inspektorat telah mensetujui KAK ('.$judul_kak.')' ,
         'tanggal' => $tgl_sekarang,
         'idkak' => $id_kak,
         'idsatker' => $id_satker,
         'status' => 1
         ]);
       }

       $status_per_validator_kak = DB::table('kak')
                      ->select(DB::raw('*'))
                      ->where('idkak','=',$id_kak)
                      ->get();
       foreach ($status_per_validator_kak as $key) {
                 $kakstatusbkaad = $key->kak_status_bkaad;
                 $kakstatusbapeda = $key->kak_status_bapeda;
                 $kakstatusinspektorat = $key->kak_status_inspektorat;
       }

       if($kakstatusbkaad == '3' && $kakstatusbapeda == '3' && $kakstatusinspektorat == '3'){
         $status__kak = kak::find($id_kak);
         $status__kak->status = '3';
         $status__kak->update();

         // $id__revisi = DB::table('revisi')
         //                ->select(DB::raw('*'))
         //                ->where('idkak','=',$id_kak)
         //                ->get();
         // foreach ($id__revisi as $key) {
         //           $id___revisi = $key->kak_status_bkaad;
         // }
         //
         // $status__validator = revisi::find($id___revisi);
         // $status__validator->statusrevisi = '3';
         // $status__validator->update();
       }

       if($kakstatusbkaad == '3' && $kakstatusbapeda == '3' && $kakstatusinspektorat == '2'){
         $status__kak = kak::find($id_kak);
         $status__kak->status = '1';
         $status__kak->update();
       }

       if($kakstatusbkaad == '3' && $kakstatusbapeda == '2' && $kakstatusinspektorat == '2'){
         $status__kak = kak::find($id_kak);
         $status__kak->status = '1';
         $status__kak->update();
       }

       if($kakstatusbkaad == '2' && $kakstatusbapeda == '2' && $kakstatusinspektorat == '2'){
         $status__kak = kak::find($id_kak);
         $status__kak->status = '1';
         $status__kak->update();
       }

       if($kakstatusbkaad == '2' && $kakstatusbapeda == '2' && $kakstatusinspektorat == '3'){
         $status__kak = kak::find($id_kak);
         $status__kak->status = '1';
         $status__kak->update();
       }

       if($kakstatusbkaad == '2' && $kakstatusbapeda == '3' && $kakstatusinspektorat == '3'){
         $status__kak = kak::find($id_kak);
         $status__kak->status = '1';
         $status__kak->update();
       }
       if($kakstatusbkaad == '2' && $kakstatusbapeda == '3' && $kakstatusinspektorat == '2'){
         $status__kak = kak::find($id_kak);
         $status__kak->status = '1';
         $status__kak->update();
       }
       if($kakstatusbkaad == '3' && $kakstatusbapeda == '2' && $kakstatusinspektorat == '3'){
         $status__kak = kak::find($id_kak);
         $status__kak->status = '1';
         $status__kak->update();
       }

       $detailagenda = DB::table('kak')
              ->select(DB::raw('*'))
              ->where('idkak','=',$id)
              ->get();
       foreach ($detailagenda as $key) {
         $idsatker = $key->idsatker;
       }
       return redirect('validatorinspektorattampildetailkakpersatker/'.$idsatker);
     }
     public function detailrevisipervalidator(Request $request, $id)
     {

       // $level = Auth::user()->level;

       $iduser = Auth::user()->iduser;




       $revisi = DB::table('revisi')
              ->select(DB::raw('*'))
              ->where('revisi.idkak','=',$id)
              ->where('revisi.iduser','=',$iduser)
              ->join('kak','kak.idkak','=','revisi.idkak')
              ->join('users','users.iduser','=','kak.idsatker')
              ->get();

       return view('validatorinspektorat.revisi.index',['revisi'=>$revisi],['idkak'=>$id]);
     }

     public function revisi(Request $request, $id)
     {
       return view('validatorinspektorat.revisi.tambahrevisi',['idkak'=>$id]);

     }

     public function tambahrevisi(Request $request)
     {
       $iduser = Auth::user()->iduser;

       $revisi = new Revisi;
       $revisi->idkak = $request['idkak'];
       $revisi->statusrevisi = '1';
       $revisi->iduser = $iduser;
       $revisi->detailrevisi = $request['revisi'];
       $revisi->babkakrevisi  = $request['babkakrevisi'];
       $revisi->save();

       // $revisi = $request['revisi'];
       // dd($revisi);

       $revisi = DB::table('revisi')
              ->select(DB::raw('*'))
              ->where('revisi.idkak','=',$request['idkak'])
              ->where('revisi.iduser','=',$iduser)
              ->join('kak','kak.idkak','=','revisi.idkak')
              ->join('users','users.iduser','=','kak.idsatker')
              ->get();

       $idkak = DB::table('kak')
                     ->select(DB::raw('*'))
                     ->where('idkak','=',$request['idkak'])
                     ->get();
              foreach ($idkak as $key) {
                $idsatker = $key->idsatker;
                $idkak = $key->idkak;
              }

       return view('validatorinspektorat.revisi.index',['revisi'=>$revisi],['idkak'=>$idkak]);

     }
     public function updaterevisibenar(Request $request,$id)
     {
       $iduser = Auth::user()->iduser;

       $revisi = Revisi::find($id);
       $revisi->statusrevisi = '2';
       $revisi->update();



      $idkakkak = DB::table('revisi')
                     ->select(DB::raw('*'))
                     ->where('idrevisi','=',$id)
                     ->get();
      foreach ($idkakkak as $key) {
                $idkak = $key->idkak;
      }

      $revisi = DB::table('revisi')
             ->select(DB::raw('*'))
             ->where('revisi.idkak','=',$idkak)
             ->where('revisi.iduser','=',$iduser)
             ->join('kak','kak.idkak','=','revisi.idkak')
             ->join('users','users.iduser','=','kak.idsatker')
             ->get();

      $status_revisi = DB::table('revisi')
             ->select(DB::raw('statusrevisi'))
             ->where('revisi.idkak','=',$idkak)
             ->where('revisi.iduser','=',$iduser)
             ->groupBy('statusrevisi')
             ->limit(1)
             ->get();

      foreach ($status_revisi as $key ) {
        $statusrevisi1 = $key->statusrevisi;
      }

      if($statusrevisi1 == '2'){
        $kak = kak::find($idkak);
        $kak->kak_status_inspektorat = '3';
        $kak->update();


      }

      $status_per_validator_kak = DB::table('kak')
                     ->select(DB::raw('*'))
                     ->where('idkak','=',$idkak)
                     ->get();
      foreach ($status_per_validator_kak as $key) {
                $kakstatusbkaad = $key->kak_status_bkaad;
                $kakstatusbapeda = $key->kak_status_bapeda;
                $kakstatusinspektorat = $key->kak_status_inspektorat;
      }

      if($kakstatusbkaad == '3' && $kakstatusbapeda == '3' && $kakstatusinspektorat == '3'){
        $status__kak = kak::find($id_kak);
        $status__kak->status = '3';
        $status__kak->update();

        // $id__revisi = DB::table('revisi')
        //                ->select(DB::raw('*'))
        //                ->where('idkak','=',$id_kak)
        //                ->get();
        // foreach ($id__revisi as $key) {
        //           $id___revisi = $key->kak_status_bkaad;
        // }
        //
        // $status__validator = revisi::find($id___revisi);
        // $status__validator->statusrevisi = '3';
        // $status__validator->update();
      }

      if($kakstatusbkaad == '3' && $kakstatusbapeda == '3' && $kakstatusinspektorat == '2'){
        $status__kak = kak::find($id_kak);
        $status__kak->status = '1';
        $status__kak->update();
      }

      if($kakstatusbkaad == '3' && $kakstatusbapeda == '2' && $kakstatusinspektorat == '2'){
        $status__kak = kak::find($id_kak);
        $status__kak->status = '1';
        $status__kak->update();
      }

      if($kakstatusbkaad == '2' && $kakstatusbapeda == '2' && $kakstatusinspektorat == '2'){
        $status__kak = kak::find($id_kak);
        $status__kak->status = '1';
        $status__kak->update();
      }

      if($kakstatusbkaad == '2' && $kakstatusbapeda == '2' && $kakstatusinspektorat == '3'){
        $status__kak = kak::find($id_kak);
        $status__kak->status = '1';
        $status__kak->update();
      }

      if($kakstatusbkaad == '2' && $kakstatusbapeda == '3' && $kakstatusinspektorat == '3'){
        $status__kak = kak::find($id_kak);
        $status__kak->status = '1';
        $status__kak->update();
      }
      if($kakstatusbkaad == '2' && $kakstatusbapeda == '3' && $kakstatusinspektorat == '2'){
        $status__kak = kak::find($id_kak);
        $status__kak->status = '1';
        $status__kak->update();
      }
      if($kakstatusbkaad == '3' && $kakstatusbapeda == '2' && $kakstatusinspektorat == '3'){
        $status__kak = kak::find($id_kak);
        $status__kak->status = '1';
        $status__kak->update();
      }

      return view('validatorinspektorat.revisi.index',['revisi'=>$revisi],['idkak'=>$idkak]);

     }
     public function ajukanrevisi(Request $request, $id)
     {

       $tgl_sekarang = date('d-m-Y');

       $serviceAccount = ServiceAccount::fromJsonFile(__DIR__.'/notifikasi-17e59-firebase-adminsdk-u9iha-67d45ca394.json');
       $firebase = (new Factory)
       ->withServiceAccount($serviceAccount)
       ->withDatabaseUri('https://notifikasi-17e59.firebaseio.com/')
       ->create();

       $database = $firebase->getDatabase();


         $kak = Kak::find($id);
         $kak->kak_status_inspektorat = '2';
         $kak->update();

         $status_per_validator_kak = DB::table('kak')
                        ->select(DB::raw('*'))
                        ->where('idkak','=',$id)
                        ->get();
         foreach ($status_per_validator_kak as $key) {
                   $kakstatusbkaad = $key->kak_status_bkaad;
                   $kakstatusbapeda = $key->kak_status_bapeda;
                   $kakstatusinspektorat = $key->kak_status_inspektorat;
         }

         if($kakstatusbkaad == '3' && $kakstatusbapeda == '3' && $kakstatusinspektorat == '3'){
           $status__kak = kak::find($id);
           $status__kak->status = '3';
           $status__kak->update();

           // $id__revisi = DB::table('revisi')
           //                ->select(DB::raw('*'))
           //                ->where('idkak','=',$id_kak)
           //                ->get();
           // foreach ($id__revisi as $key) {
           //           $id___revisi = $key->kak_status_bkaad;
           // }
           //
           // $status__validator = revisi::find($id___revisi);
           // $status__validator->statusrevisi = '3';
           // $status__validator->update();
         }

         if($kakstatusbkaad == '3' && $kakstatusbapeda == '3' && $kakstatusinspektorat == '2'){
           $status__kak = kak::find($id);
           $status__kak->status = '1';
           $status__kak->update();
         }

         if($kakstatusbkaad == '3' && $kakstatusbapeda == '2' && $kakstatusinspektorat == '2'){
           $status__kak = kak::find($id);
           $status__kak->status = '1';
           $status__kak->update();
         }

         if($kakstatusbkaad == '2' && $kakstatusbapeda == '2' && $kakstatusinspektorat == '2'){
           $status__kak = kak::find($id);
           $status__kak->status = '1';
           $status__kak->update();
         }

         if($kakstatusbkaad == '2' && $kakstatusbapeda == '2' && $kakstatusinspektorat == '3'){
           $status__kak = kak::find($id);
           $status__kak->status = '1';
           $status__kak->update();
         }

         if($kakstatusbkaad == '2' && $kakstatusbapeda == '3' && $kakstatusinspektorat == '3'){
           $status__kak = kak::find($id);
           $status__kak->status = '1';
           $status__kak->update();
         }
         if($kakstatusbkaad == '2' && $kakstatusbapeda == '3' && $kakstatusinspektorat == '2'){
           $status__kak = kak::find($id);
           $status__kak->status = '1';
           $status__kak->update();
         }
         if($kakstatusbkaad == '3' && $kakstatusbapeda == '2' && $kakstatusinspektorat == '3'){
           $status__kak = kak::find($id);
           $status__kak->status = '1';
           $status__kak->update();
         }

         $kak = DB::table('kak')
                ->select(DB::raw('*'))
                ->where('idkak','=',$id)
                ->get();

         foreach ($kak as $key) {
           $judul_kak = $key->judul;
           $id_kak = $key->idkak;
           $id_satker = $key->idsatker;
         }

         $newPost = $database
         ->getReference('notifikasi_status_kak')
         ->push([
         'notifikasi' => 'inspektorat telah mengajukan revisi KAK ('.$judul_kak.')' ,
         'tanggal' => $tgl_sekarang,
         'idkak' => $id_kak,
         'idsatker' => $id_satker,
         'status' => 1
         ]);




       $detailagenda = DB::table('kak')
              ->select(DB::raw('*'))
              ->where('idkak','=',$id)
              ->get();
       foreach ($detailagenda as $key) {
         $idsatker = $key->idsatker;
         $idkak = $key->idkak;
       }

       $kak = DB::table('kak')
                ->select(DB::raw('*'))
                ->where('idsatker','=',$idsatker)
                ->orderBy('kak.idkak')
                ->get();
       return redirect('validatorinspektorattampildetailkakpersatker/'.$idsatker);

     }


}
