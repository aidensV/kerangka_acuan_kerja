<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use Auth;
use App\Agenda;
class SatkerAgendaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        return view('satker.agenda.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $agenda = new Agenda;
        $agenda->idkak = $request['kak_id'];
        $agenda->idsatker = $request['satker_id'];
        $agenda->title = $request['agenda_judul'];
        $agenda->start = $request['agenda_tgl_mulai'];
        $agenda->end = $request['agenda_tgl_selesai'];
        $agenda->agenda_anggaran = $request['agenda_anggaran'];
        $agenda->statuspengerjaan = '1';
        $agenda->save();

        return redirect()->route('satkeragenda.show',[$request['kak_id']]);

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
      $agenda = Agenda::where('idkak',$id)->get();

        return view('satker.agenda.create',compact('agenda'))->with('idkak',$id);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request,$id)
    {
      $idkak=$request['kak_id'];
      $agenda=Agenda::findOrFail($id);
      $agenda->delete();
      return redirect()->route('satkeragenda.show',[$idkak]);

    }

    public function ambil_agenda_sudah_terlaksanan()
    {
      $iduser = Auth::user()->iduser;
      $ambil_agenda = DB::table('agenda')
                 ->select(DB::raw('agenda.*,satker.*'))
                 ->join('satker','satker.idsatker','=','agenda.idsatker')
                 ->join('kak','kak.idkak','=','agenda.idkak')
                 ->where('statuspengerjaan','=','3')
                 ->where('agenda.idsatker','=',$iduser)
                 ->orderBy('agenda.id')
                 ->get();

      // $dt = array();
      foreach ($ambil_agenda as $key ) {
      $id = $key->id;
      $title = $key->title;
      $start = $key->start;
      $end = $key->end;
      }

      return($ambil_agenda);
      // return view('layouts.inspektorat.master',['ambil_agenda'=>$ambil_agenda]);
      // array_push($dt,$data);

    }
    public function ambil_agenda_belum_terlaksanan()
    {
      $iduser = Auth::user()->iduser;
      $ambil_agenda = DB::table('agenda')
                 ->select(DB::raw('agenda.*,satker.*'))
                 ->join('satker','satker.idsatker','=','agenda.idsatker')
                 ->where('statuspengerjaan','=','1')
                 ->where('agenda.idsatker','=',$iduser)
                 ->orderBy('agenda.id')
                 ->get();

      // $dt = array();
      foreach ($ambil_agenda as $key ) {
      $id = $key->id;
      $title = $key->title;
      $start = $key->start;
      $end = $key->end;
      }

      return($ambil_agenda);
      // return view('layouts.inspektorat.master',['ambil_agenda'=>$ambil_agenda]);
      // array_push($dt,$data);

    }
    public function ambil_agenda_proses_pelaksanaan()
    {
      $iduser = Auth::user()->iduser;
      $ambil_agenda = DB::table('agenda')
                 ->select(DB::raw('agenda.*,satker.*'))
                 ->join('satker','satker.idsatker','=','agenda.idsatker')
                 ->where('statuspengerjaan','=','2')
                 ->where('agenda.idsatker','=',$iduser)
                 ->orderBy('agenda.id')
                 ->get();

      // $dt = array();
      foreach ($ambil_agenda as $key ) {
      $id = $key->id;
      $title = $key->title;
      $start = $key->start;
      $end = $key->end;
      }

      return($ambil_agenda);
      // return view('layouts.inspektorat.master',['ambil_agenda'=>$ambil_agenda]);
      // array_push($dt,$data);

    }
    public function ambil_data_agenda()
    {
      $ambil_agenda = DB::table('agenda')
                 ->select(DB::raw('*'))
                 ->orderBy('agenda.id')
                 ->get();

      // $dt = array();
      foreach ($ambil_agenda as $key ) {
      $id = $key->id;
      $title = $key->title;
      $start = $key->start;
      $end = $key->end;
      }

      // return($ambil_agenda);
      return response()->json($ambil_agenda);
      // array_push($dt,$data);



    }
    public function tampil_per_kegiatan($id)
    {
      // $tampil_kegiatan_per_hari = $request['id'];
      // dd($tampil_kegiatan_per_hari);
      $detailagendaperkegiatan = DB::table('agenda')
               ->select(DB::raw('*'))
               ->where('id','=',$id)
               ->orderBy('agenda.id')
               ->get();

      foreach ($detailagendaperkegiatan as $key) {
                 $idkak = $key->idkak;
                 $namakegiatan = $key->title;
               }

      $detailagenda = DB::table('agenda')
                        ->select(DB::raw('*'))
                        ->where('idkak','=',$idkak)
                        ->orderBy('agenda.id')
                        ->get();


      return view('satker.detailagenda.index',['detailagenda'=>$detailagenda,'namakegiatan'=>$namakegiatan]);

      // return view('satker.detailagenda.index',['detailagenda' => $ambil_agenda_per_hari]);
      // $returnHTML = view('satker.detailagenda.index')->with('detailagenda', $ambil_agenda_per_hari)->render();
      // return response()->json(array('success' => true, 'html'=>$returnHTML));


      // return redirect("detailagenda/".$idkak)->with( ['namakegiatan' => $namakegiatan] );
      // return redirect::route("detailagenda/".$idkak)->with( ['namakegiatan' => $namakegiatan] );


      // return json_encode($ambil_agenda_per_hari);
      // return redirect('satkertampilperkegiatan/'.$id);
      // return redirect('satker.detailagenda.index');
      // array_push($dt,$data);
      // return response();


    }

    public function tampil_per_hari($id)
    {
      // $tampil_kegiatan_per_hari = $request['id'];
      // dd($tampil_kegiatan_per_hari);
      $ambil_agenda_per_hari = DB::table('agenda')
                 ->select(DB::raw('*'))
                 ->where('agenda.id','=', $id)
                 ->orderBy('agenda.id')
                 ->get();
      return($ambil_agenda_per_hari);
      // array_push($dt,$data);

    }
    public function detailagendaubahstatusproses($id)
    {
      $agenda = Agenda::find($id);
      $agenda->statuspengerjaan = '2';
      $agenda->update();

      $data_agenda = DB::table('agenda')
                 ->select(DB::raw('*'))
                 ->get();
      foreach ($data_agenda as $key) {
        $idkak = $key->idkak;
      }


      return redirect('detailagenda/'.$idkak);
      // return view('satker.detailagenda.index',['detailagenda'=>$detailagenda,'namakegiatan'=>$namakegiatan]);
    }
    public function buktiprogramsudahterlaksanan($id)
    {
      return view('detailagenda/'.$idkak);
    }
}
