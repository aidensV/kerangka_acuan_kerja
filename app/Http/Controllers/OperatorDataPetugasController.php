<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use App\Petugas;
use App\Pengguna;

class OperatorDataPetugasController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
      $petugas = DB::table('users')
                 ->select(DB::raw('*'))
                 ->where('level','<>','1')
                 ->orderBy('users.id')
                 ->get();
        return view('operator.datapetugas.index',['petugas'=>$petugas]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
      return view('operator.datapetugas.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
      $selectMax = DB::table('pengawas')
                      ->select('idpengawas')
                      ->max('idpengawas')
                      ;
                      $setID = (int) substr($selectMax, 2, 3);
                      $char = "P";
                      $getID = $char . sprintf("%04s", $setID);
                      $getID++;

        $petugas = new Petugas;
        $petugas->idpengawas = $getID;
        $petugas->namapengawas = $request['petugas_nama'];
        $petugas->alamatpengawas = $request['petugas_nohp'];
        $petugas->nohppengawas = $request['petugas_alamat'];
        $petugas->save();

        $users = new Pengguna;
        $users->iduser = $getID;
        $users->name = $request['petugas_nama'];
        $users->email = $request['petugas_email'];
        $users->password = bcrypt($request['petugas_password']);
        $users->level = $request['petugas_jabatan'];
        $users->save();

        return redirect('operatordatapetugas');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
      $petugas = Petugas::join('users','users.iduser','=','pengawas.idpengawas')->where('users.iduser',$id)->get();
      // dd($petugas);
        return view('operator.datapetugas.edit',compact('petugas'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
      $users = Pengguna::find($id);
      $users->name = $request['petugas_nama'];
      $users->email = $request['petugas_email'];
      $users->password = bcrypt($request['petugas_password']);
      $users->level = $request['petugas_jabatan'];
      $users->update();

      return redirect('operatordatapetugas');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
      $petugas =Petugas::find($id)->delete();
      $users =Pengguna::find($id)->delete();
      return redirect('operatordatapetugas');
    }
}
