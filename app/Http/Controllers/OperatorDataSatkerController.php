<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use App\User;
use App\satker;
use App\Pengguna;

class OperatorDataSatkerController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
      $satker = DB::table('satker')
                 ->select(DB::raw('*'))
                 ->orderBy('satker.idsatker')
                 ->get();
        return view('operator.datasatker.index',['satker'=>$satker]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

        return view('operator.datasatker.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Responseker
     */
    public function store(Request $request)
    {
      $selectMax = DB::table('satker')
                      ->select('idsatker')
                      ->max('idsatker')
                      ;
                      $setID = (int) substr($selectMax, 2, 3);
                      $char = "S";
                      $getID = $char . sprintf("%04s", $setID);
                      $getID++;
                      // dd($selectMax);
                      // echo $getID;

        $satker = new satker;
        $satker->idsatker = $getID;
        $satker->namasatker = $request['satker_nama'];
        $satker->nohpsatker = $request['satker_nohp'];
        $satker->alamatsatker = $request['satker_alamat'];
        $satker->save();

        $users = new User;
        $users->iduser = $getID;
        $users->email = $request['satker_email'];
        $users->name = $request['satker_nama'];
        $users->password = bcrypt($request['satker_password']);
        $users->level = '1';
        $users->save();
        return redirect('operatordatasatker');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
      // $satker = satker::find($id);
      $satker = satker::join('users','users.iduser','=','satker.idsatker')->where('users.iduser',$id)->get();
      // dd($satker);
        return view('operator.datasatker.edit',compact('satker'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

      // $users =User::where('iduser',$id)->value('iduser');
      $users =Pengguna::find($id);
      $users->email = $request['satker_email'];
      $users->name = $request['satker_nama'];
      $users->password = bcrypt($request['satker_password']);
      $users->level = '1';
      $users->update();
      // dd($users1);
      return redirect('operatordatasatker');


    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $satker =satker::find($id)->delete();
        $users =Pengguna::find($id)->delete();
        return redirect('operatordatasatker');
    }
}
