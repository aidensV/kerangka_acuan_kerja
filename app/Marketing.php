<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Marketing extends Model
{
  protected $table = 'marketing';
  protected $primaryKey = 'marketing_id';
}
