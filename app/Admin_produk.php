<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Admin_produk extends Model
{
  protected $table = 'admin_produk';
  protected $primaryKey = 'admin_produk_id';
}
