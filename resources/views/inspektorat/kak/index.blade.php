@extends('layouts.inspektorat.master')
@section('content')
 <div id="page-content">
                        <!-- Dashboard Header -->
                        <!-- For an image header add the class 'content-header-media' and an image as in the following example -->






                        <div class="content-header content-header-media">
                            <div class="header-section">
                                <div class="row">

                                    <div class="col-md-4 col-lg-6 hidden-xs hidden-sm">
                                        <h1>Data <strong>Satker</strong><br><small>You Look Awesome!</small></h1>
                                    </div>



                                    <div class="col-md-8 col-lg-6">
                                        <div class="row text-center">
                                            <div class="col-xs-4 col-sm-3">
                                                <h2 class="animation-hatch">
                                                    $<strong>93.7k</strong><br>
                                                    <small><i class="fa fa-thumbs-o-up"></i> Great</small>
                                                </h2>
                                            </div>
                                            <div class="col-xs-4 col-sm-3">
                                                <h2 class="animation-hatch">
                                                    <strong>167k</strong><br>
                                                    <small><i class="fa fa-heart-o"></i> Likes</small>
                                                </h2>
                                            </div>
                                            <div class="col-xs-4 col-sm-3">
                                                <h2 class="animation-hatch">
                                                    <strong>101</strong><br>
                                                    <small><i class="fa fa-calendar-o"></i> Events</small>
                                                </h2>
                                            </div>

                                            <div class="col-sm-3 hidden-xs">
                                                <h2 class="animation-hatch">
                                                    <strong>27&deg; C</strong><br>
                                                    <small><i class="fa fa-map-marker"></i> Sydney</small>
                                                </h2>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <img src="{{asset('public/backend/img/placeholders/headers/dashboard_header.jpg')}}" alt="header image" class="animation-pulseSlow">
                        </div>



                        <div class="row">
                          @foreach($satker as $data)
                            <div class="col-sm-6 col-lg-3">

                                <a href="{{url('inspektorattampildetailkakpersatker')}}/{{$data->idsatker}}" class="widget widget-hover-effect1">
                                    <div class="widget-simple">
                                        <div class="widget-icon pull-left themed-background-autumn animation-fadeIn">
                                            <i class="fa fa-address-book"></i>
                                        </div>
                                        <h3 class="widget-content text-right animation-pullDown">
                                            <strong>{{$data->namasatker}}</strong><br>
                                            <small>



                                            </small>
                                        </h3>
                                    </div>
                                </a>

                            </div>
                          @endforeach
                        </div>




                        </div>
@endsection
