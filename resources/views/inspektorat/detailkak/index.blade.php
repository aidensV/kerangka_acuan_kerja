@extends('layouts.inspektorat.master')
@section('content')
<div id="page-content">
                       <!-- Datatables Header -->
                       <div class="content-header">
                           <div class="header-section">
                               <h1>
                                   <i class="fa fa-table"></i>Datatables<br><small>HTML tables can become fully dynamic with cool features!</small>
                               </h1>
                           </div>
                       </div>
                       <ul class="breadcrumb breadcrumb-top">
                           <li>Tables</li>
                           <li><a href="">Datatables</a></li>
                       </ul>
                       <!-- END Datatables Header -->

                       <!-- Datatables Content -->
                       <div class="block full">
                           <div class="block-title">
                               <h2><strong>Datatables</strong> integration</h2>
                           </div>
                           <p><a href="https://datatables.net/" target="_blank">DataTables</a> is a plug-in for the Jquery Javascript library. It is a highly flexible tool, based upon the foundations of progressive enhancement, which will add advanced interaction controls to any HTML table. It is integrated with template's design and it offers many features such as on-the-fly filtering and variable length pagination.</p>

                           <div class="table-responsive">
                               <table id="example-datatable" class="table table-vcenter table-condensed table-bordered">
                                   <thead>
                                       <tr>
                                           <th>No</th>
                                           <th>Judul</th>
                                           <th>Status</th>
                                           <th>Agenda</th>
                                           <th>Aksi</th>
                                       </tr>
                                   </thead>
                                   <tbody>
                                     @php
                                      $no=1;
                                     @endphp
                                     @foreach($kak  as $data)
                                       <tr>
                                           <td style="font-size: 15px;">{{$no}}</td>
                                           <td style="font-size: 15px;">{{$data->judul}}</td>
                                           <td style="font-size: 15px;">@php
                                            $stt_bapeda = $data->kak_status_bapeda;
                                            if($stt_bapeda == '1'){
                                              $stt_bapeda1 = 'belum memvalidasi';
                                              $stt_kak_hidden_visibled = "style='visibility: hidden'";
                                            }elseif($stt_bapeda == '2'){
                                              $stt_bapeda1 = 'revisi';
                                              $stt_kak_hidden_visibled = "style='visibility: hidden'";
                                            }elseif($stt_bapeda == '3'){
                                              $stt_bapeda1 = 'tersetujui';
                                              $stt_kak_hidden_visibled = "style='visibility: visible'";
                                            }
                                            echo $stt_bapeda1;
                                           @endphp</td>
                                           <td class="text-center">
                                                <div class="btn-group">
                                                    <a href="{{url('inspektoratdetailagenda')}}/{{$data->idkak}}" data-toggle="tooltip" title="Detail" class="btn btn-xs btn-default"><i class="fa fa-pencil"> Detail</i></a>
                                                </div>
                                            </td>
                                            <td>
                                                  <div class="btn-group">
                                                      <a href="{{url('inspektoratkakpdf')}}/{{$data->idkak}}" data-toggle="tooltip" title="Detail" class="btn btn-xs btn-default" @php echo $stt_kak_hidden_visibled; @endphp><i class="fa fa-print"> Lihat KAK</i></a>
                                                  </div>
                                            </td>
                                       </tr>
                                       @php
                                        $no++;
                                       @endphp
                                    @endforeach
                                   </tbody>
                               </table>
                           </div>
                       </div>
                       <!-- END Datatables Content -->
                   </div>
@endsection
@section('script')

<script>
var TablesDatatables = function() {

    return {
        init: function() {
            /* Initialize Bootstrap Datatables Integration */
            App.datatables();

            /* Initialize Datatables */
            $('#example-datatable').dataTable({

                columnDefs: [ { orderable: false, targets: [ 1, 5 ] } ],
                pageLength: 10,
                lengthMenu: [[10, 20, 30, -1], [10, 20, 30, 'All']],
            });

            /* Add placeholder attribute to the search input */
            $('.dataTables_filter input').attr('placeholder', 'Search');
        }
    };
}();
</script>

@endsection
