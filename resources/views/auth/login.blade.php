<!DOCTYPE html>
<!--[if IE 9]>         <html class="no-js lt-ie10" lang="en"> <![endif]-->
<!--[if gt IE 9]><!-->
<html class="no-js" lang="en">
<!--<![endif]-->

<head>
  <meta charset="utf-8">

  <title>ProUI - Responsive Bootstrap Admin Template</title>

  <meta name="description" content="ProUI is a Responsive Bootstrap Admin Template created by pixelcave and published on Themeforest.">
  <meta name="author" content="pixelcave">
  <meta name="robots" content="noindex, nofollow">
  <meta name="viewport" content="width=device-width,initial-scale=1.0,user-scalable=0">

  <!-- Icons -->
  <!-- The following icons can be replaced with your own, they are used by desktop and mobile browsers -->
  <link rel="shortcut icon" href="{{asset('backend/img/favicon.png')}}">
  <link rel="apple-touch-icon" href="{{asset('public/backend/img/icon57.png')}}" sizes="57x57">
  <link rel="apple-touch-icon" href="{{asset('public/backend/img/icon72.png')}}" sizes="72x72">
  <link rel="apple-touch-icon" href="{{asset('public/backend/img/icon76.png')}}" sizes="76x76">
  <link rel="apple-touch-icon" href="{{asset('public/backend/img/icon114.png')}}" sizes="114x114">
  <link rel="apple-touch-icon" href="{{asset('public/backend/img/icon120.png')}}" sizes="120x120">
  <link rel="apple-touch-icon" href="{{asset('public/backend/img/icon144.png')}}" sizes="144x144">
  <link rel="apple-touch-icon" href="{{asset('public/backend/img/icon152.png')}}" sizes="152x152">
  <link rel="apple-touch-icon" href="{{asset('public/backend/img/icon180.png')}}" sizes="180x180">
  <!-- END Icons -->

  <!-- Stylesheets -->
  <!-- Bootstrap is included in its original form, unaltered -->
  <link rel="stylesheet" href="{{asset('public/backend/css/bootstrap.min.css')}}">

  <!-- Related styles of various icon packs and plugins -->
  <link rel="stylesheet" href="{{asset('public/backend/css/plugins.css')}}">

  <!-- The main stylesheet of this template. All Bootstrap overwrites are defined in here -->
  <link rel="stylesheet" href="{{asset('public/backend/css/main.css')}}">

  <!-- Include a specific file here from css/themes/ folder to alter the default theme of the template -->

  <!-- The themes stylesheet of this template (for using specific theme color in individual elements - must included last) -->
  <link rel="stylesheet" href="{{asset('public/backend/css/themes.css')}}">
  <!-- END Stylesheets -->

  <!-- Modernizr (browser feature detection library) -->
  <script src="{{asset('public/backend/js/vendor/modernizr.min.js')}}"></script>
</head>

<body>
  <!-- Login Background -->
  <div id="login-background">
    <!-- For best results use an image with a resolution of 2560x400 pixels (prefer a blurred image for smaller file size) -->
    <img src="{{asset('public/backend/img/placeholders/headers/login_header.jpg')}}" alt="Login Background" class="animation-pulseSlow">
  </div>
  <!-- END Login Background -->

  <!-- Login Container -->
  <div id="login-container" class="animation-fadeIn">
    <!-- Login Title -->
    <div class="login-title text-center">
      <h1><i class="gi gi-flash"></i> <small>E <strong>- KAK</strong></h1>
    </div>
    <!-- END Login Title -->

    <!-- Login Block -->
    <div class="block push-bit">
      <!-- Login Form -->
      <form method="POST" action="{{ route('login') }}" id="form-login" class="form-horizontal form-bordered form-control-borderless">
        {{ csrf_field() }}
        <div class="form-group">
          <div class="col-xs-12">
            <div class="input-group">
              <span class="input-group-addon"><i class="gi gi-envelope"></i></span>
              <input type="text" id="login-email" name="email" class="form-control input-lg" placeholder="Email">
            </div>
          </div>
        </div>
        <div class="form-group">
          <div class="col-xs-12">
            <div class="input-group">
              <span class="input-group-addon"><i class="gi gi-asterisk"></i></span>
              <input type="password" id="login-password" name="password" class="form-control input-lg" placeholder="Password">
            </div>
          </div>
        </div>
        <div class="form-group form-actions">

          <div class="col-xs-7 text-right">
            <button type="submit" class="btn btn-primary">Masuk</button>
          </div>
        </div>

      </form>
    </div>
    <!-- END Login Block -->

    <!-- Footer -->
    <footer class="text-muted text-center">
      <small><span id="year-copy"></span> &copy; <a href="http://goo.gl/TDOSuC" target="_blank">ProUI 3.8</a></small>
    </footer>
    <!-- END Footer -->
  </div>
  <!-- END Login Container -->

  <!-- Modal Terms -->

  <!-- END Modal Terms -->

  <!-- jQuery, Bootstrap.js, jQuery plugins and Custom JS code -->
  <script type="text/javascript" src="{{ asset('public/js/jquery-3.3.1.min.js') }}"></script>
  <script src="{{asset('public/backend/js/vendor/bootstrap.min.js')}}"></script>
  <script src="{{asset('public/backend/js/plugins.js')}}"></script>
  <script src="{{asset('public/backend/js/app.js')}}"></script>

  <!-- Load and execute javascript code used only in this page -->
  <script src="{{asset('public/backend/js/pages/login.js')}}"></script>
  <script>
    $(function() {
      Login.init();
    });
  </script>
</body>

</html>
