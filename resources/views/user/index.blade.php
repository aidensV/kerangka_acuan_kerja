@extends('layouts.user.LayoutUser')
@section('pageTitle', 'Beranda')
@section('slide')

<div class="slider-wrapper">
    <div class="slider">
        <div class="fs_loader"></div>
        @foreach($slide1 as $sld1)
        <div class="slide">
            <img src="{{ asset('public/images/'.$sld1->gambar) }}" data-position="-30,550" data-in="bottom" data-delay="500" data-out="fade"  width="800" height="750">
            <!--3- laptop-->
            <p data-position="10,470" data-in="top"  data-out="Right" data-delay="" style="color: #BA2E32; font-style: arial; font-size: 60px">Welcome to Cokelat Ibuke</p>
        </div>
        @endforeach
        @foreach($slide2 as $sld2)
        <div class="slide">
            <img src="{{ asset('public/user/img/fraction-slider/fraction_2.png') }}" data-in="fade" data-delay="" data-out="fade" width="1920" height="450">     <!--2- slide background-->
            <img src="{{ asset('public/images/'.$sld2->gambar) }}" width="600" height="600" data-position="8,1240" data-in="bottomLeft" data-delay="500" data-out="fade" style="width:auto; height:auto">
            <p  class="claim light-pink" data-position="40,230" data-in="top"  data-out="left" data-delay="1800" data-ease-in="easeOutBounce">{{ $sld2->text1 }}</p>
            <p  class="teaser turky small"  data-position="150,230" data-in="left" data-out="left" data-delay="5500">{{ $sld2->text2 }}</p>
            <p  class="teaser turky small"  data-position="210,230"  data-in="left" data-out="left" data-delay="6500">{{ $sld2->text3 }}</p>
        </div>
        @endforeach
        @foreach($slide3 as $sld3)
        <div class="slide">
            <img src="{{ asset('public/user/img/fraction-slider/fraction_6.png') }}" data-in="fade" data-delay="20" data-out="fade" width="100%" height="450">         <!--4- slide background-->
            <p class="claim light-pink" data-position="50,1020" data-in="top"  data-out="top" data-ease-in="jswing">{{ $sld3->text1 }}</p>
            <p class="teaser turky" data-position="120,1180" data-in="left" data-delay="1500">{{ $sld3->text2 }}</p>
            <p class="teaser turky" data-position="170,1180" data-in="left"  data-delay="3000">{{ $sld3->text3 }}</p>
            <img src="{{ asset('public/images/'.$sld3->gambar) }}" width="480" height="480" data-position="-20,250" data-in="right" data-delay="500" data-out="fade" style="width:auto; height:auto">
        </div>
        @endforeach
    </div>
</div>

@endsection

@section('content')
<section class="content portfolio">
    <div class="container">
        <div class="row">
            <!--begin isotope -->
            <div class="isotope col-lg-12">
                <!--begin portfolio_list -->
                <div class="dividerHeading">
                    <h4><span>Recent Upload</span></h4>
                </div>
                <ul id="list" class="portfolio_list clearfix">
                    @foreach($data as $dt)
                    <!-- Begin Portfolio item -->
                    <li class="list_item col-lg-3 col-md-3 col-sm-3">
                        <div class="recent-item">
                            <figure>
                                <div class="touching medium">
                                    <img src="{{ asset('public/images/'.$dt->gambar_produk) }}" alt="" />
                                    <div class="hovers">
                                        <a href="{{ asset('public/images/'.$dt->gambar_produk) }}" class="hover-zoom mfp-image" ><i class="fa fa-search"></i></a>
                                        <a href="{{route('produk.show',$dt->id_produk)}}" class="hover-link"><i class="fa fa-link"></i></a>
                                    </div>
                                </div>
                                <figcaption class="item-description">
                                    <a href="{{route('produk.show',$dt->id_produk)}}"><h5>{{ $dt->nama_produk }} ({{ $dt->kode_produk }})</h5></a>
                                    <span>Rp.{{ number_format($dt->harga_produk, 0, '.','.') }},-</span>
                                </figcaption>
                            </figure>
                        </div>
                    </li>
                    <!-- End Portfolio item -->
                    @endforeach
                </ul> <!--end portfolio_list -->
            </div>
            <!--end isotope -->
        </div> <!--./span12-->
    </div> <!--./div-->
</section>
<!--Start recent work-->
@endsection