@extends('layouts.validatorbapeda.master')
@section('content')
<div id="page-content">
                       <!-- Datatables Header -->
                       <div class="content-header">
                           <div class="header-section">
                               <h1>
                                   <i class="fa fa-table"></i>

                                   <br><small>

                                   </small>
                               </h1>
                           </div>
                       </div>
                       <ul class="breadcrumb breadcrumb-top">
                           <li>Tables</li>
                           <li><a href="">Datatables</a></li>
                       </ul>
                       <!-- END Datatables Header -->

                       <!-- Datatables Content -->
                       <div class="block full">
                           <div class="block-title" style="align:center">
                                 <h2><strong>Kerangka Acuan Kerja</strong> {{$idkak}}</h2>
                           </div>
                           <p align="center">
                           <iframe id="iframepdf" src="{{url('satkerkakpdf')}}/{{$idkak}}" width="700px" height="900px" align="center"></iframe>
                           </p>
                       </div>
                       <div class="block full">
                           <div class="block-title">
                               <h2><strong>Daftar Revisi</strong> {{$idkak}}</h2>
                           </div>
                           <a href="{{url('validatorbapedarevisi')}}/{{$idkak}}" class="btn btn-primary">Tambah</a>
                           <br>
                           <br>
                           <div class="table-responsive">
                               <table id="example-datatable" class="table table-vcenter table-condensed table-bordered">
                                   <thead>
                                       <tr>
                                           <th>No</th>
                                           <th>bab revisi</th>
                                           <th>revisi</th>
                                           <th>status revisi</th>
                                           <th>aksi</th>
                                       </tr>
                                   </thead>
                                   <tbody>
                                     @php
                                      $no=1;
                                     @endphp
                                     @foreach($revisi  as $data)

                                       <tr>
                                           <td style="font-size: 15px;">{{$no}}</td>
                                           <td style="font-size: 15px;">
                                           @php
                                            $bab_kak_revisi = $data->babkakrevisi;
                                            if($bab_kak_revisi == '1'){
                                              $bab_kak_revisi1 = 'Program';
                                            }elseif($bab_kak_revisi == '2'){
                                              $bab_kak_revisi1 = 'Latar Belakang';
                                            }elseif($bab_kak_revisi == '3'){
                                              $bab_kak_revisi1 = 'Maksud dan Tujuan';
                                            }elseif($bab_kak_revisi == '4'){
                                              $bab_kak_revisi1 = 'Ruang Lingkup';
                                            }elseif($bab_kak_revisi == '5'){
                                              $bab_kak_revisi1 = 'Sasaran';
                                            }elseif($bab_kak_revisi == '6'){
                                              $bab_kak_revisi1 = 'Lokasi Kegiatan';
                                            }elseif($bab_kak_revisi == '7'){
                                              $bab_kak_revisi1 = 'Keluaran';
                                            }elseif($bab_kak_revisi == '8'){
                                              $bab_kak_revisi1 = 'Anggaran';
                                            }elseif($bab_kak_revisi == '9'){
                                              $bab_kak_revisi1 = 'Penutup';
                                            }
                                           @endphp
                                           {{$bab_kak_revisi1}}</td>
                                           <td style="font-size: 15px;">{{$data->detailrevisi}}</td>
                                           <td style="font-size: 15px;">

                                             @php
                                              $status_revisi = $data->statusrevisi;
                                              if($status_revisi == '1'){
                                                $status_revisi1 = 'salah';
                                              }elseif($status_revisi == '2'){
                                                $status_revisi1 = 'benar';
                                              }
                                             @endphp
                                             {{$status_revisi1}}

                                           </td>
                                           <td class="text-center">
                                                 <div class="btn-group">
                                                     <a href="{{url('validatorbapedaupdaterevisibenar')}}/{{$data->idrevisi}}" data-toggle="tooltip" title="Detail" class="btn btn-xs btn-default"><i class="fa fa-pencil"> benar</i></a>
                                                 </div>
                                                 <div class="btn-group">
                                                     <a href="" data-toggle="tooltip" title="Detail" class="btn btn-xs btn-default"><i class="fa fa-pencil"> salah</i></a>
                                                 </div>
                                            </td>
                                       </tr>
                                       @php
                                        $no++;
                                       @endphp
                                    @endforeach
                                   </tbody>
                               </table>
                           </div>
                       </div>
                       <!-- END Datatables Content -->
                   </div>
@endsection
@section('script')

@endsection
