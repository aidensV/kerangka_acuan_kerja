@extends('layouts.validatorbapeda.master')
@section('content')
<div id="page-content">
                        <!-- Calendar Header -->
                        <div class="content-header">
                            <div class="header-section">
                                <h1>
                                    <i class="fa fa-calendar"></i>Calendar<br><small>An awesome calendar for your events!</small>
                                </h1>
                            </div>
                        </div>
                        <ul class="breadcrumb breadcrumb-top">
                            <li>Components</li>
                            <li><a href="">Calendar</a></li>
                        </ul>
                        <!-- END Calendar Header -->

                        <!-- FullCalendar Content -->
                        <!-- <div class="block full"> -->

                            <!-- <div class="block-title"> -->
                                <!-- <h2><strong>Penjelasan</strong> Warna</h2> -->
                            <!-- </div> -->

                            <!-- <p>Belum Terlaksana : <span class="label" style="background-color:#000000">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span>&nbsp;&nbsp;&nbsp; -->
                              <!-- Proses Pelaksanaan : <span class="label" style="background-color:#FF8C00">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span>&nbsp;&nbsp;&nbsp; -->
                              <!-- Sudah Terlaksana : <span class="label" style="background-color:#006400">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span>&nbsp;&nbsp;&nbsp; -->
                            <!-- </p> -->

                        <!-- </div> -->
                        <div class="block block-alt-noborder full">
                          <div class="row">
                              <div class="col-md-12">
                                  <!-- FullCalendar (initialized in js/pages/compCalendar.js), for more info and examples you can check out http://arshaw.com/fullcalendar/ -->
                                  <p>Belum Terlaksana : <span class="label" style="background-color:#000000">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span>&nbsp;&nbsp;&nbsp;
                                    Proses Pelaksanaan : <span class="label" style="background-color:#FF8C00">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span>&nbsp;&nbsp;&nbsp;
                                    Sudah Terlaksana : <span class="label" style="background-color:#006400">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span>&nbsp;&nbsp;&nbsp;
                                  </p>
                                  <p>
                                    <hr>
                                  </p>
                              </div>
                          </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <!-- FullCalendar (initialized in js/pages/compCalendar.js), for more info and examples you can check out http://arshaw.com/fullcalendar/ -->
                                    <div id="calendar"></div>
                                </div>
                            </div>
                        </div>
                        <!-- END FullCalendar Block -->
                        <!-- END FullCalendar Content -->
                    </div>



@endsection

@section('script')



<script>

$(document).ready(function() {
  var calendar = $('#calendar').fullCalendar({

    editable:true,
    header:{
      left:'prev,next today',
      center:'title',
      right:'month, agendaWeek, agendaDay'
    },
    locale: 'id',
    eventSources:[
      {
        url: '{{url('validatorbapedaambilagendasudahterlaksana')}}',
        textColor: '#FFFFFF',
        backgroundColor: '#006400',
        borderColor: '#006400'
      },
      {
        url: '{{url('validatorbapedaambilagendaprosespelaksanaan')}}',
        textColor: '#FFFFFF',
        backgroundColor: '#FF8C00',
        borderColor: '#FF8C00'
      },
      {
        url: '{{url('validatorbapedaambilagendabelumterlaksana')}}',
        textColor: '#FFFFFF',
        backgroundColor: '#000000',
        borderColor: '#000000'
      }
    ],




    selectable:true,
    selectHelper:true,
    editable:true,

    dayClick:function(date, jsEvent, view){
      alert(date);
    },

    eventClick:function(event){
      var id = event.id;
      var title = event.title;
      var satker = event.namasatker;
      if(confirm("Lihat kegiatan "+"'"+title+"' ?"+" dari "+satker)){
        window.location.href = "validatorbapedatampilperkegiatan/"+id;
        // $.ajax({
        //
        //   url:"inspektorattampilperkegiatan/"+id,
        //   type:"GET",
        //   data: {
        //    _token: '{!! csrf_token() !!}',
        //  },
        //   dataType:"JSON",
        //   success:function()
        //   {
        //     calendar.fullCalendar('refetchEvents');
        //     window.location.assign("inspektorattampilperkegiatan/"+id)
        //   }
        // })
      }
    }
  }).css('font-size','15px');
});

</script>


<script>
var TablesDatatables = function() {

    return {
        init: function() {
            /* Initialize Bootstrap Datatables Integration */
            App.datatables();

            /* Initialize Datatables */
            $('#example-datatable').dataTable({

                columnDefs: [ { orderable: false, targets: [ 1, 5 ] } ],
                pageLength: 10,
                lengthMenu: [[10, 20, 30, -1], [10, 20, 30, 'All']],
            });

            /* Add placeholder attribute to the search input */
            $('.dataTables_filter input').attr('placeholder', 'Search');
        }
    };
}();
</script>



@endsection
