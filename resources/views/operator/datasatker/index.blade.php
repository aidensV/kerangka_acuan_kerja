@extends('layouts.operator.master')
@section('content')
<div id="page-content">
  <!-- Datatables Header -->
  <div class="content-header">
    <div class="header-section">
      <h1>
        <i class="fa fa-table"></i>Datatables<br><small>HTML tables can become fully dynamic with cool features!</small>
      </h1>
    </div>
  </div>
  <ul class="breadcrumb breadcrumb-top">
    <li>Tables</li>
    <li><a href="">Datatables</a></li>
  </ul>
  <!-- END Datatables Header -->

  <!-- Datatables Content -->
  <div class="block full">
    <div class="block-title">
      <h2><strong>Datatables</strong> integration</h2>
    </div>
    <a href="{{route('operatordatasatker.create')}}" class="btn btn-primary">Tambah</a>
    <div class="table-responsive">
      <table id="example-datatable" class="table table-vcenter table-condensed table-bordered">
        <thead>
          <tr>
            <th style="width:1%">No</th>
            <th style="width:10%">Nama Satker</th>
            <th style="width:15%">Alamat </th>
            <th style="width:10%">No HP</th>
            <th style="width:5%">Aksi</th>
          </tr>
        </thead>
        <tbody>
          @php
          $no=1;
          @endphp
          @foreach($satker as $data)
          <tr>
            <td style="font-size: 15px;">{{$no}}</td>
            <td style="font-size: 15px;">{{$data->namasatker}}</td>
            <td style="font-size: 15px;">{{$data->alamatsatker}}</td>
            <td style="font-size: 15px;">{{$data->nohpsatker}}</td>
            <td>
              <form action="{{ route('operatordatasatker.destroy', $data->idsatker) }}" method="post">
                {{csrf_field()}}
                {{ method_field('DELETE') }}
                <div class="btn-group">
                  <button type="submit" name="button" data-toggle="tooltip" title="Hapus Data Satker" class="btn btn-xs btn-default"><i class="fa fa-trash"> Delete</i></button>
                  {{-- <a href="{{route('operatordatasatker.edit',$data->idsatker)}}" data-toggle="tooltip" title="Detail" class="btn btn-xs btn-default"><i class="fa fa-trash"> Delete</i></a> --}}
                </div>
                <div class="btn-group">
                  <a href="{{route('operatordatasatker.edit',$data->idsatker)}}" data-toggle="tooltip" title="Ubah Data Satker" class="btn btn-xs btn-default"><i class="fa fa-pencil"> Update</i></a>
                </div>
              </form>
            </td>
          </tr>
          @php
          $no++;
          @endphp
          @endforeach
        </tbody>
      </table>
    </div>
  </div>
  <!-- END Datatables Content -->
</div>
@endsection
@section('script')

<script>
  var TablesDatatables = function() {

    return {
      init: function() {
        /* Initialize Bootstrap Datatables Integration */
        App.datatables();

        /* Initialize Datatables */
        $('#example-datatable').dataTable({

          columnDefs: [{
            orderable: false,
            targets: [1, 5]
          }],
          pageLength: 10,
          lengthMenu: [
            [10, 20, 30, -1],
            [10, 20, 30, 'All']
          ],
        });

        /* Add placeholder attribute to the search input */
        $('.dataTables_filter input').attr('placeholder', 'Search');
      }
    };
  }();
</script>

@endsection
