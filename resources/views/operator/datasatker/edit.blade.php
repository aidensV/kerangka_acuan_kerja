@extends('layouts.operator.master')
@section('content')
<div id="page-content">
  <!-- Datatables Header -->
  <div class="content-header">
    <div class="header-section">
      <h1>
        <i class="fa fa-table"></i>Datatables<br><small>HTML tables can become fully dynamic with cool features!</small>
      </h1>
    </div>
  </div>
  <ul class="breadcrumb breadcrumb-top">
    <li>Tables</li>
    <li><a href="">Datatables</a></li>
  </ul>
  <!-- END Datatables Header -->

  <!-- Datatables Content -->
  <div class="block full">

    <!-- Basic Form Elements Title -->
    <div class="block-title">
      <div class="block-options pull-right">
        <a href="javascript:void(0)" class="btn btn-alt btn-sm btn-default toggle-bordered enable-tooltip" data-toggle="button" title="Toggles .form-bordered class">No Borders</a>
      </div>
      <h2><strong>Basic Form</strong> Elements</h2>
    </div>
    <!-- END Form Elements Title -->

    <!-- Basic Form Elements Content -->
    @foreach ($satker as $data)
    <form action="{{route('operatordatasatker.update',$data['iduser'])}}" method="post" enctype="multipart/form-data" class="form-horizontal form-bordered">
      {{ csrf_field() }}
      {{ method_field('PUT') }}



      <div class="form-group">
        <label class="col-md-3 control-label" for="example-text-input">Nama</label>
        <div class="col-md-9">
          <input type="text" id="example-text-input" name="satker_nama" readonly value="{{$data['namasatker']}}" class="form-control" placeholder="Text">
          <span class="help-block">This is a help text</span>
        </div>
      </div>
      <div class="form-group">
        <label class="col-md-3 control-label" for="example-email-input">ALamat Email</label>
        <div class="col-md-9">
          <input type="email" id="example-email-input" name="satker_email"  value="{{$data['email']}}" class="form-control" placeholder="Enter Email">
          <span class="help-block">Please enter your email</span>
        </div>
      </div>
      <div class="form-group">
        <label class="col-md-3 control-label" for="example-password-input">Password</label>
        <div class="col-md-9">
          <input type="password" id="example-password-input" name="satker_password" class="form-control" placeholder="Password">
          <span class="help-block">Please enter a complex password</span>
        </div>
      </div>

      @endforeach
      {{-- <button type="submit" class="btn btn-primary" name="button">Simpan</button> --}}
      <input type="submit" name="" class="btn btn-primary" value="SIMPAN">
    </form>
    <!-- END Basic Form Elements Content -->
  </div>



  <!-- END Datatables Content -->
</div>
@endsection
