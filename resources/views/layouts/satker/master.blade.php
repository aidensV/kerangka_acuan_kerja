<?php
namespace App\Http\Controllers;
use Auth;
$nama = Auth::user()->name;
?>

<!DOCTYPE html>
<!--[if IE 9]>         <html class="no-js lt-ie10" lang="en"> <![endif]-->
<!--[if gt IE 9]><!-->
<html class="no-js" lang="en">
<!--<![endif]-->

<head>
  <meta charset="utf-8">

  <title>ProUI - Responsive Bootstrap Admin Template</title>

  <meta name="csrf-token" content="{{ csrf_token() }}">

  <meta name="description" content="ProUI is a Responsive Bootstrap Admin Template created by pixelcave and published on Themeforest.">
  <meta name="author" content="pixelcave">
  <meta name="robots" content="noindex, nofollow">
  <meta name="viewport" content="width=device-width,initial-scale=1.0,user-scalable=0">

  <!-- Icons -->
  <!-- The following icons can be replaced with your own, they are used by desktop and mobile browsers -->
  <link rel="shortcut icon" href="{{asset('backend/img/favicon.png')}}">
  <link rel="apple-touch-icon" href="{{asset('public/backend/img/icon57.png')}}" sizes="57x57">
  <link rel="apple-touch-icon" href="{{asset('public/backend/img/icon72.png')}}" sizes="72x72">
  <link rel="apple-touch-icon" href="{{asset('public/backend/img/icon76.png')}}" sizes="76x76">
  <link rel="apple-touch-icon" href="{{asset('public/backend/img/icon114.png')}}" sizes="114x114">
  <link rel="apple-touch-icon" href="{{asset('public/backend/img/icon120.png')}}" sizes="120x120">
  <link rel="apple-touch-icon" href="{{asset('public/backend/img/icon144.png')}}" sizes="144x144">
  <link rel="apple-touch-icon" href="{{asset('public/backend/img/icon152.png')}}" sizes="152x152">
  <link rel="apple-touch-icon" href="{{asset('public/backend/img/icon180.png')}}" sizes="180x180">
  <!-- END Icons -->

  <!-- Stylesheets -->
  <!-- Bootstrap is included in its original form, unaltered -->
  <link rel="stylesheet" href="{{asset('public/backend/css/bootstrap.min.css')}}">

  <!-- Related styles of various icon packs and plugins -->
  <link rel="stylesheet" href="{{asset('public/backend/css/plugins.css')}}">

  <!-- The main stylesheet of this template. All Bootstrap overwrites are defined in here -->
  <link rel="stylesheet" href="{{asset('public/backend/css/main.css')}}">

  <!-- Include a specific file here from css/themes/ folder to alter the default theme of the template -->

  <!-- The themes stylesheet of this template (for using specific theme color in individual elements - must included last) -->
  <link rel="stylesheet" href="{{asset('public/backend/css/themes.css')}}">


  <script src="https://www.gstatic.com/firebasejs/5.9.0/firebase.js"></script>
  <script src="https://www.gstatic.com/firebasejs/5.9.0/firebase.js"></script>
  <script>
    // Initialize Firebase
    var config = {
      apiKey: "AIzaSyC6vFCnUEWcTHsjC5kEGuJpLsEtRT61caQ",
      authDomain: "notifikasi-17e59.firebaseapp.com",
      databaseURL: "https://notifikasi-17e59.firebaseio.com",
      projectId: "notifikasi-17e59",
      storageBucket: "notifikasi-17e59.appspot.com",
      messagingSenderId: "415834373016"
    };
    firebase.initializeApp(config);

    var database = firebase.database();

    var lastIndex = 0;

  // Get Data
    firebase.database().ref('notifikasi_status_kak/').limitToLast(5).orderByChild("status").equalTo(1).on('value', function(snapshot) {
      var value = snapshot.val();
      var htmls = [];
      $.each(value, function(index, value){
      	if(value) {
      		htmls.push('<tr>\
          		<td>'+ value.tanggal +'</td>\
          	</tr>\
            <tr>\
                <td>'+ "<a href='page_ready_inbox_message.html'><strong>"+value.notifikasi+"</strong></a>" +'</td>\
              </tr>\
              <br>');
      	}
      	lastIndex = index;
      });
      $('#notifikasi').html(htmls);
      $("#submitUser").removeClass('desabled');
    });

    $('#submitUser').on('click', function(){
  	var values = $("#addUser").serializeArray();
  	var first_name = values[0].value;
  	var last_name = values[1].value;
  	var userID = lastIndex+1;

      firebase.database().ref('users/' + userID).set({
          first_name: first_name,
          last_name: last_name,
      });

      // Reassign lastID value
      lastIndex = userID;
  	$("#addUser input").val("");
  });

  // Update Data
  var updateID = 0;
  $('body').on('click', '.updateData', function() {
  	updateID = $(this).attr('data-id');
  	firebase.database().ref('users/' + updateID).on('value', function(snapshot) {
  		var values = snapshot.val();
  		var updateData = '<div class="form-group">\
  		        <label for="first_name" class="col-md-12 col-form-label">First Name</label>\
  		        <div class="col-md-12">\
  		            <input id="first_name" type="text" class="form-control" name="first_name" value="'+values.first_name+'" required autofocus>\
  		        </div>\
  		    </div>\
  		    <div class="form-group">\
  		        <label for="last_name" class="col-md-12 col-form-label">Last Name</label>\
  		        <div class="col-md-12">\
  		            <input id="last_name" type="text" class="form-control" name="last_name" value="'+values.last_name+'" required autofocus>\
  		        </div>\
  		    </div>';

  		    $('#updateBody').html(updateData);
  	});
  });

  $('.updateUserRecord').on('click', function() {
  	var values = $(".users-update-record-model").serializeArray();
  	var postData = {
  	    first_name : values[0].value,
  	    last_name : values[1].value,
  	};

  	var updates = {};
  	updates['/users/' + updateID] = postData;

  	firebase.database().ref().update(updates);

  	$("#update-modal").modal('hide');
  });


  // Remove Data
  $("body").on('click', '.removeData', function() {
  	var id = $(this).attr('data-id');
  	$('body').find('.users-remove-record-model').append('<input name="id" type="hidden" value="'+ id +'">');
  });

  $('.deleteMatchRecord').on('click', function(){
  	var values = $(".users-remove-record-model").serializeArray();
  	var id = values[0].value;
  	firebase.database().ref('users/' + id).remove();
      $('body').find('.users-remove-record-model').find( "input" ).remove();
  	$("#remove-modal").modal('hide');
  });
  $('.remove-data-from-delete-form').click(function() {
  	$('body').find('.users-remove-record-model').find( "input" ).remove();
  });
  </script>


  <!-- END Stylesheets -->
  @yield('frolaCSS')




</head>

<body>
  <!-- Page Wrapper -->
  <!-- In the PHP version you can set the following options from inc/config file -->
  <!--
            Available classes:

            'page-loading'      enables page preloader
        -->
  <div id="page-wrapper">
    <!-- Preloader -->
    <!-- Preloader functionality (initialized in js/app.js) - pageLoading() -->
    <!-- Used only if page preloader is enabled from inc/config (PHP version) or the class 'page-loading' is added in #page-wrapper element (HTML version) -->
    <div class="preloader themed-background">
      <h1 class="push-top-bottom text-light text-center"><strong>Pro</strong>UI</h1>
      <div class="inner">
        <h3 class="text-light visible-lt-ie10"><strong>Loading..</strong></h3>
        <div class="preloader-spinner hidden-lt-ie10"></div>
      </div>
    </div>
    <!-- END Preloader -->

    <!-- Page Container -->
    <!-- In the PHP version you can set the following options from inc/config file -->
    <!--
                Available #page-container classes:

                '' (None)                                       for a full main and alternative sidebar hidden by default (> 991px)

                'sidebar-visible-lg'                            for a full main sidebar visible by default (> 991px)
                'sidebar-partial'                               for a partial main sidebar which opens on mouse hover, hidden by default (> 991px)
                'sidebar-partial sidebar-visible-lg'            for a partial main sidebar which opens on mouse hover, visible by default (> 991px)
                'sidebar-mini sidebar-visible-lg-mini'          for a mini main sidebar with a flyout menu, enabled by default (> 991px + Best with static layout)
                'sidebar-mini sidebar-visible-lg'               for a mini main sidebar with a flyout menu, disabled by default (> 991px + Best with static layout)

                'sidebar-alt-visible-lg'                        for a full alternative sidebar visible by default (> 991px)
                'sidebar-alt-partial'                           for a partial alternative sidebar which opens on mouse hover, hidden by default (> 991px)
                'sidebar-alt-partial sidebar-alt-visible-lg'    for a partial alternative sidebar which opens on mouse hover, visible by default (> 991px)

                'sidebar-partial sidebar-alt-partial'           for both sidebars partial which open on mouse hover, hidden by default (> 991px)

                'sidebar-no-animations'                         add this as extra for disabling sidebar animations on large screens (> 991px) - Better performance with heavy pages!

                'style-alt'                                     for an alternative main style (without it: the default style)
                'footer-fixed'                                  for a fixed footer (without it: a static footer)

                'disable-menu-autoscroll'                       add this to disable the main menu auto scrolling when opening a submenu

                'header-fixed-top'                              has to be added only if the class 'navbar-fixed-top' was added on header.navbar
                'header-fixed-bottom'                           has to be added only if the class 'navbar-fixed-bottom' was added on header.navbar

                'enable-cookies'                                enables cookies for remembering active color theme when changed from the sidebar links
            -->
    <div id="page-container" class="sidebar-partial sidebar-visible-lg sidebar-no-animations">
      <!-- Alternative Sidebar -->
      <div id="sidebar-alt">
        <!-- Wrapper for scrolling functionality -->
        <div id="sidebar-alt-scroll">
          <!-- Sidebar Content -->
          <div class="sidebar-content">
            <!-- Chat -->
            <!-- Chat demo functionality initialized in js/app.js -> chatUi() -->

            <!-- END Chat Users -->

            <!-- Chat Talk -->
            <div class="chat-talk display-none">
              <!-- Chat Info -->
              <div class="chat-talk-info sidebar-section">
                <button id="chat-talk-close-btn" class="btn btn-xs btn-default pull-right">
                  <i class="fa fa-times"></i>
                </button>
                <img src="{{asset('public/backend/img/placeholders/avatars/avatar5.jpg')}}" alt="avatar" class="img-circle pull-left">
                <strong>John</strong> Doe
              </div>
              <!-- END Chat Info -->

              <!-- Chat Messages -->
              <ul class="chat-talk-messages">
                <li class="text-center"><small>Yesterday, 18:35</small></li>
                <li class="chat-talk-msg animation-slideRight">Hey admin?</li>
                <li class="chat-talk-msg animation-slideRight">How are you?</li>
                <li class="text-center"><small>Today, 7:10</small></li>
                <li class="chat-talk-msg chat-talk-msg-highlight themed-border animation-slideLeft">I'm fine, thanks!</li>
              </ul>
              <!-- END Chat Messages -->

              <!-- Chat Input -->
              <form action="index.html" method="post" id="sidebar-chat-form" class="chat-form">
                <input type="text" id="sidebar-chat-message" name="sidebar-chat-message" class="form-control form-control-borderless" placeholder="Type a message..">
              </form>
              <!-- END Chat Input -->
            </div>
            <!--  END Chat Talk -->
            <!-- END Chat -->

            <!-- Activity -->

            <!-- END Activity -->

            <!-- Messages -->
            <a href="page_ready_inbox.html" class="sidebar-title">
              <i class="fa fa-envelope pull-right"></i> <strong>Messages</strong>UI (5)
            </a>

            <div class="sidebar-section">
              <div class="alert alert-alt" id="notifikasi">


                  <!-- Notifikasi -->



                <br>

              </div>
            </div>

            <!-- END Messages -->
          </div>
          <!-- END Sidebar Content -->
        </div>
        <!-- END Wrapper for scrolling functionality -->
      </div>
      <!-- END Alternative Sidebar -->

      <!-- Main Sidebar -->
      <div id="sidebar">
        <!-- Wrapper for scrolling functionality -->
        <div id="sidebar-scroll">
          <!-- Sidebar Content -->
          <div class="sidebar-content">
            <!-- Brand -->
            <a href="index.html" class="sidebar-brand">
              <i class="gi gi-flash"></i><span class="sidebar-nav-mini-hide"><strong>Pro</strong>UI</span>
            </a>
            <!-- END Brand -->

            <!-- User Info -->
            <div class="sidebar-section sidebar-user clearfix sidebar-nav-mini-hide">
              <div class="sidebar-user-avatar">
                <a href="page_ready_user_profile.html">
                  <img src="{{asset('public/backend/img/placeholders/avatars/avatar2.jpg')}}" alt="avatar">
                </a>
              </div>
              <div class="sidebar-user-name">{{$nama}}</div>
              <div class="sidebar-user-links">
                <a href="page_ready_user_profile.html" data-toggle="tooltip" data-placement="bottom" title="Profile"><i class="gi gi-user"></i></a>
                <a href="page_ready_inbox.html" data-toggle="tooltip" data-placement="bottom" title="Messages"><i class="gi gi-envelope"></i></a>
                <!-- Opens the user settings modal that can be found at the bottom of each page (page_footer.html in PHP version) -->
                <a href="javascript:void(0)" class="enable-tooltip" data-placement="bottom" title="Settings" onclick="$('#modal-user-settings').modal('show');"><i class="gi gi-cogwheel"></i></a>
                <a href="login.html" data-toggle="tooltip" data-placement="bottom" title="Logout"><i class="gi gi-exit"></i></a>
              </div>
            </div>
            <!-- END User Info -->

            <!-- Theme Colors -->
            <!-- Change Color Theme functionality can be found in js/app.js - templateOptions() -->
            <ul class="sidebar-section sidebar-themes clearfix sidebar-nav-mini-hide">
              <!-- You can also add the default color theme
                                <li class="active">
                                    <a href="javascript:void(0)" class="themed-background-dark-default themed-border-default" data-theme="default" data-toggle="tooltip" title="Default Blue"></a>
                                </li>
                                -->
              <li>
                <a href="javascript:void(0)" class="themed-background-dark-night themed-border-night" data-theme="css/themes/night.css" data-toggle="tooltip" title="Night"></a>
              </li>
              <li>
                <a href="javascript:void(0)" class="themed-background-dark-amethyst themed-border-amethyst" data-theme="css/themes/amethyst.css" data-toggle="tooltip" title="Amethyst"></a>
              </li>
              <li>
                <a href="javascript:void(0)" class="themed-background-dark-modern themed-border-modern" data-theme="css/themes/modern.css" data-toggle="tooltip" title="Modern"></a>
              </li>
              <li>
                <a href="javascript:void(0)" class="themed-background-dark-autumn themed-border-autumn" data-theme="css/themes/autumn.css" data-toggle="tooltip" title="Autumn"></a>
              </li>
              <li>
                <a href="javascript:void(0)" class="themed-background-dark-flatie themed-border-flatie" data-theme="css/themes/flatie.css" data-toggle="tooltip" title="Flatie"></a>
              </li>
              <li>
                <a href="javascript:void(0)" class="themed-background-dark-spring themed-border-spring" data-theme="css/themes/spring.css" data-toggle="tooltip" title="Spring"></a>
              </li>
              <li>
                <a href="javascript:void(0)" class="themed-background-dark-fancy themed-border-fancy" data-theme="css/themes/fancy.css" data-toggle="tooltip" title="Fancy"></a>
              </li>
              <li>
                <a href="javascript:void(0)" class="themed-background-dark-fire themed-border-fire" data-theme="css/themes/fire.css" data-toggle="tooltip" title="Fire"></a>
              </li>
              <li>
                <a href="javascript:void(0)" class="themed-background-dark-coral themed-border-coral" data-theme="css/themes/coral.css" data-toggle="tooltip" title="Coral"></a>
              </li>
              <li>
                <a href="javascript:void(0)" class="themed-background-dark-lake themed-border-lake" data-theme="css/themes/lake.css" data-toggle="tooltip" title="Lake"></a>
              </li>
              <li>
                <a href="javascript:void(0)" class="themed-background-dark-forest themed-border-forest" data-theme="css/themes/forest.css" data-toggle="tooltip" title="Forest"></a>
              </li>
              <li>
                <a href="javascript:void(0)" class="themed-background-dark-waterlily themed-border-waterlily" data-theme="css/themes/waterlily.css" data-toggle="tooltip" title="Waterlily"></a>
              </li>
              <li>
                <a href="javascript:void(0)" class="themed-background-dark-emerald themed-border-emerald" data-theme="css/themes/emerald.css" data-toggle="tooltip" title="Emerald"></a>
              </li>
              <li>
                <a href="javascript:void(0)" class="themed-background-dark-blackberry themed-border-blackberry" data-theme="css/themes/blackberry.css" data-toggle="tooltip" title="Blackberry"></a>
              </li>
            </ul>
            <!-- END Theme Colors -->

            <!-- Sidebar Navigation -->
            <ul class="sidebar-nav">
              <li>
                <a href="{{ url('satkerindex') }}"><i class="gi gi-stopwatch sidebar-nav-icon"></i><span class="sidebar-nav-mini-hide">Dashboard</span></a>
              </li>
              <li>
                <a href="{{ url('satkeragenda') }}"><i class="gi gi-leaf sidebar-nav-icon"></i><span class="sidebar-nav-mini-hide">Agenda</span></a>
              </li>
              <li>
                <a href="{{ url('satkerkak') }}"><i class="gi gi-leaf sidebar-nav-icon"></i><span class="sidebar-nav-mini-hide">Tambah KAK</span></a>
              </li>
            </ul>
            <!-- END Sidebar Navigation -->

            <!-- Sidebar Notifications -->

            <!-- END Sidebar Notifications -->
          </div>
          <!-- END Sidebar Content -->
        </div>
        <!-- END Wrapper for scrolling functionality -->
      </div>
      <!-- END Main Sidebar -->

      <!-- Main Container -->
      <div id="main-container">
        <!-- Header -->
        <!-- In the PHP version you can set the following options from inc/config file -->
        <!--
                        Available header.navbar classes:

                        'navbar-default'            for the default light header
                        'navbar-inverse'            for an alternative dark header

                        'navbar-fixed-top'          for a top fixed header (fixed sidebars with scroll will be auto initialized, functionality can be found in js/app.js - handleSidebar())
                            'header-fixed-top'      has to be added on #page-container only if the class 'navbar-fixed-top' was added

                        'navbar-fixed-bottom'       for a bottom fixed header (fixed sidebars with scroll will be auto initialized, functionality can be found in js/app.js - handleSidebar()))
                            'header-fixed-bottom'   has to be added on #page-container only if the class 'navbar-fixed-bottom' was added
                    -->
        <header class="navbar navbar-default">
          <!-- Left Header Navigation -->
          <ul class="nav navbar-nav-custom">
            <!-- Main Sidebar Toggle Button -->
            <li>
              <a href="javascript:void(0)" onclick="App.sidebar('toggle-sidebar');this.blur();">
                <i class="fa fa-bars fa-fw"></i>
              </a>
            </li>
            <!-- END Main Sidebar Toggle Button -->

            <!-- Template Options -->
            <!-- Change Options functionality can be found in js/app.js - templateOptions() -->
            <li class="dropdown">
              <a href="javascript:void(0)" class="dropdown-toggle" data-toggle="dropdown">
                <i class="gi gi-settings"></i>
              </a>
              <ul class="dropdown-menu dropdown-custom dropdown-options">
                <li class="dropdown-header text-center">Header Style</li>
                <li>
                  <div class="btn-group btn-group-justified btn-group-sm">
                    <a href="javascript:void(0)" class="btn btn-primary" id="options-header-default">Light</a>
                    <a href="javascript:void(0)" class="btn btn-primary" id="options-header-inverse">Dark</a>
                  </div>
                </li>
                <li class="dropdown-header text-center">Page Style</li>
                <li>
                  <div class="btn-group btn-group-justified btn-group-sm">
                    <a href="javascript:void(0)" class="btn btn-primary" id="options-main-style">Default</a>
                    <a href="javascript:void(0)" class="btn btn-primary" id="options-main-style-alt">Alternative</a>
                  </div>
                </li>
              </ul>
            </li>
            <!-- END Template Options -->
          </ul>
          <!-- END Left Header Navigation -->

          <!-- Search Form -->
          <form action="page_ready_search_results.html" method="post" class="navbar-form-custom">
            <div class="form-group">
              <input type="text" id="top-search" name="top-search" class="form-control" placeholder="Search..">
            </div>
          </form>
          <!-- END Search Form -->

          <!-- Right Header Navigation -->
          <ul class="nav navbar-nav-custom pull-right">
            <!-- Alternative Sidebar Toggle Button -->
            <li>
              <!-- If you do not want the main sidebar to open when the alternative sidebar is closed, just remove the second parameter: App.sidebar('toggle-sidebar-alt'); -->
              <a href="javascript:void(0)" onclick="App.sidebar('toggle-sidebar-alt', 'toggle-other');this.blur();">
                <i class="gi gi-share_alt"></i>
                <span class="label label-primary label-indicator animation-floating">5</span>
              </a>
            </li>
            <!-- END Alternative Sidebar Toggle Button -->

            <!-- User Dropdown -->
            <li class="dropdown">
              <a href="javascript:void(0)" class="dropdown-toggle" data-toggle="dropdown">
                <img src="{{asset('public/backend/img/placeholders/avatars/avatar2.jpg')}}" alt="avatar"> <i class="fa fa-angle-down"></i>
              </a>
              <ul class="dropdown-menu dropdown-custom dropdown-menu-right">
                <li class="dropdown-header text-center">Account</li>

                <li class="divider"></li>
                <li>
                  <a href="page_ready_user_profile.html">
                    <i class="fa fa-user fa-fw pull-right"></i>
                    Profile
                  </a>
                  <!-- Opens the user settings modal that can be found at the bottom of each page (page_footer.html in PHP version) -->
                  <a href="#modal-user-settings" data-toggle="modal">
                    <i class="fa fa-cog fa-fw pull-right"></i>
                    Settings
                  </a>
                </li>
                <li class="divider"></li>
                <li>
                  <a href="{{ route('logout') }}" onclick="event.preventDefault();
                               document.getElementById('logout-form').submit();"><i class="icon ion-power"></i>
                    Logout
                  </a>

                  <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                    {{ csrf_field() }}
                  </form>
                </li>

              </ul>
            </li>
            <!-- END User Dropdown -->
          </ul>
          <!-- END Right Header Navigation -->
        </header>
        <!-- END Header -->

        <!-- Page content -->
        @yield('content')
        <!-- END Page Content -->

        <!-- Footer -->
        <footer class="clearfix">
          <div class="pull-right">
            Crafted with <i class="fa fa-heart text-danger"></i> by <a href="http://goo.gl/vNS3I" target="_blank">pixelcave</a>
          </div>
          <div class="pull-left">
            <span id="year-copy"></span> &copy; <a href="http://goo.gl/TDOSuC" target="_blank">ProUI 3.8</a>
          </div>
        </footer>
        <!-- END Footer -->
      </div>
      <!-- END Main Container -->
    </div>
    <!-- END Page Container -->
  </div>
  <!-- END Page Wrapper -->

  <!-- Scroll to top link, initialized in js/app.js - scrollToTop() -->
  <a href="#" id="to-top"><i class="fa fa-angle-double-up"></i></a>

  <!-- User Settings, modal which opens from Settings link (found in top right user menu) and the Cog link (found in sidebar user info) -->
  <div id="modal-user-settings" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
      <div class="modal-content">
        <!-- Modal Header -->
        <div class="modal-header text-center">
          <h2 class="modal-title"><i class="fa fa-pencil"></i> Settings</h2>
        </div>
        <!-- END Modal Header -->

        <!-- Modal Body -->
        <div class="modal-body">
          <form action="index.html" method="post" enctype="multipart/form-data" class="form-horizontal form-bordered" onsubmit="return false;">
            <fieldset>
              <legend>Vital Info</legend>
              <div class="form-group">
                <label class="col-md-4 control-label">Username</label>
                <div class="col-md-8">
                  <p class="form-control-static">Admin</p>
                </div>
              </div>
              <div class="form-group">
                <label class="col-md-4 control-label" for="user-settings-email">Email</label>
                <div class="col-md-8">
                  <input type="email" id="user-settings-email" name="user-settings-email" class="form-control" value="admin@example.com">
                </div>
              </div>
              <div class="form-group">
                <label class="col-md-4 control-label" for="user-settings-notifications">Email Notifications</label>
                <div class="col-md-8">
                  <label class="switch switch-primary">
                    <input type="checkbox" id="user-settings-notifications" name="user-settings-notifications" value="1" checked>
                    <span></span>
                  </label>
                </div>
              </div>
            </fieldset>
            <fieldset>
              <legend>Password Update</legend>
              <div class="form-group">
                <label class="col-md-4 control-label" for="user-settings-password">New Password</label>
                <div class="col-md-8">
                  <input type="password" id="user-settings-password" name="user-settings-password" class="form-control" placeholder="Please choose a complex one..">
                </div>
              </div>
              <div class="form-group">
                <label class="col-md-4 control-label" for="user-settings-repassword">Confirm New Password</label>
                <div class="col-md-8">
                  <input type="password" id="user-settings-repassword" name="user-settings-repassword" class="form-control" placeholder="..and confirm it!">
                </div>
              </div>
            </fieldset>
            <div class="form-group form-actions">
              <div class="col-xs-12 text-right">
                <button type="button" class="btn btn-sm btn-default" data-dismiss="modal">Close</button>
                <button type="submit" class="btn btn-sm btn-primary">Save Changes</button>
              </div>
            </div>
          </form>
        </div>
        <!-- END Modal Body -->
      </div>
    </div>
  </div>

  <!-- END User Settings -->
  <script type="text/javascript" src="{{ asset('public/js/jquery-3.3.1.min.js') }}"></script>
  <script src="{{asset('public/backend/js/vendor/jquery.min.js')}}"></script>
  <script src="{{asset('public/backend/js/vendor/bootstrap.min.js')}}"></script>
  <script src="{{asset('public/backend/js/plugins.js')}}"></script>
  <script src="{{asset('public/backend/js/app.js')}}"></script>
  <script type="text/javascript" src="{{ asset('public/js/validator.js') }}"></script>
  <script type="text/javascript" src='https://cdnjs.cloudflare.com/ajax/libs/fullcalendar/3.9.0/locale/id.js'></script>
  @yield('script')
  @yield('js')
  @yield('frolaJS')
  <script>
    $(function() {
      FormsWizard.init();
      TablesDatatables.init();
      $('textarea').froalaEditor();
    });
  </script>



</body>

</html>
