<!DOCTYPE html>
<!--[if IE 8 ]><html class="ie ie8" class="no-js" lang="en"> <![endif]-->
<!--[if (gte IE 9)|!(IE)]><!--><html class="no-js" lang="en"> <!--<![endif]-->
<head>
    <meta http-equiv="content-type" content="text/html; charset=UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <title>Cokelat Ibuke | @yield('pageTitle')</title>
    <meta name="description" content="">

    <!-- CSS FILES -->
    <link rel="stylesheet" type="text/css" href="{{ asset('public/user/css/vendor/bootstrap.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('public/user/css/style.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('public/user/css/style.css') }}" media="screen" data-name="skins">
    <link rel="stylesheet" href="{{ asset('public/user/css/style-fraction.css') }}" type="text/css" charset="utf-8" />
    <link rel="stylesheet" href="{{ asset('public/user/css/fractionslider.css') }}">

    <link rel="stylesheet" type="text/css" href="{{ asset('public/user/css/switcher.css') }}" media="screen" />
    <link rel="shortcut icon" type="image/x-icon" href="{{ asset('public/user/images/LOGO COKELAT IBUKE 2.png') }}" />
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
<!--[if lt IE 9]>
<script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
<![endif]-->
</head>
<body>
    <!--Start Header-->
    <header id="header" class="clearfix" style="background: #FBD006 !important">
        <div id="top-bar">
            <div class="container">
                <div class="row">
                    <div class="top-info hidden-xs col-sm-7 ">
                        <span><i class="fa fa-phone"></i>WhatsApp: 0812-3367-1967</span>
                        <span><i class="fa fa-envelope"></i>Email: yuyuntrihandini@gmail.com</span>
                    </div>
                    <div class="top-info col-sm-5">
                        <ul class="clearfix">
                            <li><a href="" class="my-tweet"><i class="fa fa-twitter"></i></a></li>
                            <li><a href="" class="my-facebook"><i class="fa fa-facebook"></i></a></li>
                            <li><a href="" class="my-skype"><i class="fa fa-skype"></i></a></li>
                            <li><a href="" class="my-pint"><i class="fa fa-pinterest"></i></a></li>
                            <li><a href="" class="my-rss"><i class="fa fa-rss"></i></a></li>
                            <li><a href="" class="my-google"><i class="fa fa-google-plus"></i></a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
        <!-- Nav Bar -->
        <div id="nav-bar" class="clearfix" style="background: #BA2E32 !important">
            <div class="container-fluid">
                <div class="row">
                    <!-- Logo / Mobile Menu -->
                    <div class="col-sm-2">
                        <div id="logo">
                            <h1><a href="{{ url('/home') }}"><img style="width: 100px; height: 50px; padding-top: 2px; padding-bottom: 2px" src="{{ asset('public/user/images/LOGO COKELAT IBUKE 1.png') }}" alt="Cokelat Ibuke" /></a></h1>
                        </div>
                    </div>

                    <!-- Navigation================================================== -->
                    <div class="col-sm-10">
                        <nav id="navigation" class="menu">
                            <ul id="">
                                <li><a class="{{ Request::is('home','/') ? 'border' : '' }}" href="{{ url('/home') }}">Beranda</a></li>


                                <li><a class="{{ Request::is('praline','lolipop','bar') ? 'border' : '' }}">Produk Regular</a>
                                    <ul>
                                        @foreach($kategori_reguler as $ktr)
                                        <li><a href="{{ url('/kat_reguler') }}/{{ $ktr->id_kategori }}">{{ $ktr->nama_kategori }}</a></li>
                                        @endforeach
                                    </ul>
                                </li>

                                <li><a class="{{ Request::is('lebaran','valentine','ulang-tahun','branding-perusahaan') ? 'border' : '' }}" >Edisi Khusus</a>
                                    <ul>
                                        @foreach($kategori_edisi as $kte)
                                        <li><a href="{{ url('/kat_edisi') }}/{{ $kte->id_kategori }}">{{ $kte->nama_kategori }}</a></li>
                                        @endforeach
                                    </ul>
                                </li>
                                <li><a class="{{ Request::is('agenda') ? 'border' : '' }}" href="{{ url('/agenda') }}">Agenda</a></li>
                                <li><a class="{{ Request::is('cara-belanja') ? 'border' : '' }}" href="{{ url('/cara-belanja') }}">Cara Belanja</a></li>
                                <li><a class="{{ Request::is('testimoni') ? 'border' : '' }}" href="{{ url('/testimoni') }}">Testimoni</a></li>
                                <li><a class="{{ Request::is('tos') ? 'border' : '' }}" href="{{ url('/tos') }}">T.O.S</a></li>
                                <li><a class="{{ Request::is('faq') ? 'border' : '' }}" href="{{route('faq.create')}}">F.A.Q</a></li>

                                <li><a class="{{ Request::is('about') ? 'border' : '' }}" href="{{ url('/about') }}">About</a></li>

                            </ul>
                        </nav>
                    </div>
                </div>
            </div>
        </div>
        <!-- End Nav Bar -->
    </header>
    <!--End Header-->

    <!--start wrapper-->
    <section class="wrapper" style="min-height: 300px">

        <!--Start Slider-->
        @yield('slide')
        <!--End Slider-->

        <!--start content-->
        @yield('content')
        <!--end content-->



    </section>
    <!--end wrapper-->


    <!--start footer-->
    <footer class="footer" >
        <div class="container">
            <div class="row">
                <div class="col-sm-6 col-md-4 col-lg-4">
                    <div class="widget_title">
                        <h4><span>Informasi Kontak</span></h4>

                    </div>
                    <div class="widget_content">
                        <!-- <p align="justify">Usaha ini berdiri sejak tahun 2010 dengan mengeluarkan produk lolipop dan praline bermerk dagang “frenzy”. Pada awalnya produksi dilakukan hanya pada saat ada pesanan saja dan pemasarannya hanya sebatas teman. Tahun 2013 kami mengeluarkan produk baru, yaitu cokelat bar dengan merk dagang “Ibuke”.</p> -->
                        <ul class="contact-details-alt">
                            <li><i class="fa fa-map-marker"></i> <p><strong>Alamat</strong>: Jl. Jaksa Agung Suprapto III/15B Kota Kediri Jawa Timur</p></li>
                            <li><i class="fa fa-phone-square"></i> <p><strong>Telephone</strong>: 0354-775146</p></li>
                            <li><i class="fa fa-mobile"></i> <p><strong>WhatsApp</strong>: 0812-3367-1967</p></li>
                            <li><i class="fa fa-phone"></i> <p><strong>HandPhone</strong>: 0811-3706-03</p></li>
                            <li><i class="fa fa-envelope"></i> <p><strong>Email</strong>: <a href="#">yuyuntrihandini@gmail.com</a></p></li>
                        </ul>
                    </div>
                </div>
                <div class="col-sm-6 col-md-4 col-lg-4">
                    <div class="widget_title">
                        <h4><span>LAPAK DI MARKETPLACE</span></h4>

                    </div>
                    <div class="widget_content">
                        <div class="flickr">
                            <a href="http://www.bukalapak.com/"><img width="200px" src="{{ asset('public/user/images/Logo Bukalapak.png') }}" alt="League" /></a>
                        </div>
                        <div class="flickr">
                            <a style="padding-top: 100px" href="http://www.tokopedia.com/"><img width="200px" src="{{ asset('public/user/images/Logo Tokopedia.png') }}" alt="League" /></a>
                        </div>
                    </div>
                </div>
                <div class="col-sm-6 col-md-4 col-lg-4">
                    <div class="widget_title">
                        <h4><span>Jasa Pengiriman</span></h4>

                    </div>
                    <div class="widget_content">
                        <div class="flickr">
                            <a href="http://www.posindonesia.co.id/"><img width="100px" src="{{ asset('public/user/images/Logo Pos.png') }}" alt="POS Indonesia" /></a>
                            <a href="http://www.jne.co.id/"><img width="100px" src="{{ asset('public/user/images/Logo JNE.png') }}" alt="JNE" /></a>
                            <a href="http://www.jet.co.id/"><img width="100px" src="{{ asset('public/user/images/Logo J&T.png') }}" alt="J&T" /></a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </footer>
    <!--end footer-->

    <section class="footer_bottom" style="background: #BA2E32 !important">
        <div class="container">
            <div class="row">
                <div class="col-lg-6 col-md-6 col-sm-6">
                    <p class="copyright">&copy; Copyright 2018 <a href="{{ url('/') }}">Cokelat Ibuke </a></p>
                </div>

                <div class="col-lg-6 col-md-6 col-sm-6">
                    <div class="footer_social">
                        <ul class="footbot_social">
                            <li><a class="fb" href="#." data-placement="top" data-toggle="tooltip" title="Facbook"><i class="fa fa-facebook"></i></a></li>
                            <li><a class="twtr" href="#." data-placement="top" data-toggle="tooltip" title="Twitter"><i class="fa fa-twitter"></i></a></li>
                            <li><a class="dribbble" href="#." data-placement="top" data-toggle="tooltip" title="Dribbble"><i class="fa fa-dribbble"></i></a></li>
                            <li><a class="skype" href="#." data-placement="top" data-toggle="tooltip" title="Skype"><i class="fa fa-skype"></i></a></li>
                            <li><a class="rss" href="#." data-placement="top" data-toggle="tooltip" title="RSS"><i class="fa fa-rss"></i></a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </section>


    <script type="text/javascript" src="{{ asset('public/user/js/vendor/jquery-1.10.2.min.js') }}"></script>
    <script src="{{ asset('public/user/js/vendor/bootstrap.js') }}"></script>
    <script src="{{ asset('public/user/js/jquery.easing.1.3.js') }}"></script>
    <script src="{{ asset('public/user/js/retina-1.1.0.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('public/user/js/jquery.cookie.js') }}"></script> <!-- jQuery cookie -->
    <script type="text/javascript" src="{{ asset('public/user/js/styleswitch.js') }}"></script> <!-- Style Colors Switcher -->
    <script src="{{ asset('public/user/js/jquery.fractionslider.js') }}" type="text/javascript" charset="utf-8"></script>

    <script src="{{ asset('public/user/js/jquery.superfish.js') }}"></script>
    <script src="{{ asset('public/user/js/jquery.meanmenu.js') }}"></script>

    <script type="text/javascript" src="{{ asset('public/user/js/jquery.jcarousel.js') }}"></script>
    <script type="text/javascript" src="{{ asset('public/user/js/jflickrfeed.js') }}"></script>

    <script type="text/javascript" src="{{ asset('public/user/js/jquery.magnific-popup.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('public/user/js/jquery.isotope.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('public/user/js/swipe.js') }}"></script>

    <script src="{{ asset('public/user/js/main.js') }}"></script>

    <!-- Start Style Switcher -->
    <!-- <div class="switcher"></div> -->
    <!-- End Style Switcher -->
    @yield('js')

    <script type="text/javascript"> /*-- Fraction Slider Parameters --*/
    $(window).load(function(){
        $('.slider').fractionSlider({
            'fullWidth': 			true,
            'controls': 			true,
            'responsive': 			true,
            'dimensions': 			"1920,450",
            'increase': 			true,
            'pauseOnHover': 		true,
            'slideEndAnimation': 	true,
            'autoChange':           true
        });

    });
</script>
</body>
</html>
