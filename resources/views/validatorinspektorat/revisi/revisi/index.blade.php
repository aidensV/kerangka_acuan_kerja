@extends('layouts.validatorbkaad.master')
@section('content')
<div id="page-content">
                       <!-- Datatables Header -->
                       <div class="content-header">
                           <div class="header-section">
                               <h1>
                                   <i class="fa fa-table"></i>

                                   <br><small>

                                   </small>
                               </h1>
                           </div>
                       </div>
                       <ul class="breadcrumb breadcrumb-top">
                           <li>Tables</li>
                           <li><a href="">Datatables</a></li>
                       </ul>
                       <!-- END Datatables Header -->

                       <!-- Datatables Content -->
                       <div class="block full">
                           <div class="block-title">
                               <h2><strong>Datatables</strong> integration</h2>
                           </div>
                           <a href="{{url('validatorbkaadrevisi')}}/{{$idkak}}" class="btn btn-primary">Tambah</a>
                           <br>
                           <br>
                           <div class="table-responsive">
                               <table id="example-datatable" class="table table-vcenter table-condensed table-bordered">
                                   <thead>
                                       <tr>
                                           <th>No</th>
                                           <th>revisi</th>
                                           <th>status revisi</th>
                                           <th>aksi</th>
                                       </tr>
                                   </thead>
                                   <tbody>
                                     @php
                                      $no=1;
                                     @endphp
                                     @foreach($revisi  as $data)

                                       <tr>
                                           <td style="font-size: 15px;">{{$no}}</td>
                                           <td style="font-size: 15px;">{{$data->detailrevisi}}</td>
                                           <td style="font-size: 15px;">

                                             @php
                                              $status_revisi = $data->statusrevisi;
                                              if($status_revisi == '1'){
                                                $status_revisi1 = 'salah';
                                              }elseif($status_revisi == '2'){
                                                $status_revisi1 = 'benar';
                                              }
                                             @endphp
                                             {{$status_revisi1}}

                                           </td>
                                           <td class="text-center">
                                                 <div class="btn-group">
                                                     <a href="{{url('validatorbkaadupdaterevisibenar')}}/{{$data->idrevisi}}" data-toggle="tooltip" title="Detail" class="btn btn-xs btn-default"><i class="fa fa-pencil"> benar</i></a>
                                                 </div>
                                                 <div class="btn-group">
                                                     <a href="" data-toggle="tooltip" title="Detail" class="btn btn-xs btn-default"><i class="fa fa-pencil"> salah</i></a>
                                                 </div>
                                            </td>
                                       </tr>
                                       @php
                                        $no++;
                                       @endphp
                                    @endforeach
                                   </tbody>
                               </table>
                           </div>
                       </div>
                       <!-- END Datatables Content -->
                   </div>
@endsection
@section('script')

@endsection
