@extends('layouts.validatorbkaad.master')
@section('content')
 <div id="page-content">
                        <!-- Dashboard Header -->
                        <!-- For an image header add the class 'content-header-media' and an image as in the following example -->






                        <div class="content-header content-header-media">
                            <div class="header-section">
                                <div class="row">

                                    <div class="col-md-4 col-lg-6 hidden-xs hidden-sm">
                                        <h1>Data <strong>Satker</strong><br><small>You Look Awesome!</small></h1>
                                    </div>



                                    <div class="col-md-8 col-lg-6">
                                        <div class="row text-center">
                                            <div class="col-xs-4 col-sm-3">
                                                <h2 class="animation-hatch">
                                                    $<strong>93.7k</strong><br>
                                                    <small><i class="fa fa-thumbs-o-up"></i> Great</small>
                                                </h2>
                                            </div>
                                            <div class="col-xs-4 col-sm-3">
                                                <h2 class="animation-hatch">
                                                    <strong>167k</strong><br>
                                                    <small><i class="fa fa-heart-o"></i> Likes</small>
                                                </h2>
                                            </div>
                                            <div class="col-xs-4 col-sm-3">
                                                <h2 class="animation-hatch">
                                                    <strong>101</strong><br>
                                                    <small><i class="fa fa-calendar-o"></i> Events</small>
                                                </h2>
                                            </div>

                                            <div class="col-sm-3 hidden-xs">
                                                <h2 class="animation-hatch">
                                                    <strong>27&deg; C</strong><br>
                                                    <small><i class="fa fa-map-marker"></i> Sydney</small>
                                                </h2>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <img src="{{asset('public/backend/img/placeholders/headers/dashboard_header.jpg')}}" alt="header image" class="animation-pulseSlow">
                        </div>



                        <div class="row">
                            <div class="col-md-12">
                                <!-- Basic Form Elements Block -->
                                <div class="block">
                                    <!-- Basic Form Elements Title -->
                                    <div class="block-title">
                                        <div class="block-options pull-right">
                                            <a href="javascript:void(0)" class="btn btn-alt btn-sm btn-default toggle-bordered enable-tooltip" data-toggle="button" title="Toggles .form-bordered class">No Borders</a>
                                        </div>
                                        <h2><strong>Basic Form</strong> Elements</h2>
                                    </div>
                                    <!-- END Form Elements Title -->

                                    <!-- Basic Form Elements Content -->
                                    <form class="form-horizontal tasi-form" action="{{ url('validatorbkaadtambahrevisi') }}" method="post" enctype="multipart/form-data">
                                      {{csrf_field()}}
                                      <input type="hidden" id="idkak" name="idkak" class="form-control" placeholder="Text" value="{{$idkak}}" >
                                        <div class="form-group">
                                            <label class="col-md-3 control-label">idkak</label>
                                            <div class="col-md-9">
                                                <p class="form-control-static">{{$idkak}}</p>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-md-3 control-label" for="example-textarea-input">revisi</label>
                                            <div class="col-md-9">
                                                <textarea id="revisi" name="revisi"  class="form-control" placeholder="revisi.."></textarea>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-md-3 control-label" for="example-textarea-input"></label>
                                            <div class="col-md-9">
                                                <button type="submit" class="btn btn-primary">Tambah</button>
                                                <a href="{{url('validatorbapedaajukanrevisi')}}/{{$idkak}}" class="btn btn-danger">Kembali</a>
                                            </div>
                                        </div>

                                    </form>

                                    <!-- END Basic Form Elements Content -->
                                </div>
                                <!-- END Basic Form Elements Block -->
                            </div>

                        </div>




                        </div>
@endsection
