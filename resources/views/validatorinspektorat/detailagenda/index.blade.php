@extends('layouts.validatorinspektorat.master')
@section('content')
<div id="page-content">
                       <!-- Datatables Header -->
                       <div class="content-header">
                           <div class="header-section">
                               <h1>
                                   <i class="fa fa-table"></i>Datatables<br><small>HTML tables can become fully dynamic with cool features!</small>
                               </h1>
                           </div>
                       </div>
                       <ul class="breadcrumb breadcrumb-top">
                           <li>Tables</li>
                           <li><a href="">Datatables</a></li>
                       </ul>
                       <!-- END Datatables Header -->

                       <!-- Datatables Content -->
                       <div class="block full">
                           <div class="block-title">
                               <h2><strong>Datatables</strong> integration</h2>
                           </div>
                           <p><a href="https://datatables.net/" target="_blank">DataTables</a> is a plug-in for the Jquery Javascript library. It is a highly flexible tool, based upon the foundations of progressive enhancement, which will add advanced interaction controls to any HTML table. It is integrated with template's design and it offers many features such as on-the-fly filtering and variable length pagination.</p>

                           <div class="table-responsive">
                               <table id="example-datatable" class="table table-vcenter table-condensed table-bordered">
                                   <thead>
                                       <tr>
                                           <th>No</th>
                                           <th>agenda</th>
                                           <th>mulai</th>
                                           <th>selesai</th>
                                           <th>status</th>
                                           <!-- <th>aksi</th> -->
                                       </tr>
                                   </thead>
                                   <tbody>
                                     @php
                                      $no=1;
                                     @endphp
                                     @foreach($detailagenda  as $data)

                                     @php
                                      $statuspengerjaan = $data->statuspengerjaan;
                                      if($statuspengerjaan == 1){
                                        $statuspengerjaan1 = 'Belum';
                                      }else if($statuspengerjaan == 2){
                                        $statuspengerjaan1 = 'Proses';
                                      }else if($statuspengerjaan == 3){
                                        $statuspengerjaan1 = 'Sudah';
                                      }else{

                                      }
                                     @endphp

                                       <tr>
                                           <td style="font-size: 15px;">{{$no}}</td>
                                           <td style="font-size: 15px;">{{$data->title}}</td>
                                           <td style="font-size: 15px;">{{$data->start}}</td>
                                           <td style="font-size: 15px;">{{$data->end}}</td>
                                           <td style="font-size: 15px;">{{$statuspengerjaan1}}</td>
                                           <!-- <td class="text-center">
                                                <div class="btn-group">
                                                    <a href="" data-toggle="tooltip" title="Detail" class="btn btn-xs btn-default"><i class="fa fa-pencil"> Proses Pelaksanaan</i></a>
                                                </div>
                                                <div class="btn-group">
                                                    <a href="" data-toggle="tooltip" title="Detail" class="btn btn-xs btn-default"><i class="fa fa-pencil"> Terlaksana</i></a>
                                                </div>
                                            </td> -->
                                       </tr>
                                       @php
                                        $no++;
                                       @endphp
                                    @endforeach
                                   </tbody>
                               </table>
                           </div>
                       </div>
                       <!-- END Datatables Content -->
                   </div>
@endsection
@section('script')

<script>
var TablesDatatables = function() {

    return {
        init: function() {
            /* Initialize Bootstrap Datatables Integration */
            App.datatables();

            /* Initialize Datatables */
            $('#example-datatable').dataTable({

                columnDefs: [ { orderable: false, targets: [ 1, 4 ] } ],
                pageLength: 10,
                lengthMenu: [[10, 20, 30, -1], [10, 20, 30, 'All']],
                search: {
                  "search": "{{$namakegiatan}}"
                }
            });

            /* Add placeholder attribute to the search input */
            $('.dataTables_filter input').attr('placeholder', 'Search');
        }
    };
}();
</script>

@endsection
