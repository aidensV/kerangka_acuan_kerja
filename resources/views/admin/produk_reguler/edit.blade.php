<?php $hal = 'admin_produk_reguler' ?>


@extends('layouts.admin.master')

@section('title', 'Tabel | Admin')

@section('css')
<link href="{{ asset('public/admin/lib/medium-editor/medium-editor.css') }}" rel="stylesheet">
<link href="{{ asset('public/admin/lib/medium-editor/default.css') }}" rel="stylesheet">
<link href="{{ asset('public/admin/lib/summernote/summernote-bs4.css') }}" rel="stylesheet">
<link href="{{ asset('public/admin/lib/select2/css/select2.min.css') }}" rel="stylesheet">
<link href="{{ asset('public/admin/lib/spectrum/spectrum.css') }}" rel="stylesheet">

@endsection

@section('content')
<!-- ##### MAIN PANEL ##### -->

<div class="kt-pagetitle">
  <h5>Produk Reguler</h5>
</div><!-- kt-pagetitle -->

<div class="kt-pagebody">

  <div class="card pd-20 pd-sm-40">

    <div class="table-wrapper">
      @foreach($produk as $data)
      <form class="form-horizontal tasi-form" action="{{ route('admin_produk_reguler.update',$data->id_produk) }}" method="post" enctype="multipart/form-data">
                {{csrf_field()}}
                {{ method_field('PUT') }}

                @if ($errors->any())
    							<div class="alert alert-danger"><ul>
    							@foreach($errors->all() as $error)
    							<li>{{ $error }}</li>
    							@endforeach
    							</ul></div>
    						@endif


                                  <div class="form-group">
                                      <label class="col-sm-2 col-sm-2 control-label">Kode Produk</label>
                                      <div class="col-sm-10">
                                          <input type="text" value="{{$data->kode_produk}}" class="form-control" name="kode_produk" placeholder="Isikan Kode Produk">
                                      </div>
                                  </div>

                                  <div class="form-group">
                                    <label for="kategori" class="col-md-3 control-label">Kategori</label>
                      							<div class="col-md-6">
                      								<select id="kategori" class="form-control" name="kategori_produk">
                      									<option value="">-- Pilih --</option>
                      									@foreach($kategori as $list)
                      									<option value="{{ $list->id_kategori }}" {{ $data->id_kategori==$list->id_kategori ? 'selected' : '' }} > {{ $list->nama_kategori }} </option>
                      									@endforeach
                      								</select>
                      							</div>
                                  </div>


                                  <div class="form-group">
                                      <label class="col-sm-2 col-sm-2 control-label">Nama Produk</label>
                                      <div class="col-sm-10">
                                          <input type="text"  value="{{$data->nama_produk}}" class="form-control" name="nama_produk" placeholder="Isikan Nama Produk">
                                      </div>
                                  </div>

                                  <div class="form-group">
                                      <label class="col-sm-2 col-sm-2 control-label">Isi</label>
                                      <div class="col-sm-10">
                                          <input type="text"  value="{{$data->isi_produk}}" class="form-control" name="isi_produk" placeholder="Keterangan Isi">
                                      </div>
                                  </div>

                                  <div class="form-group">
                                      <label class="col-sm-2 col-sm-2 control-label">Berat</label>
                                      <div class="col-sm-10">
                                          <input type="text"  value="{{$data->berat_produk}}" class="form-control" name="berat_produk" placeholder="Isikan Berat Produk">
                                      </div>
                                  </div>

                                  <div class="form-group">
                                      <label class="col-sm-2 col-sm-2 control-label">Dimensi</label>
                                      <div class="col-sm-10">
                                          <input type="text"  value="{{$data->dimensi_produk}}" class="form-control" name="dimensi_produk" placeholder="Isikan Dimensi Produk">
                                      </div>
                                  </div>

                                  <div class="form-group">
                                      <label class="col-sm-2 col-sm-2 control-label">Harga</label>
                                      <div class="col-sm-10">
                                        <div class="input-group">
                                          <span class="input-group-addon">Rp.</span>
                                          <input type="number"  value="{{$data->harga_produk}}" class="form-control" name="harga_produk" placeholder="Isikan Harga Produk">
                                        </div>
                                      </div>
                                  </div>


                                  <div class="form-group">
                                        <label class="col-sm-2 col-sm-2 control-label">Gambar</label>
                                        <div class="col-sm-10">
                                            <input type="file" class="form-control" name="gambar">
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="col-sm-2 col-sm-2 control-label">Keterangan</label>
                                        <div class="col-sm-10">
                                            <textarea type="text"  class="form-control" name="keterangan_produk" placeholder="Isikan Keterangan Produk"> {{$data->keterangan_produk}}</textarea>
                                        </div>
                                    </div>


                <div class="form-group">
                                      <label class="col-sm-2 col-sm-2 control-label"></label>
                                      <div class="col-sm-10">
                                          <button type="submit" class="btn btn-primary">Update</button>
                                      </div>
                                  </div>

                              </form>
                              @endforeach
    </div><!-- table-wrapper -->
  </div><!-- card -->






     </div><!-- kt-pagebody -->




@endsection


@section('js')
  <!-- <script src="{{ asset('admin/lib/jquery-ui/jquery-ui.js') }}"></script> -->
  <!-- <script src="{{ asset('public/admin/lib/highlightjs/highlight.pack.js') }}"></script>
  <script src="{{ asset('public/admin/lib/highlightjs/highlight.pack.js') }}"></script>
  <script src="{{ asset('public/admin/lib/datatables/jquery.dataTables.js') }}"></script>
  <script src="{{ asset('public/admin/lib/datatables-responsive/dataTables.responsive.js') }}"></script> -->

<script src="{{ asset('public/admin/lib/highlightjs/highlight.pack.js') }}"></script>
<script src="{{ asset('public/admin/lib/medium-editor/medium-editor.js') }}"></script>
<script src="{{ asset('public/admin/lib/summernote/summernote-bs4.min.js') }}"></script>
<script src="{{ asset('public/admin/lib/jquery-ui/jquery-ui.js') }}"></script>
<script src="{{ asset('public/admin/lib/highlightjs/highlight.pack.js') }}"></script>
<script src="{{ asset('public/admin/lib/select2/js/select2.min.js') }}"></script>
<script src="{{ asset('public/admin/lib/spectrum/spectrum.js') }}"></script>


@endsection

@section('script')



<script>
      $(function(){
        'use strict';

        // Inline editor
        var editor = new MediumEditor('.editable');

        // Summernote editor
        $('#summernote').summernote({
          height: 150,
          tooltip: false
        });

        'use strict';

        $('.select2').select2({
          minimumResultsForSearch: Infinity
        });

        // Select2 by showing the search
        $('.select2-show-search').select2({
          minimumResultsForSearch: ''
        });

        // Select2 with tagging support
        $('.select2-tag').select2({
          tags: true,
          tokenSeparators: [',', ' ']
        });


        // Datepicker
        $('.fc-datepicker').datepicker({
          showOtherMonths: true,
          selectOtherMonths: true
        });

      });
    </script>

@endsection
