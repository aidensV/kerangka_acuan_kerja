<?php $hal = 'admin_produk_reguler' ?>


@extends('layouts.admin.master')

@section('title', 'Produk Reguler | Admin')

@section('content')
    <!-- ##### MAIN PANEL ##### -->

    <div class="kt-pagetitle">
      <h5>Produk Reguler</h5>
    </div><!-- kt-pagetitle -->

    <div class="kt-pagebody">

      @foreach($produk as $data)

       <div class="row">
         <div class="col-md-4 col-lg-3">
           <label class="content-left-label">Gambar Produk </label>
           <figure class="edit-profile-photo">
             <img src="{{asset('public/images')}}/{{$data->gambar_produk}}" class="img-fluid" alt="">

           </figure>
         </div><!-- col-3 -->
         <div class="col-md-8 col-lg-9 mg-t-30 mg-md-t-0">


           <label class="content-left-label">Informasi Produk </label>
           <div class="card bg-gray-200 bd-0">
             <div class="edit-profile-form">
               <div class="form-group row">
                 <label class="col-sm-3 form-control-label">Kode Produk</label>
                 <div class="col-sm-8 col-xl-6 mg-t-10 mg-sm-t-0">
                   <input readonly class="form-control" placeholder="Enter firstname" type="text" value="{{$data->kode_produk}}">
                 </div>
               </div><!-- form-group -->
               <div class="form-group row">
                 <label class="col-sm-3 form-control-label">Katrgori</label>
                 <div class="col-sm-8 col-xl-6 mg-t-10 mg-sm-t-0">
                   <input readonly class="form-control" placeholder="Enter lastname" type="text" value="{{$data->nama_kategori}}">
                 </div>
               </div><!-- form-group -->
               <div class="form-group row">
                 <label class="col-sm-3 form-control-label">Nama Produk</label>
                 <div class="col-sm-8 col-xl-6 mg-t-10 mg-sm-t-0">
                   <input readonly class="form-control" placeholder="Enter lastname" type="text" value="{{$data->nama_produk}}">
                 </div>
               </div><!-- form-group -->
               <div class="form-group row">
                 <label class="col-sm-3 form-control-label">Isi</label>
                 <div class="col-sm-8 col-xl-6 mg-t-10 mg-sm-t-0">
                   <input readonly class="form-control" placeholder="Enter location" type="text" value="{{$data->isi_produk}}">
                 </div>
               </div><!-- form-group -->
               <div class="form-group row">
                 <label class="col-sm-3 form-control-label">Berat</label>
                 <div class="col-sm-8 col-xl-6 mg-t-10 mg-sm-t-0">
                   <input readonly class="form-control" placeholder="Enter location" type="text" value="{{$data->berat_produk}}">
                 </div>
               </div><!-- form-group -->
               <div class="form-group row">
                 <label class="col-sm-3 form-control-label">Dimensi</label>
                 <div class="col-sm-8 col-xl-6 mg-t-10 mg-sm-t-0">
                   <input readonly class="form-control" placeholder="Enter location" type="text" value="{{$data->dimensi_produk}}">
                 </div>
               </div><!-- form-group -->
               <div class="form-group row">
                 <label class="col-sm-3 form-control-label">Harga (Rp.)</label>
                 <div class="col-sm-8 col-xl-6 mg-t-10 mg-sm-t-0">
                   <?php $harga_produk=number_format($data->harga_produk,0,',','.'); ?>
                   <input readonly class="form-control" placeholder="Enter location" type="text" value="{{$harga_produk}}.-">
                 </div>
               </div><!-- form-group -->
               <div class="form-group row mg-b-0">
                 <label class="col-sm-3 form-control-label">Keterangan</label>
                 <div class="col-sm-9 col-xl-8 mg-t-10 mg-sm-t-0">
                   <textarea style="resize:none;width:550px;height:200px;" readonly class="form-control" placeholder="Enter some description of yourself" rows="7" >

                    {!! $data->keterangan_produk !!}
                  </textarea>
                 </div>
               </div><!-- form-group -->
             </div><!-- wd-60p -->
           </div><!-- card -->

         </div><!-- col-9 -->
       </div><!-- row -->

@endforeach





     </div><!-- kt-pagebody -->




@endsection


@section('js')
  <!-- <script src="{{ asset('admin/lib/jquery-ui/jquery-ui.js') }}"></script> -->
  <script src="{{ asset('public/admin/lib/highlightjs/highlight.pack.js') }}"></script>
  <script src="{{ asset('public/admin/lib/highlightjs/highlight.pack.js') }}"></script>
  <script src="{{ asset('public/admin/lib/datatables/jquery.dataTables.js') }}"></script>
  <script src="{{ asset('public/admin/lib/datatables-responsive/dataTables.responsive.js') }}"></script>

@endsection

@section('script')



<script>
   $(function(){
     'use strict';

     $('#datatable1').DataTable({
       responsive: true,
       language: {
         searchPlaceholder: 'Search...',
         sSearch: '',
         lengthMenu: '_MENU_ items/page',
       }
     });

     $('#datatable2').DataTable({
       bLengthChange: false,
       searching: false,
       responsive: true
     });

     // Select2
     $('.dataTables_length select').select2({ minimumResultsForSearch: Infinity });

   });
 </script>

@endsection
