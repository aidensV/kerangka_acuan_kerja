<?php $hal = 'admin_produk_reguler' ?>
<?php $sub = 0 ?>


@extends('layouts.admin.master')

@section('title', 'Tabel | Admin')

@section('content')
    <!-- ##### MAIN PANEL ##### -->

    <div class="kt-pagetitle">
      <h5>Produk Reguler</h5>
    </div><!-- kt-pagetitle -->

    <div class="kt-pagebody">

      <div class="card pd-20 pd-sm-40">
        <a href="{{route('admin_produk_reguler.create')}}" style="margin-bottom:20px; width:100px;" class="card-body-title"><button class="btn btn-primary">Tambah</button></a>

        <div class="table-wrapper">
          <table id="datatable1" class="table display responsive nowrap">
            <thead>
              <tr>
                  <th class="wd-15p">Nomor</th>
                <th class="wd-15p">kode produk</th>
                <th class="wd-15p">Kategori</th>
                <th class="wd-20p">nama produk</th>
                <th class="wd-25p">Action</th>
              </tr>
            </thead>
            <tbody>

              <?php $no = 1 ?>
              @foreach($produk as $data)
              <tr>
                <td>{{$no}}</td>
                <td>{{$data->kode_produk}}</td>
                <td>{{$data->nama_kategori}}</td>
                <td>{{$data->nama_produk}}</td>
                <td>

                  <form action="{{ route('admin_produk_reguler.destroy', $data->id_produk) }}" method="post">
											{{csrf_field()}}
											{{ method_field('DELETE') }}
                      <a href="{{route('admin_produk_reguler.show',$data->id_produk)}}" style="margin-right:10px;" class="btn btn-primary" data-toggle="tooltip" data-placement="botttom" title="Lihat Detail"><i class="fa fa-eye"></i></a>
                      <a href="{{route('admin_produk_reguler.edit',$data->id_produk)}}" style="margin-right:10px;" class="btn btn-success" data-toggle="tooltip" data-placement="botttom" title="Edit Data"><i class="icon ion-edit"></i></a>
                      <button class="btn btn-danger" onclick="return confirm('Are you sure?');" data-toggle="tooltip" data-placement="botttom" title="Hapus Data"><i class="icon ion-trash-b"></i></button >

                </form>
                </td>
              </tr>
                <?php $no++ ?>
              @endforeach


            </tbody>
          </table>
        </div><!-- table-wrapper -->
      </div><!-- card -->



@endsection


@section('js')
  <!-- <script src="{{ asset('admin/lib/jquery-ui/jquery-ui.js') }}"></script> -->
  <script src="{{ asset('public/admin/lib/highlightjs/highlight.pack.js') }}"></script>
  <script src="{{ asset('public/admin/lib/highlightjs/highlight.pack.js') }}"></script>
  <script src="{{ asset('public/admin/lib/datatables/jquery.dataTables.js') }}"></script>
  <script src="{{ asset('public/admin/lib/datatables-responsive/dataTables.responsive.js') }}"></script>

@endsection

@section('script')



<script>
   $(function(){
     'use strict';

     $('#datatable1').DataTable({
       responsive: true,
       language: {
         searchPlaceholder: 'Search...',
         sSearch: '',
         lengthMenu: '_MENU_ items/page',
       }
     });

     $('#datatable2').DataTable({
       bLengthChange: false,
       searching: false,
       responsive: true
     });

     // Select2
     $('.dataTables_length select').select2({ minimumResultsForSearch: Infinity });

   });
 </script>

@endsection
