@extends('layouts.satker.master')
@section('content')
<div id="page-content">
  <!-- Datatables Header -->

  <div class="content-header">
    <div class="header-section">

    </div>
  </div>
  <ul class="breadcrumb breadcrumb-top">
    <li>Tables</li>
    <li><a href="">Datatables</a></li>
  </ul>
  <!-- END Datatables Header -->

  <!-- Datatables Content -->




  {{-- <div class="block">
  <!-- Normal Form Title -->
  <div class="block-title">
    <div class="block-options pull-right">
      <a href="javascript:void(0)" class="btn btn-alt btn-sm btn-default toggle-bordered enable-tooltip" data-toggle="button" title="Toggles .form-bordered class">No Borders</a>
    </div>
    <h2><strong>Normal</strong> Form</h2>
  </div>
  <!-- END Normal Form Title -->

  <!-- Normal Form Content -->
  <form action="{{'satkeragenda.store'}}"
  method="post" class="form-bordered" onsubmit="return false;">
  {{ csrf_field() }}
  <div class="form-group">
    <label for="example-nf-email">Kegiatan</label>
    <input type="text" name="agenda_judul" class="form-control" placeholder="">
    {{-- <input type="hidden" value="{{Auth::user()->iduser}}" name="satker_id" class="form-control" placeholder=""> --}}
    {{-- <input type="hidden" value="{{$idkak}}" name="kak_id" class="form-control" placeholder=""> --}}
    {{-- </div>
    <div class="form-group">
      <label for="example-nf-email">Tanggal Mulai</label>
      <input type="text"  name="agenda_tgl_mulai" class="form-control" placeholder="">
    </div>
    <div class="form-group">
      <label for="example-nf-email">Tanggal Selesai</label>
      <input type="text"  name="agenda_tgl_selesai" class="form-control" placeholder="">
    </div>
    </div> --}}
    {{-- <div class="form-group">
      <label for="example-nf-email">Anggaran</label>
      <input type="text"  name="agenda_anggaran" class="form-control" placeholder="">
    </div> --}}
    {{-- <div class="form-group form-actions">
      <button type="submit" class="btn btn-sm btn-primary"><i class="fa fa-user"></i> Tambah</button>
      <button type="submit" class="btn btn-sm btn-primary"><i class="fa fa-user"></i> Tambah</button>
    </div>
  </form>
  <!-- END Normal Form Content -->
</div> --}}

    <div class="block">
    <!-- Normal Form Title -->
    <div class="block-title">
      <div class="block-options pull-right">
        <a href="javascript:void(0)" class="btn btn-alt btn-sm btn-default toggle-bordered enable-tooltip" data-toggle="button" title="Toggles .form-bordered class">No Borders</a>
      </div>
      <h2><strong>Normal</strong> Form</h2>
    </div>
    <!-- END Normal Form Title -->

    <!-- Normal Form Content -->
    <form action="{{route('satkeragenda.store')}}" method="post" class="form-bordered">
      {{ csrf_field() }}
      <div class="form-group">
        <label for="example-nf-email">Kegiatan</label>
        <input type="text" name="agenda_judul" class="form-control" placeholder="">
        <input type="hidden" value="{{Auth::user()->iduser}}" name="satker_id" class="form-control" placeholder="">
        <input type="hidden" value="{{$idkak}}" name="kak_id" class="form-control" placeholder="">
      </div>
      <div class="form-group">
        <label for="example-nf-email">Tanggal Mulai</label>
        <input type="date"  name="agenda_tgl_mulai" class="form-control" placeholder="">
      </div>
      <div class="form-group">
        <label for="example-nf-email">Tanggal Selesai</label>
        <input type="date"  name="agenda_tgl_selesai" class="form-control" placeholder="">
      </div>
      <div class="form-group">
        <label for="example-nf-email">Anggaran</label>
        <input type="text"  name="agenda_anggaran" class="form-control" placeholder="">
      </div>
      <div class="form-group form-actions">
        <button type="submit" class="btn btn-sm btn-primary"><i class="fa fa-user"></i> Tambah</button>
        <a  style="float:right;" class="btn btn-success" href="{{url('satkerkak')}}">Selesai</a>
      </div>
    </form>
    <!-- END Normal Form Content -->
  </div>

  <div class="block full">
    <div class="block-title">
      <h2>Data KAK</h2>

    </div>

    <div class="table-responsive">
      <table id="example-datatable" class="table table-vcenter table-condensed table-bordered">
        <thead>
          <tr>
            <th>No</th>
            <th>Nama Kegiatan</th>
            <th>Tanggal Mulai</th>
            <th>Tanggal Selesai</th>
            <th>Anggaran</th>
            <th>Aksi</th>
          </tr>
        </thead>
        <tbody>
          @php
          $no=1;
          @endphp
          @foreach($agenda as $data)
          <tr>
            <td style="font-size: 15px;">{{$no}}</td>
            <td style="font-size: 15px;">{{$data->title}}</td>
            <td style="font-size: 15px;">{{$data->start}}</td>
            <td style="font-size: 15px;">{{$data->end}}</td>
            <td style="font-size: 15px;">{{$data->agenda_anggaran}}</td>

            <td>
              <div class="btn-group">
                <form  action="{{route('satkeragenda.destroy',$data->id)}}" method="post">
                  {{ csrf_field() }}
                  {{ method_field('DELETE') }}
                  <input type="hidden" value="{{$idkak}}" name="kak_id" class="form-control" placeholder="">
                  <button type="submit" class="btn btn-danger">Hapus</button>
                </form>
              </div>
            </td>

          </tr>
          @php
          $no++;
          @endphp
          @endforeach
        </tbody>
      </table>
    </div>
  </div>
  <!-- END Datatables Content -->
</div>
@endsection
@section('script')

<script>
  var TablesDatatables = function() {

    return {
      init: function() {
        /* Initialize Bootstrap Datatables Integration */
        App.datatables();

        /* Initialize Datatables */
        $('#example-datatable').dataTable({

          columnDefs: [{
            orderable: false,
            targets: [1, 5]
          }],
          pageLength: 10,
          lengthMenu: [
            [10, 20, 30, -1],
            [10, 20, 30, 'All']
          ],
        });

        /* Add placeholder attribute to the search input */
        $('.dataTables_filter input').attr('placeholder', 'Search');
      }
    };
  }();
</script>

@endsection
