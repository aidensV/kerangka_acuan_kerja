@extends('layouts.satker.master')
@section('content')
<div id="page-content">
                       <!-- Datatables Header -->
                       <div class="content-header">
                           <div class="header-section">
                               <h1>
                                   <i class="fa fa-table"></i>Laporan Pembayaran PBB<br><small>Laporan Pembayaran PBB</small>
                               </h1>
                           </div>
                       </div>
                       <ul class="breadcrumb breadcrumb-top">
                           <li>Tables</li>
                           <li><a href="">Datatables</a></li>
                       </ul>
                       <!-- END Datatables Header -->

                       <!-- Datatables Content -->
                       <div class="block full">
                           <div class="block-title">
                               <h2><strong>Datatables</strong> integration</h2>
                           </div>

                           <p><a href="" class="btn btn-success btn-md pull-left" style="margin-top:-8px">Cetak PDF</a><br></p>
                             <form action="" method="post" class="form-inline">
                               {{ csrf_field() }}
                                       <div class="form-group">
                                         <select id="example-select" name="caribulan" id="caribulan" class="form-control" size="1">
                                                 <option value="">Bulan</option>
                                                 <option value="1">Januari</option>
                                                 <option value="2">Februari</option>
                                                 <option value="3">Maret</option>
                                                 <option value="4">April</option>
                                                 <option value="5">Mei</option>
                                                 <option value="6">Juni</option>
                                                 <option value="7">Juli</option>
                                                 <option value="8">Agustus</option>
                                                 <option value="9">September</option>
                                                 <option value="10">Oktober</option>
                                                 <option value="11">November</option>
                                                 <option value="12">Desember</option>
                                             </select>
                                       </div>
                                       <div class="form-group">
                                         <select id="example-select" name="caritahun" id="caritahun" class="form-control" size="1">
                                                 <option value="">Tahun</option>
                                                 <option value="2018">2018</option>
                                                 <option value="2019">2019</option>
                                                 <option value="2020">2020</option>
                                             </select>
                                       </div>
                                       <div class="form-group">
                                           <button type="submit" class="btn btn-primary"><i class="fa fa-search"></i> Search</button>
                                       </div>
                                   </form>

                           <div class="table-responsive">
                             <table id="example-datatable" class="table table-vcenter table-condensed table-bordered">
                               <thead>
                                 <th>No</th>
                                 <th>Nama Satker</th>
                                 <th>Alamat Satker</th>
                                 <th>No Hp</th>
                               </thead>



                               <tbody>
                                 <th>1</th>
                                 <th>nama</th>
                                 <th>alamat</th>
                                 <th>no hp</th>
                               </tbody>



                             </table>
                           </div>
                       </div>
                       <!-- END Datatables Content -->
                   </div>
@endsection
@endsection
@section('script')

<script>
var TablesDatatables = function() {

    return {
        init: function() {
            /* Initialize Bootstrap Datatables Integration */
            App.datatables();

            /* Initialize Datatables */
            $('#example-datatable').dataTable({

                columnDefs: [ { orderable: false, targets: [ 1, 5 ] } ],
                pageLength: 10,
                lengthMenu: [[10, 20, 30, -1], [10, 20, 30, 'All']],
            });

            /* Add placeholder attribute to the search input */
            $('.dataTables_filter input').attr('placeholder', 'Search');
        }
    };
}();
</script>

@endsection
