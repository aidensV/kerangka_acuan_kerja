@extends('layouts.satker.master')
@section('content')
<div id="page-content">
  <!-- Datatables Header -->
  <div class="content-header">
    <div class="header-section">
      <h1>
        <i class="fa fa-table"></i>Datatables<br><small>HTML tables can become fully dynamic with cool features!</small>
      </h1>
    </div>
  </div>
  <ul class="breadcrumb breadcrumb-top">
    <li>Tables</li>
    <li><a href="">Datatables</a></li>
  </ul>
  <!-- END Datatables Header -->

  <!-- Datatables Content -->
  <div class="block full">
    <div class="block-title">
      <h2>Data KAK</h2>
    </div>
    <a href="{{route('satkerkak.create')}}" class="btn btn-primary">Tambah</a>
    <div class="table-responsive">
      <table id="example-datatable" class="table table-vcenter table-condensed table-bordered">
        <thead>
          <tr>
            <th>No</th>
            <th>Judul KAK</th>
            <th>kak status</th>
            <th>bapeda</th>
            <th>bkaad</th>
            <th>inspektorat</th>
            <th>agenda</th>
            <th>Aksi</th>
          </tr>
        </thead>
        <tbody>
          @php
          $no=1;
          @endphp
          @foreach($kak as $data)
          <tr>
            <td style="font-size: 15px;">{{$no}}</td>
            <td style="font-size: 15px;">{{$data->judul}}</td>
            <td style="font-size: 15px;">
              @php
               $status1 = $data->status;
               if($status1 == '1'){
                 $status2 = 'draf';
                 $stt_kak_hidden_visibled = "style='visibility: visible'";
               }elseif($status1 == '2'){
                 $status2 = 'proses pengajuan';
                 $stt_kak_hidden_visibled = "style='visibility: hidden'";
               }elseif($status1 == '3'){
                 $status2 = 'complete';
                 $stt_kak_hidden_visibled = "style='visibility: hidden'";
               }
               echo $status2;
              @endphp
            </td>
            <td style="font-size: 15px;">@php
             $stt_bapeda = $data->kak_status_bapeda;
             if($stt_bapeda == '1'){
               $stt_bapeda1 = 'belum memvalidasi';
             }elseif($stt_bapeda == '2'){
               $stt_bapeda1 = 'revisi';
             }elseif($stt_bapeda == '3'){
               $stt_bapeda1 = 'tersetujui';
             }
             echo $stt_bapeda1;
            @endphp</td>
            <td style="font-size: 15px;">@php
             $stt_bkaad = $data->kak_status_bkaad;
             if($stt_bkaad == '1'){
               $stt_bkaad1 = 'belum memvalidasi';
             }elseif($stt_bkaad == '2'){
               $stt_bkaad1 = 'revisi';
             }elseif($stt_bkaad == '3'){
               $stt_bkaad1 = 'tersetujui';
             }
             echo $stt_bkaad1;
            @endphp</td>
            <td style="font-size: 15px;">@php
            $stt_inspektorat = $data->kak_status_inspektorat;
            if($stt_inspektorat == '1'){
              $stt_inspektorat1 = 'belum memvalidasi';
            }elseif($stt_inspektorat == '2'){
              $stt_inspektorat1 = 'revisi';
            }elseif($stt_inspektorat == '3'){
              $stt_inspektorat1 = 'tersetujui';
            }
            echo $stt_inspektorat1;
           @endphp</td>

           @php

           if($stt_bkaad == '3' && $stt_bapeda == '3' && $stt_inspektorat == '2'){
             $nama_btn = 'revisi';
           }else if($stt_bkaad == '3' && $stt_bapeda == '2' && $stt_inspektorat == '2'){
             $nama_btn = 'revisi';
           }else if($stt_bkaad == '2' && $stt_bapeda == '2' && $stt_inspektorat == '2'){
             $nama_btn = 'revisi';
           }else if($stt_bkaad == '2' && $stt_bapeda == '2' && $stt_inspektorat == '3'){
             $nama_btn = 'revisi';
           }else if($stt_bkaad == '2' && $stt_bapeda == '3' && $stt_inspektorat == '3'){
             $nama_btn = 'revisi';
           }else if($stt_bkaad == '2' && $stt_bapeda == '3' && $stt_inspektorat == '2'){
             $nama_btn = 'revisi';
           }else if($stt_bkaad == '3' && $stt_bapeda == '2' && $stt_inspektorat == '3'){
             $nama_btn = 'revisi';
           }else{
             $nama_btn = 'update';
           }

           @endphp
            <td>
              <div class="btn-group">
                <a href="{{url('detailagenda')}}/{{$data->idkak}}" data-toggle="tooltip" title="Detail" class="btn btn-xs btn-default"><i class="fa fa-pencil"> Detail</i></a>
              </div>
            </td>
            <td>
              <div class="btn-group">
                <form action="{{route('satkerkak.destroy',$data->idkak)}}" method="post">
                  {{csrf_field()}}
                  {{ method_field('DELETE') }}
                  <button type="submit" name="button" data-toggle="tooltip" title="Hapus Data KAK" class="btn btn-xs btn-default" @php echo $stt_kak_hidden_visibled; @endphp><i class="fa fa-trash"> Hapus</i></button>
                  {{-- <a href="{{route('satkerkak.show',$data->idkak)}}" data-toggle="tooltip" title="Detail" class="btn btn-xs btn-default" @php echo $stt_kak_hidden_visibled; @endphp><i class="fa fa-trash"> Delete</i></a> --}}
                </form>
              </div>
              <div class="btn-group">
                <a href="{{route('satkerkak.edit',$data->idkak)}}" data-toggle="tooltip" title="Ubah Data KAK" class="btn btn-xs btn-default" @php echo $stt_kak_hidden_visibled; @endphp><i class="fa fa-pencil"> {{$nama_btn}} </i></a>
              </div>
              <div class="btn-group">
                <a href="{{url('satkerkakpdf')}}/{{$data->idkak}}" data-toggle="tooltip" title="Detail" class="btn btn-xs btn-default"><i class="fa fa-print"> Lihat KAK</i></a>
              </div>
              <form action="satkerkakstatus/{{$data->idkak}}" method="post">
                {{csrf_field()}}
                {{ method_field('PUT') }}
                <button type="submit" type="button" name="button" data-toggle="tooltip" title="Ajukan Dokumen KAK" class="btn btn-xs btn-default" @php echo $stt_kak_hidden_visibled; @endphp><i class="fa fa-trash"> Ajukan</i></button>

              </form>

            </td>
          </tr>
          @php
          $no++;
          @endphp
          @endforeach
        </tbody>
      </table>
    </div>
  </div>
  <!-- END Datatables Content -->
</div>
@endsection
@section('script')

<script>
  var TablesDatatables = function() {

    return {
      init: function() {
        /* Initialize Bootstrap Datatables Integration */
        App.datatables();

        /* Initialize Datatables */
        $('#example-datatable').dataTable({

          columnDefs: [{
            orderable: false,
            targets: [1, 5]
          }],
          pageLength: 10,
          lengthMenu: [
            [10, 20, 30, -1],
            [10, 20, 30, 'All']
          ],
        });

        /* Add placeholder attribute to the search input */
        $('.dataTables_filter input').attr('placeholder', 'Search');
      }
    };
  }();
</script>

@endsection
