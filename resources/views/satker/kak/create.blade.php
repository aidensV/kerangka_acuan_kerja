@extends('layouts.satker.master')
@section('frolaCSS')

<!-- Include external CSS. -->
<link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.4.0/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/codemirror/5.25.0/codemirror.min.css">

<!-- Include Editor style. -->
<link href="https://cdn.jsdelivr.net/npm/froala-editor@2.9.3/css/froala_editor.pkgd.min.css" rel="stylesheet" type="text/css" />
<link href="https://cdn.jsdelivr.net/npm/froala-editor@2.9.3/css/froala_style.min.css" rel="stylesheet" type="text/css" />
@endsection

@section('content')
<div id="page-content">
  <!-- Dashboard Header -->
  <!-- For an image header add the class 'content-header-media' and an image as in the following example -->
  <div class="content-header content-header-media">
    <div class="header-section">
      <div class="row">

        <div class="col-md-4 col-lg-6 hidden-xs hidden-sm">
          <h1>Welcome <strong>Admin</strong><br><small>You Look Awesome!</small></h1>
        </div>



        <div class="col-md-8 col-lg-6">
          <div class="row text-center">
            <div class="col-xs-4 col-sm-3">
              <h2 class="animation-hatch">
                $<strong>93.7k</strong><br>
                <small><i class="fa fa-thumbs-o-up"></i> Great</small>
              </h2>
            </div>
            <div class="col-xs-4 col-sm-3">
              <h2 class="animation-hatch">
                <strong>167k</strong><br>
                <small><i class="fa fa-heart-o"></i> Likes</small>
              </h2>
            </div>
            <div class="col-xs-4 col-sm-3">
              <h2 class="animation-hatch">
                <strong>101</strong><br>
                <small><i class="fa fa-calendar-o"></i> Events</small>
              </h2>
            </div>

            <div class="col-sm-3 hidden-xs">
              <h2 class="animation-hatch">
                <strong>27&deg; C</strong><br>
                <small><i class="fa fa-map-marker"></i> Sydney</small>
              </h2>
            </div>
          </div>
        </div>
      </div>
    </div>

    <img src="{{asset('public/backend/img/placeholders/headers/dashboard_header.jpg')}}" alt="header image" class="animation-pulseSlow">
  </div>



  <!-- Clickable Wizard Block -->
  <div class="block">
    <!-- Clickable Wizard Title -->
    <div class="block-title">
      <h2><strong>E</strong> KAK</h2>
    </div>
    <!-- END Clickable Wizard Title -->

    <!-- Clickable Wizard Content -->
    <form id="clickable-wizard" action="{{route('satkerkak.store')}}" method="post" class="form-horizontal form-bordered">
      {{ csrf_field() }}
      <!-- First Step -->
      <div id="clickable-first" class="step">
        <!-- Step Info -->
        <div class="form-group">
          <div class="col-xs-12">
            <ul class="nav nav-pills nav-justified clickable-steps">
              <li class="active"><a href="javascript:void(0)" data-gotostep="clickable-first"><strong>1. Program</strong></a></li>
              <li><a href="javascript:void(0)" data-gotostep="clickable-second"><strong>2. Latar Belakang</strong></a></li>
              <li><a href="javascript:void(0)" data-gotostep="clickable-third"><strong>3. Maksud dan Tujuan</strong></a></li>
            </ul>
          </div>
        </div>
        <!-- END Step Info -->

        <div class="form-group">
          <label class="col-md-4 control-label" for="kak_program">Program</label>
          <div class="col-md-5">
            <input type="text" id="kak_program" name="kak_program" class="form-control" placeholder="Diisi nama program">
          </div>
        </div>

        <div class="form-group">
          <label class="col-md-4 control-label" for="kak_kegiatan">Kegiatan</label>
          <div class="col-md-5">
            <input type="text" id="kak_kegiatan" name="kak_kegiatan" class="form-control" placeholder="Diisi nama kegiatan">
          </div>
        </div>

      </div>
      <!-- END First Step -->

      <!-- Second Step -->
      <div id="clickable-second" class="step">
        <!-- Step Info -->
        <div class="form-group">
          <div class="col-xs-12">
            <ul class="nav nav-pills nav-justified clickable-steps">
              <li><a href="javascript:void(0)" data-gotostep="clickable-first"><i class="fa fa-check"></i> <strong>1. Program</strong></a></li>
              <li class="active"><a href="javascript:void(0)" data-gotostep="clickable-second"><strong>2. Latar Belakang</strong></a></li>
              <li><a href="javascript:void(0)" data-gotostep="clickable-third"><strong>3. Maksud dan Tujuan</strong></a></li>
            </ul>
          </div>
        </div>
        <!-- END Step Info -->
        <fieldset>
          <legend><i class="fa fa-angle-right"></i> Latar Belakang</legend>
          <div class="form-group">
            <div class="col-xs-12">
              <textarea name="kak_latarbelakang">1. Dasar Hukum Tugas Fungsi/Kebijakan
                Menjelaskan dasar hukum yang terkait dengan tugas pokok dan fungsi satker berupa UU, Kepres, Permen dan yang lainnya dan kebijakan Kementerian Negara/Lembaga yang merupakan dasar keberadaan kegiatan/aktifitas berkenaan berupa
                Peraturan Perundangan yang berlaku.
                <br>
                2. Gambaran Umum
                Menjelaskan secara singkat mengapa (why) kegiatan tersebut dilaksanakan dan alasan penting kegiatan tersebut dilaksanakan serta keterkaitan kegiatan yang dipilih dengan kegiatan keluaran (output) dalam mendukung pencapaian sasaran
                dan kinerja program, yang pada akhirnya akan mendukung pencapaian tujuan kebijakan.</textarea>

            </div>
          </div>
        </fieldset>

      </div>
      <!-- END Second Step -->

      <!-- Third Step -->
      <div id="clickable-third" class="step">
        <!-- Step Info -->
        <div class="form-group">
          <div class="col-xs-12">
            <ul class="nav nav-pills nav-justified clickable-steps">
              <li><a href="javascript:void(0)" data-gotostep="clickable-second"><i class="fa fa-check"></i> <strong>2. Latar Belakang</strong></a></li>
              <li class="active"><a href="javascript:void(0)" data-gotostep="clickable-third"><strong>3. Maksud dan Tujuan</strong></a></li>
              <li><a href="javascript:void(0)" data-gotostep="clickable-fourth"></i> <strong>4. Ruang Lingkup</strong></a></li>
            </ul>
          </div>
        </div>
        <!-- END Step Info -->


        <fieldset>
          <legend><i class="fa fa-angle-right"></i> Maksud</legend>
          <div class="form-group">
            <div class="col-xs-12">
              <textarea name="kak_maksud">Meningkatnya kualitas kinerja dan pelayanan kepada masyarakat
              </textarea>
            </div>
          </div>
        </fieldset>
        <fieldset>
          <legend><i class="fa fa-angle-right"></i> Tujuan</legend>
          <div class="form-group">
            <div class="col-xs-12">
              <textarea name="kak_tujuan">Tersedianya bahan pakai habis berupa perangko,materai dan benda pos lainnya.

              </textarea>
            </div>
          </div>
        </fieldset>
      </div>
      <!-- END Third Step -->

      <div id="clickable-fourth" class="step">
        <!-- Step Info -->
        <div class="form-group">
          <div class="col-xs-12">
            <ul class="nav nav-pills nav-justified clickable-steps">
              <li><a href="javascript:void(0)" data-gotostep="clickable-third"><i class="fa fa-check"></i> <strong>3. Maksud dan Tujuan</strong></a></li>
              <li class="active"><a href="javascript:void(0)" data-gotostep="clickable-fourth"><strong>4. Ruang Lingkup </strong></a></li>
              <li><a href="javascript:void(0)" data-gotostep="clickable-fifth"></i> <strong>5. Sasaran</strong></a></li>

            </ul>
          </div>
        </div>
        <!-- END Step Info -->


        <fieldset>
          <legend><i class="fa fa-angle-right"></i> Ruang Lingkup</legend>
          <div class="form-group">
            <div class="col-xs-12">
              <textarea name="kak_ruang_lingkup">Metode/Mekanisme yang digunakan dalam Kegiatan Penyediaan</div>
            </textarea>
            </div>
        </fieldset>
      </div>
      <!-- END Fourth Step -->

      <div id="clickable-fifth" class="step">
        <!-- Step Info -->
        <div class="form-group">
          <div class="col-xs-12">
            <ul class="nav nav-pills nav-justified clickable-steps">
              <li><a href="javascript:void(0)" data-gotostep="clickable-fourth"><i class="fa fa-check"></i> <strong>4. Ruang Lingkup</strong></a></li>
              <li class="active"><a href="javascript:void(0)" data-gotostep="clickable-fifth"><strong>5. Sasaran </strong></a></li>
              <li><a href="javascript:void(0)" data-gotostep="clickable-sixth"></i> <strong>6. Lokasi Kegiatan</strong></a></li>

            </ul>
          </div>
        </div>
        <!-- END Step Info -->


        <fieldset>
          <legend><i class="fa fa-angle-right"></i> Sasaran</legend>
          <div class="form-group">
            <div class="col-xs-12">
              <textarea name="kak_sasaran">Kegiatan Penyediaan Jasa Surat Menyurat diperlukan untuk Pegawai Badan Kesatuan
                Bangsa</textarea>
            </div>
          </div>
        </fieldset>
      </div>
      <!-- END Fifth Step -->

      <div id="clickable-sixth" class="step">
        <!-- Step Info -->
        <div class="form-group">
          <div class="col-xs-12">
            <ul class="nav nav-pills nav-justified clickable-steps">
              <li><a href="javascript:void(0)" data-gotostep="clickable-fifth"><i class="fa fa-check"></i> <strong>5. Sasaran</strong></a></li>
              <li class="active"><a href="javascript:void(0)" data-gotostep="clickable-sixth"><strong>6. Lokasi Kegiatan </strong></a></li>
              <li><a href="javascript:void(0)" data-gotostep="clickable-seventh"></i> <strong>7. Keluaran</strong></a></li>

            </ul>
          </div>
        </div>
        <!-- END Step Info -->


        <fieldset>
          <legend><i class="fa fa-angle-right"></i> Lokasi Kegiatan</legend>
          <div class="form-group">
            <div class="col-xs-12">
              <textarea name="kak_lokasi">Badan Kesatuan Bangsa dan Politik Provinsi Jawa Timur.
                .</textarea>
            </div>
          </div>
        </fieldset>
      </div>
      <!-- END Sixth Step -->
      <div id="clickable-seventh" class="step">
        <!-- Step Info -->
        <div class="form-group">
          <div class="col-xs-12">
            <ul class="nav nav-pills nav-justified clickable-steps">
              <li><a href="javascript:void(0)" data-gotostep="clickable-sixth"><i class="fa fa-check"></i> <strong>6. Lokasi Kegiatan</strong></a></li>
              <li class="active"><a href="javascript:void(0)" data-gotostep="clickable-seventh"><strong>7. Keluaran </strong></a></li>
              <li><a href="javascript:void(0)" data-gotostep="clickable-eighth"></i> <strong>8. Anggaran</strong></a></li>

            </ul>
          </div>
        </div>
        <!-- END Step Info -->


        <fieldset>
          <legend><i class="fa fa-angle-right"></i> Keluaran</legend>
          <div class="form-group">
            <div class="col-xs-12">
              <textarea name="kak_keluaran">Terselenggaranya Kegiatan
                .</textarea>
            </div>
          </div>
        </fieldset>
      </div>
      <!-- END seventh Step -->
      <div id="clickable-eighth" class="step">
        <!-- Step Info -->
        <div class="form-group">
          <div class="col-xs-12">
            <ul class="nav nav-pills nav-justified clickable-steps">
              <li><a href="javascript:void(0)" data-gotostep="clickable-seventh"><i class="fa fa-check"></i> <strong>7. Keluaran</strong></a></li>
              <li class="active"><a href="javascript:void(0)" data-gotostep="clickable-eighth"><strong>8. Anggaran </strong></a></li>
              <li><a href="javascript:void(0)" data-gotostep="clickable-ninth"></i> <strong>9. Penutup</strong></a></li>

            </ul>
          </div>
        </div>
        <!-- END Step Info -->


        <fieldset>
          <legend><i class="fa fa-angle-right"></i> Anggaran</legend>
          <div class="form-group">
            <div class="col-xs-12">
              <textarea name="kak_anggaran">Jumlah Anggaran Rp. 550.000.000,-
                .</textarea>
            </div>
          </div>
        </fieldset>
      </div>
      <!-- END eighth Step -->
      <div id="clickable-ninth" class="step">
        <!-- Step Info -->
        <div class="form-group">
          <div class="col-xs-12">
            <ul class="nav nav-pills nav-justified clickable-steps">
              <li><a href="javascript:void(0)" data-gotostep="clickable-eighth"><i class="fa fa-check"></i> <strong>8. Anggaran</strong></a></li>
              <li class="active"><a href="javascript:void(0)" data-gotostep="clickable-ninth"><strong>9. Penutup </strong></a></li>
              <li><a href="javascript:void(0)" data-gotostep="clickable-tenth"></i> <strong>10. Jadwal Kegiatan</strong></a></li>

            </ul>
          </div>
        </div>
        <!-- END Step Info -->


        <fieldset>
          <legend><i class="fa fa-angle-right"></i> Penutup</legend>
          <div class="form-group">
            <div class="col-xs-12">
              <textarea name="kak_penutup">Demikian Term Of Reference (TOR) ini disampaikan sebagai panduan dalam Kegiatan
                Penyediaan Jasa Komunikasi.
                .</textarea>
            </div>
          </div>
        </fieldset>
      </div>
      <!-- END ninth Step -->

      <!-- Form Buttons -->
      <div class="form-group form-actions">
        <div class="col-md-8 col-md-offset-4">
          <button type="reset" class="btn btn-sm btn-warning" id="back4">Previous</button>
          <button type="submit" class="btn btn-sm btn-primary" id="next4">Next</button>
        </div>
      </div>
      <!-- END Form Buttons -->
    </form>
    <!-- END Clickable Wizard Content -->



  </div>
  @endsection

  @section('js')
  <!-- Load and execute javascript code used only in this page -->
  <script src="{{asset('public/backend/js/pages/formsWizard.js')}}"></script>

  @endsection

  @section('frolaJS')
  {{-- <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script> --}}
  <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/codemirror/5.25.0/codemirror.min.js"></script>
  <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/codemirror/5.25.0/mode/xml/xml.min.js"></script>

  <!-- Include Editor JS files. -->
  <script type="text/javascript" src="https://cdn.jsdelivr.net/npm/froala-editor@2.9.3/js/froala_editor.pkgd.min.js"></script>

  <!-- Initialize the editor. -->
  <script>
    $(function() {
      $('textarea').froalaEditor()
    });
  </script>
  @endsection
