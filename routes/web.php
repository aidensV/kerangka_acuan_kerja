<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('welcome');
// });


//====================================================== Front ======================================================
Route::get('', 'MenuController@beranda');





Auth::routes();

// Route::get('/home', 'HomeController@index')->name('home');

Route::group(['middleware'=>'auth'], function(){
Route::get('index', 'LoginMultiUser@index');


  Route::get('/validatorbkaad', function(){
  	return view('validatorbkaad.dashboard.index');
  })->middleware('auth:validatorbkaad');

  Route::get('/validatorbapeda', function(){
  	return view('validatorbapeda.dashboard.index');
  })->middleware('auth:validatorbapeda');

  Route::get('/validatorinspektorat', function(){
  	return view('validatorinspektorat.dashboard.index');
  })->middleware('auth:validatorinspektorat');



// OPERATOR
  // Route::resource('admin_kak', 'adminOperatorKAKController');
// END OPERATOR

  //Inspektorat ==========================================================================================
  Route::resource('inspektoratagenda', 'InspektoratAgendaController');
  Route::resource('inspektoratindex', 'InspektoratDashboardController');
  Route::resource('inspektoratkak', 'InspektoratKAKController');

  Route::get('inspektoratambilagendasudahterlaksana', 'InspektoratAgendaController@ambil_agenda_sudah_terlaksanan');
  Route::get('inspektoratambilagendaprosespelaksanaan', 'InspektoratAgendaController@ambil_agenda_proses_pelaksanaan');
  Route::get('inspektoratambilagendabelumterlaksana', 'InspektoratAgendaController@ambil_agenda_belum_terlaksanan');

  Route::get('inspektorattampilperkegiatan/{id}', 'InspektoratAgendaController@tampil_per_kegiatan');
  Route::get('inspektorattampilperhari/{id}', 'InspektoratAgendaController@tampil_per_hari');

  Route::get('inspektorattampildetailkakpersatker/{id}', 'InspektoratKAKController@detailkakpersatker');

  Route::get('inspektoratdetailagenda/{id}', 'InspektoratKAKController@detailagenda');

  Route::get('inspektoratkakpdf/{id}','InspektoratKAKController@inspektoratkakpdf');
  // Route::get('inspektorattampilkegiatan/{id}', 'InspektoratAgendaController@tampil_kegiatan');
  //==========================================================================================

  //satker ==========================================================================================
  Route::resource('satkerindex','SatkerDashboardController');
  Route::resource('satkeragenda', 'SatkerAgendaController');
  Route::resource('satkerkak', 'SatkerKAKController');
  Route::put('satkerkakstatus/{id}', 'SatkerKAKController@ubahStatus');

  Route::get('satkerambilagendasudahterlaksana', 'SatkerAgendaController@ambil_agenda_sudah_terlaksanan');
  Route::get('satkerambilagendaprosespelaksanaan', 'SatkerAgendaController@ambil_agenda_proses_pelaksanaan');
  Route::get('satkerambilagendabelumterlaksana', 'SatkerAgendaController@ambil_agenda_belum_terlaksanan');

  Route::get('satkertampilperkegiatan/{id}', 'SatkerAgendaController@tampil_per_kegiatan');
  // Route::post('satkertampilperkegiatan', 'SatkerAgendaController@tampil_per_kegiatan');
  Route::get('satkertampilperhari/{id}', 'SatkerAgendaController@tampil_per_hari');

  Route::get('detailagenda/{id}', 'SatkerKAKController@detailagenda');

  Route::get('satkerkakpdf/{id}','SatkerKAKController@satkerkakpdf');

  Route::get('satkerdetailagendaubahstatusproses/{id}','SatkerAgendaController@detailagendaubahstatusproses');
  Route::get('satkerbuktiprogramsudahterlaksanan/{id}','SatkerAgendaController@buktiprogramsudahterlaksanan');
  //==========================================================================================

  //Validator bkaad==========================================================================================

  Route::resource('validatorbkaadagenda', 'ValidatorBkaadAgendaController');
  Route::resource('validatorbkaadindex', 'ValidatorBkaadDashboardController');
  Route::resource('validatorbkaadkak', 'ValidatorBkaadKAKController');

  Route::get('validatorbkaadambilagendasudahterlaksana', 'ValidatorBkaadAgendaController@ambil_agenda_sudah_terlaksanan');
  Route::get('validatorbkaadambilagendaprosespelaksanaan', 'ValidatorBkaadAgendaController@ambil_agenda_proses_pelaksanaan');
  Route::get('validatorbkaadambilagendabelumterlaksana', 'ValidatorBkaadAgendaController@ambil_agenda_belum_terlaksanan');

  Route::get('validatorbkaadtampilperkegiatan/{id}', 'ValidatorBkaadAgendaController@tampil_per_kegiatan');
  Route::get('validatorbkaadtampilperhari/{id}', 'ValidatorBkaadAgendaController@tampil_per_hari');

  Route::get('validatorbkaadtampildetailkakpersatker/{id}', 'ValidatorBkaadKAKController@detailkakpersatker');

  Route::get('validatorbkaaddetailagenda/{id}', 'ValidatorBkaadKAKController@detailagenda');

  Route::get('validatorbkaadupdatestatustersetujui/{id}', 'ValidatorBkaadKAKController@updatetersetujuipervalidator');
  Route::get('validatorbkaaddetailrevisi/{id}', 'ValidatorBkaadKAKController@detailrevisipervalidator');

  Route::get('validatorbkaadrevisi/{id}', 'ValidatorBkaadKAKController@revisi');
  Route::post('validatorbkaadtambahrevisi', 'ValidatorBkaadKAKController@tambahrevisi');

  Route::get('validatorbkaadupdaterevisibenar/{id}', 'ValidatorBkaadKAKController@updaterevisibenar');
  Route::get('validatorbkaadajukanrevisi/{id}', 'ValidatorBkaadKAKController@ajukanrevisi');
  // Route::get('inspektorattampilkegiatan/{id}', 'InspektoratAgendaController@tampil_kegiatan');
  //==========================================================================================

  //Validator bapeda==========================================================================================
  Route::resource('validatorbapedaagenda', 'ValidatorBapedaAgendaController');
  Route::resource('validatorbapedaindex', 'ValidatorBapedaDashboardController');
  Route::resource('validatorbapedakak', 'ValidatorBapedaKAKController');

  Route::get('validatorbapedaambilagendasudahterlaksana', 'ValidatorBapedaAgendaController@ambil_agenda_sudah_terlaksanan');
  Route::get('validatorbapedaambilagendaprosespelaksanaan', 'ValidatorBapedaAgendaController@ambil_agenda_proses_pelaksanaan');
  Route::get('validatorbapedaambilagendabelumterlaksana', 'ValidatorBapedaAgendaController@ambil_agenda_belum_terlaksanan');

  Route::get('validatorbapedatampilperkegiatan/{id}', 'ValidatorBapedaAgendaController@tampil_per_kegiatan');
  Route::get('validatorbapedatampilperhari/{id}', 'ValidatorBapedaAgendaController@tampil_per_hari');

  Route::get('validatorbapedatampildetailkakpersatker/{id}', 'ValidatorBapedaKAKController@detailkakpersatker');

  Route::get('validatorbapedadetailagenda/{id}', 'ValidatorBapedaKAKController@detailagenda');

  Route::get('validatorbapedaupdatestatustersetujui/{id}', 'ValidatorBapedaKAKController@updatetersetujuipervalidator');
  Route::get('validatorbapedadetailrevisi/{id}', 'ValidatorBapedaKAKController@detailrevisipervalidator');

  Route::get('validatorbapedarevisi/{id}', 'ValidatorBapedaKAKController@revisi');
  Route::post('validatorbapedatambahrevisi', 'ValidatorBapedaKAKController@tambahrevisi');

  Route::get('validatorbapedaupdaterevisibenar/{id}', 'ValidatorBapedaKAKController@updaterevisibenar');
  Route::get('validatorbapedaajukanrevisi/{id}', 'ValidatorBapedaKAKController@ajukanrevisi');
  // Route::get('validatorbapedaupdatestatusajukanrevisi/{id}', 'ValidatorBapedaKAKController@ajukanrevisi');
  //==========================================================================================

  //Validator inspektorat==========================================================================================
  Route::resource('validatorinspektoratagenda', 'ValidatorInspektoratAgendaController');
  Route::resource('validatorinspektoratindex', 'ValidatorInspektoratDashboardController');
  Route::resource('validatorinspektoratkak', 'ValidatorInspektoratKAKController');

  Route::get('validatorinspektoratambilagendasudahterlaksana', 'ValidatorInspektoratAgendaController@ambil_agenda_sudah_terlaksanan');
  Route::get('validatorinspektoratambilagendaprosespelaksanaan', 'ValidatorInspektoratAgendaController@ambil_agenda_proses_pelaksanaan');
  Route::get('validatorinspektoratambilagendabelumterlaksana', 'ValidatorInspektoratAgendaController@ambil_agenda_belum_terlaksanan');

  Route::get('validatorinspektorattampilperkegiatan/{id}', 'ValidatorInspektoratAgendaController@tampil_per_kegiatan');
  Route::get('validatorinspektorattampilperhari/{id}', 'ValidatorInspektoratAgendaController@tampil_per_hari');

  Route::get('validatorinspektorattampildetailkakpersatker/{id}', 'ValidatorInspektoratKAKController@detailkakpersatker');

  Route::get('validatorinspektoratdetailagenda/{id}', 'ValidatorInspektoratKAKController@detailagenda');

  Route::get('validatorinspektoratupdatestatustersetujui/{id}', 'ValidatorInspektoratKAKController@updatetersetujuipervalidator');
  Route::get('validatorinspektoratdetailrevisi/{id}', 'ValidatorInspektoratKAKController@detailrevisipervalidator');

  Route::get('validatorinspektoratrevisi/{id}', 'ValidatorInspektoratKAKController@revisi');
  Route::post('validatorinspektorattambahrevisi', 'ValidatorInspektoratKAKController@tambahrevisi');

  Route::get('validatorinspektoratupdaterevisibenar/{id}', 'ValidatorInspektoratKAKController@updaterevisibenar');
  Route::get('validatorinspektoratajukanrevisi/{id}', 'ValidatorInspektoratKAKController@ajukanrevisi');
  // Route::get('inspektorattampilkegiatan/{id}', 'InspektoratAgendaController@tampil_kegiatan');

  // Route::get('validatorinspektorattambahrevisi/{id}', 'ValidatorInspektoratKAKController@updaterevisipervalidator');
  //==========================================================================================


  //Operator ==========================================================================================
  Route::resource('operatorindex', 'OperatorDashboardController');


  Route::resource('operatordatasatker', 'OperatorDataSatkerController');
  Route::resource('operatordatapetugas','OperatorDataPetugasController');
  //==========================================================================================




// END INSPEKTORAT
  //Admin kategori Reguler==========================================================================================
  Route::resource('admin_kategori_reguler', 'AdminKategoriRegulerController');
  Route::get('datakategori_reguler', 'AdminKategoriRegulerController@listData')->name('datakategori_reguler');

  //Admin Produk Reguler==========================================================================================
  Route::resource('admin_produk_reguler', 'AdminProdukRegulerController');

  //Admin Users==========================================================================================
  Route::resource('admin_users', 'AdminPenggunaController');




});

Route::get('coba_pusher',function(){
    return view('coba_pusher');
});

Route::get('revisi',function(){
    return view('validatorinspektorat.revisi.index');
});

Route::get('firebase','FirebaseNotifikasiController@index');





Route::get('/masuk', function(){
	return view('login');
})->middleware('guest');

Route::post('/kirimdata', 'login@masuk');
Route::get('/keluar', 'login@keluar');
